package com.whatsapp.web.enums;

/**
 * Created by SHIVA on 2/5/2017.
 */
public enum ClearAndDeleteMessageAlert {
    /**
     * Enum for Clear chat.
     */
    CLEAR_CHAT("Clear"),

    /**
     * Enum for Delete chat.
     */
    DELETE_CHAT("Delete");

    /**
     * Instance variable for Alert type.
     */
    String alertType;

    /**
     * Constructor.
     * @param alertType
     */
    private ClearAndDeleteMessageAlert(final String alertType) {
        this.alertType = alertType;
    }

    /**
     * Method to get Alerts type.
     * @return String.
     */
    public String getAlertType() {
        return alertType;
    }
}





