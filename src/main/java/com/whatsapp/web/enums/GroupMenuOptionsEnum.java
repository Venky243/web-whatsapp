package com.whatsapp.web.enums;

/**
 * Created by Venu on 2/11/2017.
 */
public enum GroupMenuOptionsEnum {
    /**
     * Enum for Group info.
     */
    GROUP_INFO("Group info"),
    /**
     * Enum for Clear messages.
     */
    CLEAR_MESSAGES("Clear messages"),
    /**
     * Enum for Exit group.
     */
    EXIT_GROUP("Exit group"),
    /**
     * Enum for Delete group.
     */
    DELETE_GROUP("Delete group");

    /**
     *Enum for Menu options.
     */
    String menuOptions;

    /**
     * Initialize constructor.
     * @param menuOptions
     */
    private GroupMenuOptionsEnum(String menuOptions){
        this.menuOptions = menuOptions;
    }

    /**
     * Method to Get menu options.
     * @return menuOptions
     */
    public String getMenuOptions(){
        return menuOptions;
    }

}
