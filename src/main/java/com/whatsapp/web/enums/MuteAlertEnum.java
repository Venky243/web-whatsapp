package com.whatsapp.web.enums;

/**
 * Created by SHIVA on 2/6/2017.
 */
public enum MuteAlertEnum {
    /**
     * Enum for Eight hours.
     */
    EIGHT_HOURS("8 Hours"),

    /**
     * Enum for One week.
     */
    ONE_WEEK("1 Week"),

    /**
     * Enum for One year.
     */
    ONE_YEAR("1 Year");

    /**
     * Instance variable for mute alert time.
     */
    String muteAlertTime;

    /**
     * Costructor.
     * @param muteAlertTime
     */
    private MuteAlertEnum(String muteAlertTime) {
        this.muteAlertTime = muteAlertTime;
    }

    /**
     * Method to get Mute alert time.
     * @return String
     */
    public String getMuteAlertTime() {
        return muteAlertTime;
    }
}
