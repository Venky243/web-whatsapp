package com.whatsapp.web.enums;

/**
 * This class contains Notifications turnoff alerts enum.
 * Created by Venu on 2/5/2017.
 */
public enum NotificationsTurnoffAlertsEnum {
    /**
     * Enum for One Hour notification turnoff alert.
     */
    ONEHOUR("1"),

    /**
     * Enum for One Day notification turnoff alert.
     */

    ONEDAY("24"),

    /**
     * Enum for One Week notification turnoff alert.
     */
    ONEWEEK("168"),

    /**
     * Enum for Reset turnoff alerts.
     */

    RESETTURNOFFALERTS("0");

    /**
     * Instance variable for Alert time.
     */
    String alertTime;

    /**
     * Initialize Constructor.
     * @param alertsTime
     */
    private NotificationsTurnoffAlertsEnum(final String alertsTime) {
        this.alertTime = alertsTime;
    }

    /**
     * Method to Get notification turnoff alert time.
     * @return String
     */
    public String getNotificationTurnOffAlertTime() {
        return alertTime;
    }

}
