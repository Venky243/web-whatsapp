package com.whatsapp.web.enums;

/**
 * This class contains User profile enum.
 * Created by Venu on 2/5/2017.
 */
public enum UserProfileEnum {
    /**
     * Enum for View photo.
     */
    VIEW_PHOTO("View photo"),
    /**
     * Enum for Take photo.
     */
    TAKE_PHOTO("Take photo"),
    /**
     * Enum for Upload photo.
     */
    UPLOAD_PHOTO("Upload photo"),
    /**
     * Enum for Remove photo.
     */
    REMOVE_PHOTO("Remove photo");
    /**
     *Instance variable for Photo actions.
     */
    String photoActions;

    /**
     * Initialize Constructor.
     * @param photoActions
     */
    private UserProfileEnum(final String photoActions) {
        this.photoActions = photoActions;
    }

    /**
     * Method to get photo actions.
     * @return String
     */
    public String getPhotoActions() {
        return photoActions;
    }


}
