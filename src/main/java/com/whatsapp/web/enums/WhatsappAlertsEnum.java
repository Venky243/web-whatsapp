package com.whatsapp.web.enums;

/**
 * Created by SHIVA on 2/10/2017.
 */
public enum WhatsappAlertsEnum {

    CHAT_DELETED_ALERT("Chat deleted"),

    MESSAGE_STARRED_ALERT("Message starred"),

    MESSAGE_UNSTARRED_ALERT("Message unstarred"),

    MESSAGE_DELETED_ALERT("Message deleted"),

    CHAT_CLEARED_ALERT("Chat cleared"),

    CHAT_UNMUTED("Chat unmuted"),

    BLOCK_OR_UNBLOCK_CONTACT_UNDO_BUTTON("");



    String alertType;

    private WhatsappAlertsEnum(String alertType){
        this.alertType = alertType;
    }

    public String getAlertType(){
        return alertType;
    }
}
