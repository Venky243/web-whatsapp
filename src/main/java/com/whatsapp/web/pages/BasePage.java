package com.whatsapp.web.pages;

import com.whatsapp.web.util.TestLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * @author venkatesh
 */
public class BasePage {

    /**
     * Instance Variable for Default web element timeout.
     */
    protected final static int DEFAULT_WEBELMENT_TIMEOUT = 30;

    /**
     * Instance Variable for Max web element timeout.
     */
    protected final static int MAX_WEBELMENT_TIMEOUT = 15;

    /**
     * Instance variable for Minimum web element timeout.
     */
    protected final static int MIN_WEBELMENT_TIMEOUT = 5;

    /**
     * Instance variable for WebDriver
     */
    protected WebDriver driver;

    /**
     * Instance variable for Test logger.
     */
    TestLogger logger = TestLogger.getLogger(BasePage.class);

    /**
     * Constructor.
     *
     * @param driver
     */
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Method to clear and send value to specified Web element.
     * @param element
     * @param value
     */
    public void clearAndType(WebElement element, String value) {
        element.clear();
        element.sendKeys(value);
    }

    /**
     * Method to send keys using Actions Api.
     * @param value
     */
    public void sendKeysUsingActionsApi(String value) {
        Actions action = new Actions(driver);
        action.sendKeys("value" + Keys.ENTER).build().perform();
    }

    /**
     * Method to Current page url.
     * @return String.
     */
    public String currentPageUrl() {
        return driver.getCurrentUrl();
    }

    /**
     * Method to find click-able element.
     * @param by
     * @return web element.
     */
    public WebElement findClickableElement(By by) {
        WebElement element;
        try {
            element =
                    (new WebDriverWait(driver, DEFAULT_WEBELMENT_TIMEOUT).until(ExpectedConditions
                            .elementToBeClickable(by)));
        } catch (final Exception e) {
            final String message = "Could not find clickable element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to find click-able element.
     * @param by
     * @param timeOutInSeconds
     * @return web element.
     */
    public WebElement findClickableElement(By by, int timeOutInSeconds) {
        WebElement element;
        try {
            element =
                    (new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions
                            .elementToBeClickable(by)));
        } catch (final Exception e) {
            final String message = "Could not find clickable element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to find presence of web element.
     * @param by
     * @return web element.
     */
    public WebElement findPresenceElement(By by) {
        WebElement element;
        try {
            element =
                    (new WebDriverWait(driver, DEFAULT_WEBELMENT_TIMEOUT).until(ExpectedConditions
                            .presenceOfElementLocated(by)));
        } catch (final Exception e) {
            final String message = "Could not find presence element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to find presence of web element.
     * @param by
     * @param timeOutInSeconds
     * @return web element.
     */
    public WebElement findPresenceElement(By by, int timeOutInSeconds) {
        WebElement element;
        try {
            element =
                    (new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions
                            .presenceOfElementLocated(by)));
        } catch (final Exception e) {
            final String message = "Could not find presence element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to find visible web element.
     * @param by
     * @return web element.
     */
    public WebElement findVisibleElement(By by) {
        WebElement element;
        try {
            element =
                    (new WebDriverWait(driver, DEFAULT_WEBELMENT_TIMEOUT).until(ExpectedConditions
                            .visibilityOfElementLocated(by)));
        } catch (final Exception e) {
            final String message = "Could not find visible element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to find visible web element.
     * @param by
     * @param timeOutInSeconds
     * @return web element.
     */
    public WebElement findVisibleElement(By by, int timeOutInSeconds) {
        WebElement element;
        try {
            element =
                    (new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions
                            .visibilityOfElementLocated(by)));
        } catch (final Exception e) {
            final String message = "Could not find visible element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to find list of visible web elements.
     * @param by
     * @return web element.
     */
    public List<WebElement> findVisibleElements(By by) {
        List<WebElement> element = null;
        try {
            element =
                    (new WebDriverWait(driver, DEFAULT_WEBELMENT_TIMEOUT).until(ExpectedConditions
                            .visibilityOfAllElementsLocatedBy(by)));
        } catch (final Exception e) {
            final String message = "Could not find visible element: " + by.toString();
            throw new RuntimeException(message);
        }
        return element;
    }

    /**
     * Method to get the element text.
     * @param by
     * @param timeInSeconds
     * @return String.
     */
    public String getText(By by, int timeInSeconds) {
        String text = null;
        try {
            text = findVisibleElement(by, timeInSeconds).getText();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Method to get the element text.
     * @param element
     * @return String.
     */
    public String getText(WebElement element) {
        String text = null;
        try {
            text = element.getText();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Method to handle alert.
     * @param isAlertExist
     */
    public void handleAlert(boolean isAlertExist) {
        if (isAlertExist) {
            if (isAlertPresent()) {
                driver.switchTo().alert().accept();
            }
        } else {
            if (isAlertPresent()) {
                driver.switchTo().alert().dismiss();
            }
        }
    }

    /**
     * Method to verify Alert present.
     * @return boolean.
     */
    public boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    /**
     * Method to check whether element is displayed or not.
     * @param by
     * @param timeUnits
     * @return boolean.
     */
    public boolean isElementDisplayed(By by, int timeUnits) {
        boolean isDisplayed = false;
        try {
            isDisplayed = findVisibleElement(by, timeUnits).isDisplayed();
        } catch (final Exception e) {
            isDisplayed = false;
        }
        return isDisplayed;
    }

    /**
     * Method to check whether element is displayed or not.
     * @param by
     * @return boolean.
     */
    public boolean isElementDisplayed(By by) {
        boolean isDisplayed = false;
        try {
            isDisplayed = findVisibleElement(by).isDisplayed();
        } catch (final Exception e) {
            isDisplayed = false;
        }
        return isDisplayed;
    }

    /**
     * Method to check whether element displayed or not.
     * @param element
     * @return boolean.
     */
    public boolean isElementDisplayed(WebElement element) {
        boolean isDisplayed = false;
        try {
            isDisplayed = element.isDisplayed();
        } catch (final Exception e) {
            isDisplayed = false;
        }
        return isDisplayed;
    }

    /**
     * Method to check whether element is Enabled or not.
     * @param by
     * @return boolean.
     */
    public boolean isEnabled(By by) {
        boolean isEnabled = false;
        try {
            isEnabled = findVisibleElement(by).isEnabled();
        } catch (final Exception e) {
            isEnabled = false;
        }
        return isEnabled;
    }

    /**
     * Method to check whether element is Enabled or not.
     * @param element
     * @return boolean.
     */
    public boolean isEnabled(WebElement element) {
        boolean isEnabled = false;
        try {
            isEnabled = element.isEnabled();
        } catch (final Exception e) {
            isEnabled = false;
        }
        return isEnabled;
    }

    /**
     * Method to check whether element Selected or not.
     * @param by
     * @return boolean.
     */
    public boolean isSelected(By by) {
        boolean isSelected = false;
        try {
            isSelected = findVisibleElement(by).isSelected();
        } catch (final Exception e) {
            isSelected = false;
        }
        return isSelected;
    }

    /**
     * Method to check whether element is Selected or not.
     * @param element
     * @return boolean
     */
    public boolean isSelected(WebElement element) {
        boolean isSelected = false;
        try {
            isSelected = element.isSelected();
        } catch (final Exception e) {
            isSelected = false;
        }
        return isSelected;
    }

    /**
     * Method to select the dropdown.
     * @param element
     * @param value
     * @param index
     * @param visibleText
     */
    public void selectDropdown(WebElement element, int index, String value, String visibleText) {
        final Select select = new Select(element);
        if (index != -1) {
            select.selectByIndex(index);
        }
        if (value != null) {
            select.selectByValue(value);
        }
        if (visibleText != null) {
            select.selectByVisibleText(visibleText);
        }
    }

    /**
     * Method to sleep.
     * @param timeInSeconds
     */
    public void sleep(int timeInSeconds) {
        final int milliSec = 1000;
            try {
            Thread.sleep(milliSec * timeInSeconds);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to wait until invisible of Banner.
     * @param by
     * @return boolean.
     */
    public boolean waitUntilInvisibleOfBanner(By by) {
        final WebDriverWait wait = new WebDriverWait(driver, MAX_WEBELMENT_TIMEOUT);
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    /**
     * Method to Set checkbox.
     * @param element
     */
    public void setCheckBox(WebElement element) {
        boolean isCheckboxChecked = element.isSelected();
        if (!isCheckboxChecked) {
            element.click();
            return;
        }
        logger.logInfo("CheckBox is already Checked.");
    }

    /**
     * Method to Reset checkbox.
     * @param element
     */
    public void resetCheckbox(WebElement element) {
        boolean isCheckboxChecked = element.isSelected();
        if (isCheckboxChecked) {
            element.click();
            return;
        }
        logger.logInfo("CheckBox is not checked.");
    }

    /**
<<<<<<< HEAD
     * Method to perform move to specified element and click.
     * @param element
     */
    public void moveToElementAndClick(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().build().perform();
    }

    /**
     * Method to Move to element.
     * @param element
     */
    public void moveToElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }
    /**
     * Method to Scroll down.
     */
    public void scrollDown() {
        WebElement element = driver.findElement(By.cssSelector(".drawer-body"));
        Long height = (Long) ((JavascriptExecutor) driver).executeScript("return arguments[0].scrollHeight;", element);
        System.out.println("The height is " + height.toString());
        Long newScrollHeight = 0L;
        while (height > newScrollHeight) {
            ((JavascriptExecutor) driver).executeScript("return arguments[0].scrollTop = " + newScrollHeight.toString() + ";", element);
            newScrollHeight = newScrollHeight + 10;
        }
        sleep(2);
    }

    /**
     * Method to Get first selected options.
     * @param element
     * @param attributeName
     * @return String
     */
    public String getFirstSelectedOptions(WebElement element,String attributeName){
        Select select = new Select(element);
        String text = null;
        try{
            text =  select.getFirstSelectedOption().getAttribute(attributeName);
        }catch (Exception e){
            e.printStackTrace();
        }
        return text;
    }
}
