package com.whatsapp.web.pages.Settings;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Settings page features.
 * Created by Venu on 1/24/2017.
 */
public class SettingsPage extends BasePage {
    /**
     * By element for Back to home page button.
     */
    By backToHomePageButton = By.cssSelector("span.icon-back-light");
    /**
     * By element for Notifications Link.
     */
    By notificationsLink = By.cssSelector(".icon-settings-notifications");
    /**
     * By element for Chat wallpaper link.
     */
    By chatWallpaperLink = By.cssSelector(".icon-settings-wallpaper");

    /**
     * Constructor to Initialize Settings page.
     * @param driver
     */
    public SettingsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to click on Notifications link.
     */
    public void clickOnNotificationsLink() {
        findClickableElement(notificationsLink, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on back to home page arrow.
     */
    public void navigateToHomePage() {
        findClickableElement(backToHomePageButton, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Click on Chat wallpaper link.
     */
    public void clickOnChatWallpaper() {
        findClickableElement(chatWallpaperLink, MAX_WEBELMENT_TIMEOUT).click();
    }
}
