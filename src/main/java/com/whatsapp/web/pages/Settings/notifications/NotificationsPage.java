package com.whatsapp.web.pages.Settings.notifications;

import com.whatsapp.web.pages.BasePage;
import com.whatsapp.web.enums.NotificationsTurnoffAlertsEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Notifications page features.
 * Created by Venu on 1/25/2017.
 */
public class NotificationsPage extends BasePage {
    /**
     * By element for back arrow button
     */
    By backArrowButton = By.cssSelector("span.icon-back-light");
    /* *
     * Web element for sounds checkbox checked.
     */
    @FindBy(css = ".controls-section:nth-child(1) .checkbox-container")
    WebElement soundsCheckboxChecked;
    /* *
     * By element for sounds checkbox unchecked.
     */
    By soundsCheckboxUnChecked = By.cssSelector(".controls-section:nth-child(1) .checkbox-container");
    /**
     * Web element for desktop alerts checkbox checked.
     */
    @FindBy(css = ".controls-section:nth-child(2) .checkbox-container")
    WebElement desktopAlertsCheckboxChecked;
    /**
     * By element for desktop alerts checkbox Unchecked.
     */
    By desktopAlertsCheckboxUnChecked = By.cssSelector(".controls-section:nth-child(2) .checkbox-container");
    /**
     * Web element for show previews checkbox checked.
     */
    @FindBy(css = ".controls-section:nth-child(3) .checkbox-container")
    WebElement showPreviewsCheckboxChecked;
    /**
     * By element for show previews checkbox Unchecked.
     */
    By showPreviewsCheckboxUnChecked = By.cssSelector(".controls-section:nth-child(3) .checkbox-container");
    /**
     * Web element for select dropdown
     */
    @FindBy(css = "select")
    WebElement selectDropDown;

    /**
     * Constructor to Initialize Notifications page.
     *
     * @param driver
     */
    public NotificationsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to click on back arrow button.
     */
    public void clickOnBackArrowButton() {
        findClickableElement(backArrowButton, MAX_WEBELMENT_TIMEOUT).click();
        sleep(2);
    }

    /**
     * Method to Set sounds checkbox.
     */
    public void setSoundsCheckbox() {
        boolean isCheckedDisplayed = isElementDisplayed(soundsCheckboxChecked);
        if (isCheckedDisplayed) {
            soundsCheckboxChecked.click();
        }
    }

    /**
     * Method to reset sounds checkbox
     */
    public void resetSoundsCheckbox() {
        boolean isUnCheckedDisplayed = isElementDisplayed(soundsCheckboxUnChecked);
        if (isUnCheckedDisplayed) {
            findClickableElement(soundsCheckboxUnChecked, MAX_WEBELMENT_TIMEOUT).click();
        }
    }

    /**
     * method to check whether sounds radio button selected.
     *
     * @return boolean isDisplayed;
     */
    public boolean isSoundsCheckboxDisplayed() {
        boolean isDisplayed = isElementDisplayed(soundsCheckboxUnChecked);
        return isDisplayed;
    }

    /**
     * Method to set desktop alerts checkbox.
     */
    public void setDesktopAlertsCheckbox() {
        boolean isCheckedDisplayed = isElementDisplayed(desktopAlertsCheckboxChecked);
        if (isCheckedDisplayed) {
            desktopAlertsCheckboxChecked.click();
        }
    }

    /**
     * Method to reset desktop alerts checkbox.
     */
    public void resetDesktopAlertsCheckbox() {
        boolean isUnCheckedDisplayed = isElementDisplayed(desktopAlertsCheckboxUnChecked);
        if (isUnCheckedDisplayed) {

            findClickableElement(desktopAlertsCheckboxUnChecked, MAX_WEBELMENT_TIMEOUT).click();
        }
    }

    /**
     * Method to check whether Desktop alerts checkbox selected.
     *
     * @return boolean isDisplayed.
     */
    public boolean isDesktopAlertsCheckboxDisplayed() {
        boolean isDisplayed = isElementDisplayed(desktopAlertsCheckboxUnChecked);
        return isDisplayed;
    }

    /**
     * Method to set Show previews checkbox.
     */
    public void setShowPreviewsCheckbox() {
        boolean isCheckedDisplayed = isElementDisplayed(showPreviewsCheckboxChecked);
        if (isCheckedDisplayed) {
            showPreviewsCheckboxChecked.click();
        }
    }

    /**
     * Method to reset show previews checkbox.
     */
    public void resetShowPreviewsCheckbox() {
        boolean isUncheckedDisplayed = isElementDisplayed(showPreviewsCheckboxUnChecked);
        if (isUncheckedDisplayed) {
            findClickableElement(showPreviewsCheckboxUnChecked, MAX_WEBELMENT_TIMEOUT).click();
        }
    }

    /**
     * Method to check whether Show previews radio button selected.
     *
     * @return boolean isDisplayed.
     */
    public boolean isShowPreviewsRadioButtonDisplayed() {
        boolean isDisplayed = isElementDisplayed(showPreviewsCheckboxUnChecked);
        return isDisplayed;
    }

    /**
     * Method to Turnoff alerts
     */
    public void setTurnOffAlerts(NotificationsTurnoffAlertsEnum timeAlert) {
        selectDropdown(selectDropDown, -1, timeAlert.getNotificationTurnOffAlertTime(), null);
    }

    /**
     * Method to Get Default alerts text.
     *
     * @return String
     */
    public String getFirstSelectedValueForTurnOffAlertDropdown() {
        return getFirstSelectedOptions(selectDropDown, "value");
    }
}

