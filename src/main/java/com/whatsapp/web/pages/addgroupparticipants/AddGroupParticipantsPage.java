package com.whatsapp.web.pages.addgroupparticipants;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Add group participants page features.
 * Created by Venu on 1/30/2017.
 */
public class AddGroupParticipantsPage extends BasePage {
    /**
     * Web element for Type contact name.
     */
    @FindBy(css = ".inputarea")
    WebElement typeContactName;
    /**
     * By element for Check mark button.
     */
    By checkMarkButton = By.cssSelector(".icon-checkmark-light");

    /**
     * Constructor to Initialize Add group participants page.
     * @param driver
     */
    public AddGroupParticipantsPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to set contact name.
     * @param contactName
     */
    public void setContactName(String contactName){
        clearAndType(typeContactName,contactName);
    }

    /**
     * Method to click on searched contact name.
     * @param value
     */
    public void clickOnSearchedContactName(String value){
        String cssValue = "span[title='"+value+"']";
        findClickableElement(By.cssSelector(cssValue)).click();
    }

    /**
     * Method to click on check mark button.
     */
    public void clickOnCheckMarkButton(){
        findClickableElement(checkMarkButton,MAX_WEBELMENT_TIMEOUT).click();
    }

}
