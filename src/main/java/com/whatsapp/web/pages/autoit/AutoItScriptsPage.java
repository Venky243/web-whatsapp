package com.whatsapp.web.pages.autoit;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static java.lang.Thread.sleep;

/**
 * This class contains all Auto it scripts page.
 * Created by Venu on 2/9/2017.
 */
public class AutoItScriptsPage{

    /**
     * Method to Select group profile picture.
     */
    public static void selectGroupProfilePicture() {
        try {
            sleep(2);
            Runtime.getRuntime().exec("src\\main\\resources\\autoitscripts\\GroupProfilePicture.exe");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to Select profile picture.
     */
    public static void selectProfilePicture() {
        try {
            sleep(2);
            Runtime.getRuntime().exec("src\\main\\resources\\autoitscripts\\ProfilePicture.exe");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
