package com.whatsapp.web.pages.camera;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/2/2017.
 */
public class CameraPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public CameraPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Video viewer.
     */
    By videoViewer = By.cssSelector(".webcam");
    /**
     * By element for Captured snapshot.
     */
    By capturedSnapshot = By.cssSelector(".snapshot");
    /**
     * By element for take Photo label.
     */
    By takePhotoLabel = By.xpath("//span[text()='Take photo']");
    /**
     * Web element for cancel Take photo button.
     */
    @FindBy(css = ".btn-close-drawer")
    WebElement cancelTakePhotoButton;
    /**
     * Web element for Retake photo button.
     */
    @FindBy(css = ".drawer-header-action")
    WebElement retakePhotoButton;

    /**
     * Method to verify whether Video viewer displayed or not.
     * @return boolean
     */
    public boolean isVideoViewerDisplayed() {
        return isElementDisplayed(videoViewer);
    }

    /**
     * Method to wait until Visible of captured snapshot.
     */
    public void waitUntilVisibleOfCapturedSnapshot() {
        findVisibleElement(capturedSnapshot);
    }

    /**
     * Method to Check whether Take photo label displayed or not.
     * @return boolean
     */
    public boolean isTakePhotoLabelDisplayed() {
        return isElementDisplayed(takePhotoLabel,MIN_WEBELMENT_TIMEOUT);
    }

    /**
     * Method to click on Cancel take photo.
     */
    public void clickOnCancelTakePhoto() {
        cancelTakePhotoButton.click();
    }

    /**
     * Method to check whether captured snapshot displayed or not.
     * @return boolean
     */
    public boolean isCapturedSnapshotDisplayed() {
        return isElementDisplayed(capturedSnapshot);
    }

    /**
     * Method to click on Retake photo button.
     */
    public void clickOnRetakePhotoButton() {
        retakePhotoButton.click();
    }

    /**
     * Method to wait until invisible of Take photo label.
     * @return boolean.
     */
    public boolean waitUntilInvisibleOfTakePhotoLabel() {
        return waitUntilInvisibleOfBanner(takePhotoLabel);
    }
}
