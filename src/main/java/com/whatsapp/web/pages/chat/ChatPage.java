package com.whatsapp.web.pages.chat;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by SHIVA on 1/25/2017.
 */
public class ChatPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ChatPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     *  * By element for Block chat message button.
     */
    By blockChatMessageButton = By.cssSelector(".block-message");
    /**
     * By element for chat Input.
     */
    By chatInput = By.cssSelector("div.input-emoji .input");

    /**
     * By element for send Message button.
     */
    By sendMessageButton = By.cssSelector(".icon-send");

    /**
     * By element for You left text.
     */
    By youLeftText = By.xpath("//span[text()='You left']");

    /**
     * By element for Checkbox to select messages.
     */
    By checkboxToSelectMessages = By.cssSelector(".checkbox-container");
    /**
     * Web element for deselect Checkbox to selected messages.
     */
    @FindBy(css = ".icon-x")
    WebElement closeIconForSelectedMessages;
    /**
     * By element for Star messages button.
     */
    By starMessagesButton = By.cssSelector(".icon-star-btn");
    /**
     * By element for Delete message button.
     */
    By deleteMessageButton = By.cssSelector(".icon-delete");
    /**
     * Web element for Forward message button.
     */
    @FindBy(css = ".icon-forward")
    WebElement forwardMessageButton;
    /**
     * Web element for Selected message.
     */
    @FindBy(xpath = "//div[@class='checkbox checked']//following::div[3]")
    WebElement selectedMessage;
    /**
     * By element for Download button.
     */
    By downloadButtonDisabled = By.cssSelector(".btn-icon:disabled[title='Download']");
    /**
     * By element for Download button.
     */
    By downloadButtonEnabled = By.cssSelector(".btn-icon:enabled[title='Download']");
    /**
     * By element for Image checkbox.
     */
    By imageCheckbox = By.cssSelector(".message-image");
    /**
     * By element for textDocument.
     */
    By textDocument = By.xpath("(//span[contains(text(), '.txt')])[2]");
    /**
     * By element for Message button.
     */
    By messageButton = By.cssSelector(".bubble-btn");
    /**
     * By element for Add to Group button.
     */
    By addToAGroupButton = By.cssSelector("div[title='Add to a group']");
    /**
     * By element for Reply link.
     */
    By replyLink = By.cssSelector("a[title='Reply']");
    /**
     * By element for Options button.
     */
    By userOptionsButton = By.cssSelector(".js-context-icon");
    /**
     * By element for Chat input text.
      */
    By chatInputText = By.cssSelector("div.input");

    /**
     * Method to check whether Block chat message displayed or not.
     * @return boolean.
     */
    public boolean isBlockChatMessageDisplayed() {
        return isElementDisplayed(blockChatMessageButton);
    }

    /**
     * Method to Set chat input.
     * @param value
     */
    public void setChatInput(String value) {
        WebElement element = findVisibleElement(chatInput);
        clearAndType(element, value);
    }

    /**
     * Method to click on Send message button.
     */
    public void clickOnSendMessageButton() {
        findClickableElement(sendMessageButton).click();
    }

    /**
     * Method to get Unfinished message text.
     * @return String
     */
    public String getUnfinishedMessageText(){
      return  getText(chatInput,MAX_WEBELMENT_TIMEOUT);
    }

    /**
     * Method to check whether You left text displayed or not.
     */
    public boolean isYouLeftTextDisplayed(){
       return isElementDisplayed(youLeftText);
    }

    /**
     * Method to set Checkbox to select messages.
     */
    public void setCheckboxToSelectMessages() {
        WebElement element = findVisibleElement(checkboxToSelectMessages);
        setCheckBox(element);
    }

    /**
     * Method to click on Deselect checkbox to selected messages.
     */
    public void clickOnDeselectCheckboxToSelectedMessages() {
        closeIconForSelectedMessages.click();
    }

    /**
     * Method to verify whether Checkbox to select messages is displayed or not.
     * @return boolean.
     */
    public boolean isCheckboxToSelectMessagesIsDisplayed() {
        return isElementDisplayed(checkboxToSelectMessages);
    }

    /**
     * Method to Wait until invisible of checkbox to select messages.
     */
    public void waitUntilInvisibleOfCheckboxToSelectMessages() {
        waitUntilInvisibleOfBanner(checkboxToSelectMessages);
    }

    /**
     * Method to click on Star messages button.
     */
    public void clickOnStarMessagesButton() {
        findVisibleElement(starMessagesButton).click();
    }

    /**
     * Method to verify whether Star messages button Enabled.
     * @return boolean.
     */
    public boolean isStarMessagesButtonEnabled() {
        return isEnabled(starMessagesButton);
    }

    /**
     * Method to click on Delete message button.
     */
    public void clickOnDeleteMessageButton() {
        findClickableElement(deleteMessageButton).click();
    }

    /**
     * Method to click on Forward message button.
     */
    public void clickOnForwardMessageButton() {
        forwardMessageButton.click();
    }

    /**
     * Method to get Selected message
     * @return String
     */
    public String getSelectedMessage() {
        String selectedMessageText = getText(selectedMessage);
        return selectedMessageText;
    }

    /**
     * Method to verify whether Chat message text displayed or not.
     * @param value
     * @return boolean.
     */
    public boolean isChatMessageTextDisplayed(String value) {
        String xpathString = "(//span[text()='" + value + "'])[2]";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to select message text without time.
     * @param value
     * @return String
     */
    public String selectedMessageTextWithoutTime(String value) {
        String[] messageText = value.split("\n");
        return messageText[0];
    }

    /**
     * Method to Set all checkbox to select messages.
     * @return int.
     */
    public int setAllCheckboxesToSelectMessages() {
        int count = 0;
        final List<WebElement> list = findVisibleElements(checkboxToSelectMessages);
        for (final WebElement element : list) {
            element.click();
            count++;
        }
        return count;
    }

    /**
     * Method to check whether Selected messages count displayed or not.
     * @param value
     * @return boolean.
     */
    public boolean isSelectedMessagesCountDisplayed(int value) {
        String xpathString = "//span[text()='" + value + " selected']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether Download button disabled or not.
     * @return boolean.
     */
    public boolean isDownloadButtonDisabled() {
        return isElementDisplayed(downloadButtonDisabled);
    }

    /**
     * Method to check whether Download button enabled or not.
     * @return boolean.
     */
    public boolean isDownloadButtonEnabled() {
        return isElementDisplayed(downloadButtonEnabled);
    }

    /**
     * Method to check whether Caption text displayed or not.
     * @param value
     * @return boolean.
     */
    public boolean isCaptionTextDisplayed(String value) {
        String xpathString = "//span[text()='" + value + "']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to Set image checkbox to select message.
     */
    public void setImageCheckboxToSelectMessage() {
        findClickableElement(imageCheckbox).click();
    }

    /**
     * Method to check whether text document displayed or not.
     * @return boolean.
     */
    public boolean isTextDocumentDisplayed() {
        return isElementDisplayed(textDocument);
    }

    /**
     * Method to check whether Sended contact displayed or not.
     * @param value
     * @return boolean
     */
    public boolean isSendedContactDisplayed(String value) {
        String xpathString = "//div[text()='" + value + "']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to click on Message button.
     */
    public void clickOnMessageButton() {
        findClickableElement(messageButton).click();
    }

    /**
     * Method to click on Add to a group button.
     */
    public void clickOnAddToAGroupButton() {
        findClickableElement(addToAGroupButton).click();
    }

    /**
     * Method to check whether User added confirm message displayed or not.
     * @param user
     * @return boolean.
     */
    public boolean isUserAddedConfirmMessage(String user) {
        String xpathString = "(//span[text()='You added " + user + "'])[1]";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether Group page displayed or not.
     * @param group
     * @return boolean
     */
    public boolean isGroupPageDisplayed(String group) {
        String xpathString = "(//span[text()='" + group + "'])[2]";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether User removed confirm message displayed or not.
     * @param user
     * @return boolean
     */
    public boolean isUserRemovedConfirmMessage(String user) {
        String xpathString = "(//span[text()='You removed " + user + "'])[1]";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to click on Reply link.
     */
    public void clickOnReplyLink() {
        findVisibleElement(replyLink).click();
    }

    /**
     * Method to check whether Replay text displayed or not.
     * @param replayText
     * @return boolean
     */
    public boolean isReplayTextDisplayed(String replayText) {
        String xpathString = "//span[text()='You']//following::span[text()='" + replayText + "']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to Move to Reply message.
     * @param message
     */
    public void moveToReplyMessage(String message) {
        sleep(2);
        String xpathString = "(//span[text()='" + message + "'])[2]";
        WebElement element = findVisibleElement(By.xpath(xpathString));
        moveToElement(element);
    }

    /**
     * Method to click on Options button.
     */
    public void clickOnOptionsButton() {
        findClickableElement(userOptionsButton).click();
    }

    /**
     * Method to check whether message text displayed or not.
     * @param messageText
     * @return boolean.
     */
    public boolean isMessageTextDisplayed(String messageText){
        String xpathString = "//div[text()='"+messageText+" ']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to get Chat input text.
     * @return boolean
     */
    public String getChatInputText(){
        return getText(chatInputText,MAX_WEBELMENT_TIMEOUT);
    }

    /**
     * Method to wait until visible of chat input.
     */
    public void waitUntilVisibleOfChatInput(){
        findVisibleElement(chatInput);
    }
}
