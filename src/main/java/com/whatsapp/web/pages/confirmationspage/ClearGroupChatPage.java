package com.whatsapp.web.pages.confirmationspage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Clear group chat page features.
 * Created by Venu on 2/10/2017.
 */
public class ClearGroupChatPage extends BasePage {
    /**
     * By element for Clear button.
     */
    By clearButton = By.cssSelector(".btn-default");
    /**
     * By element for Cancel button.
     */
    By cancelButton = By.cssSelector(".popup-controls-item");
    /**
     *Initialize constructor.
     * @param driver
     */
    public ClearGroupChatPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     *Method to click on Clear button.
     */
    public void clickOnClearButton(){
        findClickableElement(clearButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on Cancel button.
     */
    public void clickOnCancelButton(){
        findClickableElement(cancelButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to check whether Clear chat group name displayed or not.
     * @param groupName
     * @return boolean
     */
    public boolean isClearChatGroupNameDisplayed(String groupName){
        String xpath = "//span[text()='Clear “"+groupName+"” group?']";
       return isElementDisplayed(By.xpath(xpath));
    }
}
