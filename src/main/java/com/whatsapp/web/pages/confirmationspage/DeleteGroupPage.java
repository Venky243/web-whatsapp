package com.whatsapp.web.pages.confirmationspage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all features of Delete group page.
 * Created by Venu on 2/11/2017.
 */
public class DeleteGroupPage extends BasePage {
    /**
     * By element for Delete button.
     */
    By deleteButton = By.cssSelector(".btn-default");
    /**
     * By element for Cancel button.
     */
    By cancelButton = By.cssSelector(".btn-plain");

    /**
     * Initialize constructor.
     * @param driver
     */
    public DeleteGroupPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to click on Delete button.
     */
    public void clickOnDeleteButton(){
        findClickableElement(deleteButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on Cancel button.
     */
    public void clickOnCancelButton(){
        findClickableElement(cancelButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to check whether deleted group name displayed or not.
     * @param groupName
     * @return boolean
     */
    public boolean isDeletedGroupNameDisplayed(String groupName){
        String xpath = "//span[text()='Delete “"+groupName+"” group?']";
        return isElementDisplayed(By.xpath(xpath));
    }
}
