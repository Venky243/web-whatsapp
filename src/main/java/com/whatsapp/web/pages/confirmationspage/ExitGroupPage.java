package com.whatsapp.web.pages.confirmationspage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all features of Exit group page.
 * Created by Venu on 2/11/2017.
 */
public class ExitGroupPage extends BasePage {
    /**
     * By element for Exit button.
     */
    By exitButton = By.cssSelector(".btn-default");
    /**
     * By element for Cancel button.
     */
    By cancelButton = By.cssSelector(".btn-plain");

    /**
     * Initialize Constructor.
     * @param driver
     */
    public ExitGroupPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to click on Exit button.
     */
    public void clickOnExitButton(){
        findClickableElement(exitButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     *Method to click on Cancel button.
     */
    public void clickOnCancelButton(){
        findClickableElement(cancelButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to check whether Exited group name displayed or not.
     * @param groupName
     * @return boolean
     */
    public boolean isExitedGroupNameDisplayed(String groupName){
        String xpath = "//span[text()='Exit “"+groupName+"” group?']";
       return isElementDisplayed(By.xpath(xpath));
    }

}
