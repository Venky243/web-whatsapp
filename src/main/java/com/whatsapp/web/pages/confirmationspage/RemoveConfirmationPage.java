package com.whatsapp.web.pages.confirmationspage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Remove confirmation page features.
 * Created by Venu on 2/7/2017.
 */
public class RemoveConfirmationPage extends BasePage {
    /**
     * By element for Remove button.
     */
    By removeButton = By.cssSelector(".btn-default");
    /**
     * By element for Cancel button.
     */
    By cancelButton = By.cssSelector(".btn-plain");

    /**
     * Initialize constructor.
     * @param driver
     */
    public RemoveConfirmationPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     *Method to click on Remove button.
     */
    public void clickOnRemoveButton(){
        findClickableElement(removeButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Click on Cancel button.
     */
    public void clickOnCancelButton(){
        findClickableElement(cancelButton,MAX_WEBELMENT_TIMEOUT).click();
    }
}
