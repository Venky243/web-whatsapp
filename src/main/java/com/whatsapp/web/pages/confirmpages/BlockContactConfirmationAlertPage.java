package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/26/2017.
 */
public class BlockContactConfirmationAlertPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public BlockContactConfirmationAlertPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Block confirm button.
     */
    By blockConfirmButton = By.cssSelector(".btn-default");

    /**
     * By element for cancel block button
     */
    By cancelBlockButton = By.xpath("//button[text()='Cancel']");

    /**
     * Method to click on Block confirm button.
     */
    public void clickOnBlockConfirm() {
        findClickableElement(blockConfirmButton).click();
    }

    /**
     * Method to check whether Block alert body displayed or not.
     * @param user
     * @return boolean
     */
    public boolean isBlockAlertBodyDisplayed(String user){
        String xpathString = "//div[text()='Block "+user+"?']";
       return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether block alert cancel button displayed or not.
     * @return boolean
     */
    public boolean isBlockAlertCancelButtonDisplayed(){
        return isElementDisplayed(cancelBlockButton);
    }

    /**
     * Method to check whether block alert block confirm button displayed or not.
     * @return boolean
     */
    public boolean isBlockAlertBlockConfirmButtonDisplayed(){
        return isElementDisplayed(blockConfirmButton);
    }





}
