package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import com.whatsapp.web.enums.ClearAndDeleteMessageAlert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class ClearOrDeleteMessageConfirmationPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ClearOrDeleteMessageConfirmationPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for clear chat alert cancel button.
     */
    By clearOrDeleteMessageAlertCancelButton = By.xpath("//button[text()='Cancel']");

    /**
     * By element for clear chat alert clear button.
     */
    By clearMessageAlertClearButton = By.xpath("//button[text()='Clear']");

    /**
     * By element for delete message alert body.
     */
    By deleteMessageAlertBody = By.xpath("//span[text()='Delete message?']");

    /**
     * By element for delete messages alert body.
     */
    By deleteMessagesAlertBody = By.xpath("//span[contains(text(),'Delete')]");

    /**
     * By element for delete message alert Delete button.
     */
    By deleteMessageAlertDeleteButton = By.xpath("//button[text()='Delete']");

    /**
     * Method to check whether Clear message alert body displayed.
     * @return boolean
     */
    public boolean isClearMessageAlertBodyDisplayed() {
        String xpathString = "//span[contains(text(),'Clear chat')]";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether clear message alert cancel button displayed or not.
     * @return boolean
     */
    public boolean isClearMessageAlertCancelButtonDisplayed() {
        return isElementDisplayed(clearOrDeleteMessageAlertCancelButton);
    }

    /**
     * Method to check whether clear message alert clear button displayed or not.
     * @return boolean
     */
    public boolean isClearMessageAlertClearButtonDisplayed() {
        return isElementDisplayed(clearMessageAlertClearButton);
    }

    /**
     * Method to Delete message alert body displayed or not.
     * @return boolean
     */
    public boolean isDeleteMessageAlertBodyDisplayed() {
        return isElementDisplayed(deleteMessageAlertBody);
    }

    /**
     * Method to Delete messages alert body displayed or not.
     * @return boolean
     */
    public boolean isDeleteMessagesAlertBodyDisplayed() {
        return isElementDisplayed(deleteMessagesAlertBody);
    }

    /**
     * Method to Delete message alert cancel button displayed or not.
     * @return boolean.
     */
    public boolean isDeleteMessageAlertCancelButtonDisplayed() {
        return isElementDisplayed(clearOrDeleteMessageAlertCancelButton);
    }

    /**
     * Method to Delete message alert delete button displayed or not.
     * @return boolean
     */
    public boolean isDeleteMessageAlertDeleteButtonDisplayed() {
        return isElementDisplayed(deleteMessageAlertDeleteButton);
    }

    /**
     * Method to click on Clear messages confirm.
     * @Param alertEnum
     */
    public void clickOnClearMessagesConfirm(ClearAndDeleteMessageAlert alertsEnum) {
        String xpathString = "//button[text()='" + alertsEnum.getAlertType() + "']";
        findVisibleElement(By.xpath(xpathString)).click();
    }

    /**
     * Method to click on Delete message confirm button.
     * @param alertsEnum
     */
    public void clickOnDeleteMessageConfirm(ClearAndDeleteMessageAlert alertsEnum) {
        String xpathString = "//button[text()='"+alertsEnum.getAlertType()+"']";
        findClickableElement(By.xpath(xpathString)).click();
    }
}
