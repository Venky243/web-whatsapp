package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import com.whatsapp.web.enums.ClearAndDeleteMessageAlert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class DeleteChatConfirmPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public DeleteChatConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for delete chat alert cancel button.
     */
    By deleteChatAlertCancelButton = By.xpath("//button[text()='Cancel']");

    /**
     * By element for delete chat alert delete button.
     */
    By deleteChatAlertDeleteButton = By.xpath("//button[text()='Delete']");

    /**
     * Method to click on Delete chat confirm button.
     */
    public void clickOnDeleteChatConfirm(ClearAndDeleteMessageAlert alertsEnum){
        String xpathString = "//button[text()='"+ alertsEnum.getAlertType()+"']";
        findClickableElement(By.xpath(xpathString)).click();
    }

    /**
     * Method to check whether Delete chat alert body displayed or not.
     * @param user
     * @return boolean
     */
    public boolean isDeleteChatAlertBodyDisplayed(String user){
        String xpathString = "//span[text()='Delete chat with “"+user+"”?']";
       return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether delete chat alert cancel button displayed or not.
     * @return boolean.
     */
    public boolean isDeleteChatAlertCancelButtonDisplayed(){
        return isElementDisplayed(deleteChatAlertCancelButton);
    }

    /**
     * Method to check whether delete chat alert delete button displayed or not.
     * @return boolean.
     */
    public boolean isDeleteChatAlertDeleteButtonDisplayed(){
        return isElementDisplayed(deleteChatAlertDeleteButton);
    }
}
