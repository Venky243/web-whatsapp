package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import org.kohsuke.rngom.parse.host.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class ForwardMessageConfirmPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ForwardMessageConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for forward message alert head.
     */
    By forwardMessageAlertHead = By.xpath("//span[text()='Forward message to']");

    /**
     * Web element for Forward message confirm button.
     */
    @FindBy(css = ".btn-default")
    WebElement forwardMessageConfirmButton;

    /**
     * Method to click on Forward message confirm button.
     */
    public void clickOnForwardMessageConfirmButton() {
        forwardMessageConfirmButton.click();
    }

    /**
     * Method to Forward message alert head displayed or not.
     * @return boolean.
     */
    public boolean isForwardMessageAlertHeadDisplayed(){
        return isElementDisplayed(forwardMessageAlertHead);
    }
}
