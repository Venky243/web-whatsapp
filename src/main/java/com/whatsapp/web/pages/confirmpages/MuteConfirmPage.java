package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import com.whatsapp.web.enums.MuteAlertEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class MuteConfirmPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public MuteConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Mute button.
     */
    By muteConfirmButton = By.cssSelector(".btn-default");

    /**
     * By element for mute alert Cancel button.
     */
    By muteAlertCancelButton = By.xpath("//button[text()='Cancel']");

    /**
     * By element for mute alert Mute button.
     */
    By muteAlertMuteButton = By.xpath("//button[text()='Mute']");
    /**
     * Method to Click on Mute Button.
     */
    public void clickOnMuteButton() {
        findClickableElement(muteConfirmButton).click();
    }

    /**
     * Method to Set mute time.
     * @param muteTime
     */
    public void setMuteTime(MuteAlertEnum muteTime) {
        String xpathString = "//label[text()='" + muteTime.getMuteAlertTime() + "']";
        findClickableElement(By.xpath(xpathString)).click();
    }

    /**
     * Method to Mute alert head displayed or not.
     * @param user
     * @return boolean
     */
    public boolean isMuteAlertHeadDisplayed(String user){
        String xpathString = "//span[text()='Mute “"+user+"” for…']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to Mute alert cancel button displayed or not.
     * @return boolean
     */
    public boolean isMuteAlertCancelButtonDisplayed(){
        return isElementDisplayed(muteAlertCancelButton);
    }

    /**
     * Method to Mute alert mute button displayed or not.
     * @return boolean
     */
    public boolean isMuteAlertMuteButtonDisplayed(){
        return isElementDisplayed(muteAlertMuteButton);
    }



}
