package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class RemoveUserConfirmPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public RemoveUserConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for remove user confirm button.
     */
    By removeUserConfirm = By.cssSelector(".btn-default");

    /**
     * By element for Remove user alert cancel button.
     */
    By removeUserAlertCancelButton = By.xpath("//button[text()='Cancel']");

    /**
     * By element for Remove user alert remove button.
     */
    By removeUserAlertRemoveButton = By.xpath("//button[text()='Remove']");
    /**
     * Method to click on Remove user confirm button
     */
    public void clickOnRemoveUserConfirmButton() {
        findClickableElement(removeUserConfirm).click();
    }

    /**
     * Method to Remove user alert head displayed or not.
     * @param user
     * @param group
     * @return boolean
     */
    public boolean isRemoveUserAlertHeadDisplayed(String user, String group){
        String xpathString = "//span[text()='Remove "+user+" from “"+group+"” group?']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to Remove user alert cancel button displayed or not.
     * @return boolean
     */
    public boolean isRemoveUserAlertCancelButton(){
        return isElementDisplayed(removeUserAlertCancelButton);
    }

    /**
     * Method to Remove user alert remove button displayed or not.
     * @return boolean
     */
    public boolean isRemoveUserAlertRemoveButton(){
        return isElementDisplayed(removeUserAlertRemoveButton);
    }

}
