package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class SendContactConfirmPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public SendContactConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for send contact confirm button
     */
    By sendContactConfirmButton = By.cssSelector(".btn-default");

    /**
     * Method to click on Send contact confirm button.
     */
    public void clickOnSendContactConfirmButton() {
        findVisibleElement(sendContactConfirmButton).click();
    }
}
