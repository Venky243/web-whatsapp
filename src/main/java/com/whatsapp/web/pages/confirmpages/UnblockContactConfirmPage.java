package com.whatsapp.web.pages.confirmpages;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/6/2017.
 */
public class UnblockContactConfirmPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public UnblockContactConfirmPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Unblock contact confirm.
     */
    By unblockContactConfirm = By.cssSelector(".btn-default");

    /**
     * By element for Unblock contact alert cancel button.
     */
    By unblockAlertCancelButton = By.xpath("//button[text()='Cancel']");

    /**
     * By element for Unblock alert unblock button.
     **/
    By unblockAlertUnblockButton = By.xpath("//button[text()='Unblock']");

    /**
     * Method to click on Unblock contact confirm button.
     */
    public void clickOnUnblockContactConfirm() {
        findClickableElement(unblockContactConfirm).click();
    }

    /**
     * Method to Unblock alert head displayed or not.
     * @param user
     * @return boolean
     */
    public boolean isUnblockAlertHeadDisplayed(String user){
        String xpathString = "//div[text()='Unblock "+user+"?']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to Unblock alert cancel button displayed or not.
     * @return boolean
     */
    public boolean isUnblockAlertCancelButtonDisplayed(){
        return isElementDisplayed(unblockAlertCancelButton);
    }

    /**
     * Method to Unblock alert unblock button displayed or not.
     * @return boolean
     */
    public boolean isUnblockAlertUnblockButtonDisplayed(){
        return isElementDisplayed(unblockAlertUnblockButton);
    }


}
