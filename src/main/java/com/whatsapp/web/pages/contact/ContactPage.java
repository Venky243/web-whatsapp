package com.whatsapp.web.pages.contact;

import com.whatsapp.web.pages.BasePage;
import com.whatsapp.web.pages.contactinfopage.ContactInfoPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/2/2017.
 */
public class ContactPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ContactPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for User.
     */
    By user = By.cssSelector("span[title='You']");

    /**
     * Method to select Send contact.
     * @param group
     */
    public void selectSendContact(String group) {
        String xpathString = "(//span[text()='"+group+"'])[1]";
        findClickableElement(By.xpath(xpathString)).click();
    }

    /**
     * Method to wait until visible of User.
     */
    public void waitUntilVisibleUser() {
        findVisibleElement(user);
    }
}
