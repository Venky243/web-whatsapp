package com.whatsapp.web.pages.contactinfopage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/25/2017.
 */
public class ContactInfoPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ContactInfoPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Contact user menu.
     */
    By contactUserMenu = By.cssSelector("div.pane-chat-controls .icon-menu");

    /**
     * By element for contact info.
     */
    By contactInfo = By.cssSelector("a[title='Contact info']");

    /**
     * By element for Select messages link.
     */
    By selectMessagesLink = By.cssSelector("a[title='Select messages']");

    /**
     * By element for Attachment button.
     */
    By attachmentButton = By.cssSelector(".icon-clip");

    /**
     * By element for Photos and Videos selection button.
     */
    By photosAndVideosButton = By.cssSelector(".menu-icons-item");

    /**
     * By element for Camera button.
     */
    By cameraButton = By.cssSelector(".menu li:nth-of-type(2)");

    /**
     * By element for Document button.
     */
    By documentButton = By.cssSelector(".menu li:nth-of-type(3)");

    /**
     * By element for Contact button.
     */
    By contactButton = By.cssSelector(".menu li:nth-of-type(4)");

    /**
     * By element for close Contact info.
     */
    By closeContactInfo = By.cssSelector(".icon-x");

    /**
     * By element for Clear message link.
     */
    By clearMessageLink = By.cssSelector("a[title='Clear messages']");

    /**
     * Method to click on Contact Info.
     */
    public void clickOnContactInfo() {
        findClickableElement(contactInfo).click();
    }

    /**
     * Method to click on Contact User menu.
     */
    public void clickOnContactUserMenu() {
        findClickableElement(contactUserMenu).click();
    }

    /**
     * Method to click on Select messages link.
     */
    public void clickOnSelectMessagesLink() {
        findClickableElement(selectMessagesLink).click();
    }

    /**
     * Method to click on Attachment button.
     */
    public void clickOnAttachmentButton() {
        findClickableElement(attachmentButton).click();
    }

    /**
     * Method to click on Photos and Videos button.
     */
    public void clickOnPhotosAndVideosButton() {
        findClickableElement(photosAndVideosButton).click();
    }

    /**
     * Method to click on Camera button.
     */
    public void clickOnCameraButton() {
        findClickableElement(cameraButton).click();
    }

    /**
     * Method to click on Document button.
     */
    public void clickOnDocumentButton() {
        findClickableElement(documentButton).click();
    }

    /**
     * Method to click on Contact button.
     */
    public void clickOnContactButton() {
        findClickableElement(contactButton).click();
    }

    /**
     * Method to click on Close contact info.
     */
    public void clickOnCloseContactInfo() {
        findVisibleElement(closeContactInfo).click();
    }

    /**
     * Method to click on Clear message link.
     */
    public void clickOnClearMessageLink() {
        findClickableElement(clearMessageLink).click();
    }
}

