package com.whatsapp.web.pages.contactlist;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/24/2017.
 */
public class ContactListPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ContactListPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * Web element for Search contact.
     */
    @FindBy(css = "input[title='Search contacts']")
    WebElement searchContact;
    /**
     * By element for Cancel mute.
     */
    By cancelMute = By.cssSelector("a[title='Cancel mute']");
    /**
     * By element for Muted Icon.
     */
    By mutedIcon = By.cssSelector(".icon-muted");
    /**
     * Web element for contact Context button.
     */
    @FindBy(css = ".btn-context")
    WebElement contactContextButton;

    /**
     * Method to check whether Muted icon displayed or not.
     * @return boolean.
     */
    public boolean isMutedIconDisplayed() {
        return isElementDisplayed(mutedIcon);
    }

    /**
     * Method to click on Search Contact.
     */
    public void clickOnSearchContact() {
        searchContact.click();
    }

    /**
     * Method to click on User ChatPage profile.
     * @param user
     */
    public void clickOnUserChatProfile(String user) {
        String xpathExpression = "//span[@title='" + user + "']";
        findClickableElement(By.xpath(xpathExpression)).click();
    }

    /**
     * Method to set Search contact.
     * @param value
     */
    public void searchContact(String value) {
        clearAndType(searchContact, value);
    }

    /**
     * Method to Move to muted icon.
     */
    public void moveToMutedIcon() {
        WebElement element = findVisibleElement(mutedIcon);
        moveToElement(element);
    }

    /**
     * Method to click on Contact Context button.
     */
    public void clickOnContactContextButton() {
        contactContextButton.click();
    }

    /**
     * Method to click on Cancel Muted.
     */
    public void clickOnCancelMuted() {
        boolean cancelMuteIsDisplayed = isElementDisplayed(cancelMute);
        if (cancelMuteIsDisplayed) {
            findClickableElement(cancelMute).click();
        }
    }
}
