package com.whatsapp.web.pages.document;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

/**
 * Created by SHIVA on 2/2/2017.
 */
public class DocumentPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public DocumentPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for send Document button.
     */
    By sendDocumentButton = By.cssSelector(".btn-round");

    /**
     * By element for Cancel preview document.
     */
    By cancelPreviewDocument = By.cssSelector(".btn-close-drawer");

    /**
     * By element for Preview title.
     */
    By previewTitle = By.cssSelector(".drawer-title-body");

    /**
     * Method to click on send Document button.
     */
    public void clickOnSendDocumentButton() {
        findClickableElement(sendDocumentButton).click();
    }

    /**
     * Method to click on Cancel preview document.
     */
    public void clickOnCancelPreviewDocument() {
        findClickableElement(cancelPreviewDocument).click();
    }

    /**
     * Method to check whether preview title displayed.
     * @return boolean.
     */
    public boolean isPreviewTitleDisplayed() {
        return isElementDisplayed(previewTitle,MIN_WEBELMENT_TIMEOUT);
    }

    /**
     * Method to wait until invisible of cancel preview document.
     */
    public void waitUntilInvisibleOfCancelPreviewDocument() {
        waitUntilInvisibleOfBanner(cancelPreviewDocument);
    }
}
