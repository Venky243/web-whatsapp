package com.whatsapp.web.pages.forwardmessage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/29/2017.
 */
public class ForwardMessagePage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ForwardMessagePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Search contact.
     */
    By searchContact = By.cssSelector(".input-search");

    /**
     * By element for Contact checkbox.
     */
    By contactCheckbox = By.cssSelector(".checkbox-container");

    /**
     * Method to set Search Contact.
     * @param value
     */
    public void setSearchContact(String value) {
        WebElement element = findVisibleElement(searchContact);
        clearAndType(element, value);
    }

    /**
     * Method to click on Searched contact.
     * @param value
     */
    public void clickOnSearchedContact(String value) {
        String cssString = "span[title='" + value + "']";
        findClickableElement(By.cssSelector(cssString)).click();
    }

    /**
     * Method to wait until visible of Search contact.
     */
    public void waitUntilVisibleOfSearchContact(){
        findVisibleElement(searchContact);
    }

    /**
     * Method to Set contact checkbox.
     */
    public void setContactCheckbox(){
        WebElement element = findClickableElement(contactCheckbox);
        setCheckBox(element);
    }




}

