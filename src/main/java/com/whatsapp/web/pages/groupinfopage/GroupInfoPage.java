package com.whatsapp.web.pages.groupinfopage;

import com.whatsapp.web.enums.GroupMenuOptionsEnum;
import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Group info page features
 * Created by Venu on 2/3/2017.
 * @author  venu
 */
public class GroupInfoPage extends BasePage {
    /**
     * By element for Group info menu button.
     */
    By groupInfoMenuButton = By.cssSelector(".pane-chat-header .icon-menu");
    /**
     * By element for Search button.
     */
    By searchButton = By.cssSelector(".icon-search-alt");


    /**
     * Initialize constructor.
     * @param driver
     */
    public GroupInfoPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to click on group info menu button.
     */
    public void clickOnGroupInfoMenuButton(){
        findClickableElement(groupInfoMenuButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Click on Group info menu options.
     * @param groupMenuOptionsEnum
     */
    public void groupInfoMenuOptions(GroupMenuOptionsEnum groupMenuOptionsEnum){
        String cssValue = "a[title='"+groupMenuOptionsEnum.getMenuOptions()+"']";
        findClickableElement(By.cssSelector(cssValue)).click();
    }

    /**
     * Method to click on Search button.
     */
    public void clickOnSearchButton(){
        findClickableElement(searchButton,MAX_WEBELMENT_TIMEOUT).click();
    }



}
