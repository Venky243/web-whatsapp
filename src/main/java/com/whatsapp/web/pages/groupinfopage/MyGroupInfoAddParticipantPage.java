package com.whatsapp.web.pages.groupinfopage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Venu on 2/3/2017.
 */
public class MyGroupInfoAddParticipantPage extends BasePage {
    /**
     * Web element for Search contacts.
     */
    @FindBy(css = "input[title='Search contacts']")
    WebElement searchContacts;
    /**
     * By element for Add participant button "Pop-up"
     */
    By popupAddParticipantButton = By.cssSelector(".btn-default");

    /**
     * Initialize constructor.
     * @param driver
     */
    public MyGroupInfoAddParticipantPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to Search contact name.
     * @param contactName
     */
    public void searchContactName(String contactName){
       searchContacts.click();
        clearAndType(searchContacts,contactName);
    }

    /**
     * Method to Click on searched contact.
     * @param contactName
     */
    public void clickOnSearchedContact(String contactName){
        String cssValue = "span[title='"+contactName+"']";
        findClickableElement(By.cssSelector(cssValue),MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Click on popup add participant button.
     */
    public void clickOnPopupAddParticipantButton(){
        findClickableElement(popupAddParticipantButton,MAX_WEBELMENT_TIMEOUT).click();
    }


}
