package com.whatsapp.web.pages.groupinfopage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


/**
 * This class contains all My group info page features.
 * Created by Venu on 2/3/2017.
 */
public class MyGroupInfoPage extends BasePage {
    /**
     * By element for participants count.
     */
    By participantsCount = By.cssSelector(".well-simple-body .row-side");
    /**
     * By element for Add participant button.
     */
    By addParticipantButton = By.cssSelector(".list-action-icon .icon-add-user-light");
    /**
     * By element for Selected contact dropdown.
     */
    By selectedContactDropdown = By.cssSelector(".icon-down");
    /**
     * By element for Remove link.
     */
    By removeLink = By.cssSelector("a[title='Remove']");
    /**
     * By element for Select user.
     */
    By selectUser = By.cssSelector(".btn-context");
    /**
     * By element for Add group icon.
     */
    By addGroupIcon = By.cssSelector(".icon-camera-light");
    /**
     * By element for Done button.
     */
    By doneButton = By.cssSelector(".button");
    /**
     * By element for Pencil button.
     */
    By pencilButton = By.cssSelector(".icon-pencil");
    /**
     * Web element for Enter group name.
     */
    @FindBy (css = ".input-text")
            WebElement enterGroupName;
    /**
     * By element for Save button.
     */
    By saveButton = By.cssSelector(".icon-checkmark");

    /**
     * Initialize constructor.
     * @param driver
     */
    public MyGroupInfoPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to Get participants count.
     * @return  string
     */
    public String getParticipantsCount(){
       return getText(participantsCount,MAX_WEBELMENT_TIMEOUT);
    }

    /**
     * Method to click on Add participant button.
     */
    public void clickOnAddParticipantButton(){
        findClickableElement(addParticipantButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     *Method to Move to selected contact dropdown.
     */
    public void moveToSelectedContactDropdown(){
        moveToElementAndClick(findVisibleElement(selectedContactDropdown));
    }

    /**
     * Method to Click on remove link.
     */
    public void clickOnRemoveLink(){
        findClickableElement(removeLink,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Move to selected contact.
     * @param contactName
     */
    public void moveToSelectedContact(String contactName){
        String xpath = "//span[text()='"+contactName+"']";
        moveToElement(findVisibleElement(By.xpath(xpath)));
    }

    /**
     * Method to click on Contact options button.
     */
    public void clickOnContactOptionsButton(){
        findClickableElement(selectUser).click();
    }

    /**
     * Method to click on Add group icon.
     */
    public void clickOnAddGroupIcon(){
        findClickableElement(addGroupIcon,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on Done button.
     */
    public void clickOnDoneButton(){
        findClickableElement(doneButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on Pencil button.
     */
    public void clickOnPencilButton(){
        findClickableElement(pencilButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Set group name.
     * @param groupName
     */
    public void setGroupName(String groupName){
        clearAndType(enterGroupName,groupName);
    }

    /**
     * Method to click on Save button.
     */
    public void clickOnSaveButton(){
      findClickableElement(saveButton,MAX_WEBELMENT_TIMEOUT).click();
    }
}
