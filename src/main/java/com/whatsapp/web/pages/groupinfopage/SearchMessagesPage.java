package com.whatsapp.web.pages.groupinfopage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all features of Search messages page.
 * Created by Venu on 2/10/2017.
 */
public class SearchMessagesPage extends BasePage {
    /**
     * Web element for Search message.
     */
    @FindBy(css = ".input-search")
    WebElement searchMessage;
    /**
     * By element for Searched message
     */
    By searchedMessage = By.cssSelector(".matched-text");
    /**
     * By element for Close button.
     */
    By closeButton = By.cssSelector(".icon-x");

    /**
     * Initialize constructor.
     * @param driver
     */
    public SearchMessagesPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to set Search message.
     * @param message
     */
    public void setSearchMessage(String message){
        clearAndType(searchMessage,message);
    }

    /**
     * Method to check whether Searched message displayed or not.
     * @return boolean
     */
    public boolean isSearchedMessageDisplayed(){
        sleep(2);
        return isElementDisplayed(searchedMessage);
    }

    /**
     * Method to click on Close button.
     */
    public void clickOnCloseButton(){
        findClickableElement(closeButton,MAX_WEBELMENT_TIMEOUT).click();
    }
}
