package com.whatsapp.web.pages.groups;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 2/3/2017.
 */
public class GroupsPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public GroupsPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Group info link.
     */
    By groupInfoLink = By.cssSelector("a[title='Group info']");

    /**
     * By element for Select user.
     */
    By selectUser = By.cssSelector(".btn-context");

    /**
     * By element for Remove button.
     */
    By removeButton = By.cssSelector("a[title='Remove']");

    /**
     * By element for Close info button.
     */
    By closeInfoButton = By.cssSelector(".icon-x");

    /**
     * Method to click on Group info link.
     */
    public void clickOnGroupInfoLink() {
        findClickableElement(groupInfoLink).click();
    }

    /**
     * Method to Click on user options button.
     */
    public void clickOnUserOptionsButton() {
        findVisibleElement(selectUser).click();
    }

    /**
     * Method to wait until Visible of user.
     */
    public void waitUntilVisibleOfUser() {
        findVisibleElement(selectUser);
    }

    /**
     * Method to click on Remove button.
     */
    public void clickOnRemoveButton() {
        findVisibleElement(removeButton).click();
    }

    /**
     * Method to Move to user.
     * @param user
     */
    public void moveToUser(String user) {
        String xpathString = "(//span[text()='" + user + "'])[1]";
        WebElement element = findVisibleElement(By.xpath(xpathString));
        moveToElement(element);
    }

    /**
     * Method to click on Close info button.
     */
    public void clickOnCloseInfoButton() {
        findClickableElement(closeInfoButton).click();
    }

    /**
     * Method to click on wait until visible of user.
     * @param user
     */
    public void waitUntilVisibleOfUser(String user) {
        String xpathString = "(//span[text()='" + user + "'])[1]";
        findVisibleElement(By.xpath(xpathString));
    }
}
