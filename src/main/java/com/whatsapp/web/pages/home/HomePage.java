package com.whatsapp.web.pages.home;
/**
 * This class Contains all HomePage Features.
 */

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Homepage features.
 * Created by Venu on 1/24/2017.
 */
public class HomePage extends BasePage {
    /**
     * By Element for Homepage text.
     */
    By homePageText = By.cssSelector(".intro-title");


    /**
     * Constructor to Initialize the HomePage.
     * @param driver
     */
    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to wait for Homepage text visible.
     */
    public void waitForHomePageTextVisible() {
        findVisibleElement(homePageText, MAX_WEBELMENT_TIMEOUT);
    }

    /**
     * Method to check whether home page text visible or not.
     * @return boolean isDisplayed
     */
    public boolean isHomePageTextVisible(){
     boolean isDisplayed = isElementDisplayed(homePageText,DEFAULT_WEBELMENT_TIMEOUT);
        return isDisplayed;
    }
}
