package com.whatsapp.web.pages.homepage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/23/2017.
 */
public class HomePage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public HomePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Home page header.
     */
    By homePageHeader = By.cssSelector(".intro-title");

    /**
     * Method to wait until Header displayed.
     */
    public void waitUntilHeaderDisplayed() {
        findVisibleElement(homePageHeader);
    }
}
