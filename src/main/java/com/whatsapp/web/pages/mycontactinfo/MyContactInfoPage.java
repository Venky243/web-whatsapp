package com.whatsapp.web.pages.mycontactinfo;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/24/2017.
 */
public class MyContactInfoPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public MyContactInfoPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Mute checkbox.
     */
    By muteCheckBox = By.cssSelector(".checkbox-container");

    /**
     * Web element for Muted button.
     */
    @FindBy(css = "div.ellipsify .title")
    WebElement mutedLabel;

    /**
     * By element for Block contact.
     */
    By blockContact = By.cssSelector(".icon-settings-blocked");

    /**
     * By element for Unblock contact button.
     */
    By unblockContactButton = By.cssSelector(".icon-settings-blocked-success");

    /**
     * By element for Delete chat button.
     */
    By deleteChat = By.xpath("//span[text()='Delete chat']");

    /**
     * Method to Click on Mute checkbox.
     */
    public void clickOnMuteCheckBox() {
        findClickableElement(muteCheckBox).click();
    }

    /**
     * Method to check whether Muted displayed.
     * @return boolean
     */
    public boolean isMutedLabelDisplayed() {
        return isElementDisplayed(mutedLabel);
    }

    /**
     * Method to click on Block contact.
     */
    public void clickOnBlockContact() {
        boolean blockContactIsDisplayed = isElementDisplayed(blockContact);
        if (blockContactIsDisplayed) {
            findClickableElement(blockContact).click();
        }
    }

    /**
     * Method to click on unblock contact button.
     */
    public void clickOnUnblockContactButton() {
        findClickableElement(unblockContactButton).click();
    }

    /**
     * Method to verify whether unblock contact button displayed.
     * @return boolean.
     */
    public boolean isUnblockContactButtonDisplayed() {
        return isElementDisplayed(unblockContactButton);
    }

    /**
     * Method to verify whether Block contact button displayed.
     * @return boolean.
     */
    public boolean isBlockContactButtonDisplayed() {
        return isElementDisplayed(blockContact);
    }

    /**
     * Method to click on Delete chat button.
     */
    public void clickOnDeleteChatButton() {
        findClickableElement(deleteChat).click();
    }
}
