package com.whatsapp.web.pages.newchat;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SHIVA on 1/24/2017.
 */
public class NewChatPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public NewChatPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * Web element for Search contact.
     */
    @FindBy(css = ".input-search")
    WebElement searchContact;

    /**
     * By element for back button of Blocked contacts.
     */
    By moveToBackPage = By.cssSelector(".btn-close-drawer");

    /**
     * Method to set Search Contact.
     * @param value
     */
    public void setSearchContact(String value) {
        clearAndType(searchContact, value);
    }

    /**
     * Method to click on Searched contact.
     * @param value
     */
    public void clickOnSearchedContact(String value) {
        String cssString = "span[title='" + value + "']";
        driver.findElement(By.cssSelector(cssString)).click();
    }

    /**
     * Method to click on Back button of blocked contacts.
     */
    public void clickOnMoveToBackPageOnNewChat() {
        findClickableElement(moveToBackPage).click();
    }
}
