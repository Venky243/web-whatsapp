package com.whatsapp.web.pages.newgroup;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all New group page features.
 * Created by Venu on 1/29/2017.
 */
public class NewGroupPage extends BasePage {
    /**
     * WebElement for group subject
     */
    @FindBy(css = ".input-text")
    WebElement groupSubject;
    /**
     * By element for arrow button.
     */
    By arrowButton = By.cssSelector(".icon-l");
    /**
     * Constructor to Initialize new group page.
     * @param driver
     */
    public NewGroupPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to click on group subject.
     * @param subject
     */
    public void clickOnGroupSubject(String subject){
        clearAndType(groupSubject,subject);
    }

    /**
     * Method to click on arrow button.
     */
    public void clickOnArrowButton(){
        findVisibleElement(arrowButton,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to check whether Created group name displayed or not.
     * @param name
     * @return boolean
     */
    public boolean isCreatedGroupNameDisplayed(String name){
       String cssExpression = "h2.chat-title span[title='"+name+"']";
    boolean isDisplayed = isElementDisplayed(By.cssSelector(cssExpression));
        return isDisplayed;
    }

    /**
     * Method to wait until invisible of Created group name displayed or not.
     * @param name
     */
    public void waitUntilInvisibleOfCreatedGroupNameDisplayed(String name){
        String cssExpression = "h2.chat-title span[title='"+name+"']";
        waitUntilInvisibleOfBanner(By.cssSelector(cssExpression));
    }

}
