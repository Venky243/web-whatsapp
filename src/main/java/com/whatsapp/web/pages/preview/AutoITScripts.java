package com.whatsapp.web.pages.preview;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

/**
 * Created by SHIVA on 2/10/2017.
 */
public class AutoITScripts extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public AutoITScripts(WebDriver webDriver){
        super(webDriver);
        PageFactory.initElements(webDriver,this);
    }

    /**
     * Method to Select Photo.
     */
    public void selectPhoto() {
        try {
            sleep(1);
            Runtime.getRuntime().exec("src\\main\\resources\\autoitscripts\\photosandvideos2.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to select Video.
     */
    public void selectVideo() {
        try {
            sleep(1);
            Runtime.getRuntime().exec("src\\main\\resources\\autoitscripts\\samplevideo.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to Set document.
     */
    public void setDocument() {
        try {
            sleep(2);
            Runtime.getRuntime().exec("src\\main\\resources\\autoitscripts\\testdocument.exe");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to select second photo.
     */
    public void selectSecondPhoto() {
        try {
            sleep(2);
            String fileLocation = "src\\main\\resources\\autoitscripts\\selectsecondphoto.exe";
            Runtime.getRuntime().exec(fileLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
