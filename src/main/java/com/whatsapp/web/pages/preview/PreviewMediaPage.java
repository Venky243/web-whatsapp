package com.whatsapp.web.pages.preview;

import com.gargoylesoftware.htmlunit.Page;
import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

/**
 * Created by SHIVA on 2/2/2017.
 */
public class PreviewMediaPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public PreviewMediaPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Send photo or Video button.
     */
    By sendButton = By.cssSelector(".icon-send-light");

    /**
     * By element for Add caption field.
     */
    By addCaptionField = By.cssSelector(".input-text");

    /**
     * By element for Add media.
     */
    By addMedia = By.cssSelector(".btn-fill");

    /**
     * By element for Take photo button.
     */
    By takePhotoButton = By.cssSelector(".icon-camera-light");

    /**
     * Method to click on send Photo or Video button.
     */
    public void clickOnSendButton() {
        findClickableElement(sendButton).click();
    }

    /**
     * Method to Set Photo or Video caption.
     * @param value
     */
    public void setCaption(String value) {
        WebElement element = findVisibleElement(addCaptionField);
        clearAndType(element, value);
    }

    /**
     * Method to click on Add media.
     */
    public void clickOnAddMedia() {
        sleep(1);
        findClickableElement(addMedia).click();
    }

    /**
     * Method to wait until caption text displayed or not.
     * @param value
     */
    public void waitUntilCaptionTextDisplayed(String value) {
        String xpathString = "//div[text()='" + value + "']";
        findVisibleElement(By.xpath(xpathString));
    }

    /**
     * Method to wait until photo displayed or not.
     */
    public void waitUntilPhotoOrVideoDisplayed() {
        String xpathString = "(//div[@class='media-thumb-body'])[2]";
        findVisibleElement(By.xpath(xpathString));

    }

    /**
     * Method to click on Take photo button.
     */
    public void clickOnTakePhotoButton() {
        findClickableElement(takePhotoButton).click();
    }
}
