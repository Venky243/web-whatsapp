package com.whatsapp.web.pages.profileandstatus;

import com.whatsapp.web.pages.BasePage;
import com.whatsapp.web.enums.UserProfileEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This Page contains all ProfileAndStatusPage features.
 * Created by Venu on 1/23/2017.
 */
public class ProfileAndStatusPage extends BasePage {
    /**
     * Web element for User profile name.
     */
    @FindBy(css = "div[class='input input-text']")
    WebElement userProfileName;
    /**
     * Web element for Edit user profile name.
     */
    @FindBy(css = "div.input.input-text")
    WebElement editUserProfileName;
    /**
     * By element for Edit user profile name pencil.
     */
    By editUserProfileNamePencil = By.cssSelector("span.control-button-container");
    /**
     * By element for Edit status pencil.
     */
    By editStatusPencil = By.cssSelector(".animate-enter1:nth-child(4) span.icon-pencil");
    /**
     * Web element for Save profile name.
     */
    @FindBy(css = ".icon-checkmark")
    WebElement saveButton;
    /**
     * Web element for Edit status.
     */
    @FindBy(css = ".animate-enter1:nth-child(4) div.input-text")
    WebElement editStatus;
    /**
     * By element for Change profile photo button.
     */
    By changeProfilePhotoButton  = By.cssSelector(".avatar-body .object-fit-cover");
    /**
     * By element for Done button.
     */
    By doneButton = By.cssSelector(".button");
    /**
     * By Element for Profile viewer image.
     */
    By profileViewerImage = By.cssSelector(".profile-viewer-img");
    /**
     * By element for profile viewer close button.
     */
    By profileViewerCloseButton = By.cssSelector(".object-fit-scaledown");
    /**
     * By element for remove profile photo button.
     */
    By removeProfilePhotoButton = By.cssSelector(".btn-default");
    /**
     * By element for reset add profile photo.
     */
    By resetAddProfilePhoto = By.cssSelector(".icon-camera-light");
    /**
     * By element for new chat button.
     */
    By newChatButton = By.cssSelector(".icon-chat");


    /**
     * Initialize Constructor.
     * @param driver
     */
    public ProfileAndStatusPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to Edit your name.
     * @param name
     */
    public void editYourName(String name) {
        findClickableElement(editUserProfileNamePencil).click();
        clearAndType(userProfileName, name);
    }

    /**
     * Method to Edit your status.
     * @param status
     */
    public void editYourStatus(String status) {
        findClickableElement(editStatusPencil).click();
        clearAndType(editStatus, status);
    }

    /**
     * Method to click on Save profile name.
     */
    public void clickOnSaveProfileName() {
        saveButton.click();
    }

    /**
     * Method to click on Save status.
     */
    public void clickOnSaveStatus() {
        saveButton.click();
    }

    /**
     * Method to Get username text.
     * @return text
     */
    public String getUserNameText() {
        String text = getText(userProfileName);
        return text;
    }

    /**
     * Method to click on Change profile photo button.
     */
    public void clickOnChangeProfilePhotoButton() {
        moveToElementAndClick(findClickableElement(changeProfilePhotoButton, MAX_WEBELMENT_TIMEOUT));
    }

    /**
     * Method to click on Done button.
     */
    public void clickOnDoneButton() {
        findClickableElement(doneButton).click();
    }

    /**
     * Method to Update user profile .
     */
    public void updateUserProfile(UserProfileEnum userProfileEnum) {
        String cssValue = "a[title='"+ userProfileEnum.getPhotoActions()+"']";
        findClickableElement(By.cssSelector(cssValue), MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to check whether profile viewer image displayed or not.
     */
    public boolean isProfileViewerImageDisplayed() {
        boolean isDisplayed = isElementDisplayed(profileViewerImage);
        return isDisplayed;
    }

    /**
     * Method to click on profile viewer image close button.
     */
    public void clickOnProfileViewerImageCloseButton() {
        findClickableElement(profileViewerCloseButton, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on remove profile photo button.
     */
    public void clickOnRemoveProfilePhotoButton() {
        findClickableElement(removeProfilePhotoButton, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to wait until invisible of profile viewer image.
     */
    public void waitUntilInvisibleOfProfileViewerImage() {
        waitUntilInvisibleOfBanner(profileViewerImage);
    }

    /**
     * Method to click on reset add profile photo.
     */
    public void clickOnResetAddProfilePhoto() {
        findClickableElement(resetAddProfilePhoto, MAX_WEBELMENT_TIMEOUT).click();
    }
}
