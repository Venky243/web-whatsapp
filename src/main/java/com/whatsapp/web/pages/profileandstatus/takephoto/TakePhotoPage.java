package com.whatsapp.web.pages.profileandstatus.takephoto;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Take photo page features.
 * Created by Venu on 2/2/2017.
 */
public class TakePhotoPage extends BasePage {
    /**
     * By element for Take photo title text.
     */
    By takePhotoTitleText = By.cssSelector(".drawer-header-popup .drawer-title-body");
    /**
     * By element for Camera icon.
     */
    By cameraIcon = By.cssSelector(".icon-camera-light");
    /**
     * By element for Camera close button.
     */
    By cameraCloseButton = By.cssSelector(".icon-x-light");
    /**
     *Initialize Constructor.
     * @param driver
     */
    public TakePhotoPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    /**
     * Method to check whether Take photo text displayed or not.
     * @return isDisplayed
     */
    public boolean isTakePhotoTextDisplayed(){
        boolean isDisplayed = isElementDisplayed(takePhotoTitleText);
        return isDisplayed;
    }

    /**
     * Method to check whether Camera icon displayed or not.
     * @return isDisplayed
     */
    public boolean isCameraIconDisplayed(){
        boolean isDisplayed = isElementDisplayed(cameraIcon);
        return isDisplayed;
    }

    /**
     * Method to click on Camera close button.
     */
    public void clickOnCameraCloseButton(){
        findClickableElement(cameraCloseButton,MAX_WEBELMENT_TIMEOUT).click();
    }
}
