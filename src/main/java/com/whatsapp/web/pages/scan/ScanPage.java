package com.whatsapp.web.pages.scan;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Scan page features.
 * Created by Venu on 1/24/2017.
 */
public class ScanPage extends BasePage {

    /**
     * By element for Scan logo
     */
    By scanLogo = By.cssSelector(".icon-logo");

    /**
     * Constructor to Initialize the ScanPage.
     * @param driver
     */
    public ScanPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to wait for scan logo visible.
     * @return boolean
     */
    public boolean isScanLogoDisplayed() {
        sleep(4);
        return isElementDisplayed(scanLogo,DEFAULT_WEBELMENT_TIMEOUT);
    }


}
