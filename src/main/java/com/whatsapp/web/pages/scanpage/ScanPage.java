package com.whatsapp.web.pages.scanpage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by SHIVA on 1/23/2017.
 */
public class ScanPage extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public ScanPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By element for Whatsapp Logo.
     */
    By whatsappLogo = By.cssSelector(".icon-logo");

    /**
     * Method to wait until Scan Logo displayed.
     */
    public void waitUntilScanLogoDisplayed() {
        findVisibleElement(whatsappLogo);
    }
}
