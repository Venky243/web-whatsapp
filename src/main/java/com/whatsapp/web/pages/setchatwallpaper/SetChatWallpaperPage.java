package com.whatsapp.web.pages.setchatwallpaper;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Set chat wallpaper page features.
 * Created by Venu on 1/29/2017.
 */
public class SetChatWallpaperPage extends BasePage {
    /**
     * WebElement for first colour in list.
     */
    @FindBy(css = "span[style='background-color: rgb(204, 235, 220);']")
    WebElement firstColourInList;
    /**
     * WebElement for default colour.
     */
    @FindBy(css = ".wallpaper-default-title")
    WebElement defaultColour;
    /**
     * By element for selected colour.
     */
    By selectedColour = By.cssSelector(".canvas-active");

    /**
     * Constructor to Initialize Set chat wall paper page.
     * @param driver
     */
    public SetChatWallpaperPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to click on first colour in list.
     */
    public void setColourInList(String colour) {
        String cssValue = "span[style='background-color: rgb("+colour+");']";
        moveToElementAndClick(findVisibleElement(By.cssSelector(cssValue)));
    }

    /**
     * Method to verify selected colour displayed.
     * @return isDisplayed
     */
    public boolean isSelectedColourDisplayed() {
        boolean isDisplayed = isElementDisplayed(selectedColour);
        return isDisplayed;
    }

    /**
     * Method to click on default colour.
     */
    public void clickOnDefaultColour(){
        defaultColour.click();
    }

}
