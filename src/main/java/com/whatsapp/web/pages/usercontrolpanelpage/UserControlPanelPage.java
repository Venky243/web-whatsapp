package com.whatsapp.web.pages.usercontrolpanelpage;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all User control panel page Features.
 * Created by Venu on 1/23/2017.
 */
public class UserControlPanelPage extends BasePage {

    /**
     * By Element for User profile picture image
     */
    By userProfilePictureImage = By.cssSelector(".pane-list-user .icon-user-default .avatar-body");
    /**
     * By Element for reset user profile picture image
     */
    By resetUserProfilePictureImage = By.cssSelector(".pane-list-user .avatar-body");
    /**
     * By Element for user control panel menu button
     */
    By userControlPanelMenuButton = By.cssSelector("button[title=Menu]");
    /**
     * By Element for settings link.
     */
    By settingsLink = By.cssSelector("a[title='Settings']");
    /**
     * By Element for New group link.
     */
    By newGroupLink = By.cssSelector("a[title='New group']");
    /**
     * By Element for Logout link.
     */
    By logoutLink = By.cssSelector("a[title='Log out']");

    /**
     *By element for Search group
     */
    By searchGroup = By.cssSelector(".input-search");
    /**
     *By element for Searched group name.
     */
    By searchedGroupName = By.cssSelector(".chat-title .matched-text");

    /**
     * By element for New chat button.
     */
    By newChatButton = By.cssSelector(".icon-chat");

    /**
     * By element for Settings.
     */
    By settings = By.cssSelector("a[title='Settings']");

    /**
     * By element for Blocked contacts.
     */
    By blockedContacts = By.cssSelector(".icon-settings-blocked");

    /**
     * Method to Click on New chat button.
     */
    public void clickOnNewChatButton() {
        findClickableElement(newChatButton).click();
    }

    /**
     * Constructor.
     *
     * @param webDriver
     */
    public UserControlPanelPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * Method to click on Blocked contacts.
     */
    public void clickOnBlockedContacts() {
        findClickableElement(blockedContacts).click();
    }

    /**
     * Method to verify whether blocked Contact displayed.
     *
     * @param user
     * @return boolean.
     */
    public boolean isBlockedContactDisplayed(String user) {
        return isElementDisplayed(By.cssSelector("span[title='" + user + "']"));
    }

    /**
     * Method to click on reset user profile picture.
     */
    public void clickOnResetUserProfilePicture() {
        findClickableElement(resetUserProfilePictureImage).click();
    }

    /**
     * Method to Click on user control panel menu.
     */
    public void clickOnUserControlPanelMenuButton() {
        sleep(2);
        findClickableElement(userControlPanelMenuButton, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on settings link.
     */
    public void clickOnSettingsLink() {
        findVisibleElement(settingsLink, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on new group link.
     */
    public void clickOnNewGroupLink() {
        findVisibleElement(newGroupLink, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to click on logout link.
     */
    public void clickOnLogoutLink() {
        findClickableElement(logoutLink, MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to clickOnUser profile picture.
     */
    public void clickOnUserProfilePicture() {
        findClickableElement(userProfilePictureImage, DEFAULT_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to check whether new chat button displayed or not.
     * @return boolean is Displayed
     */
    public boolean isNewChatButtonDisplayed() {
        boolean isDisplayed = isElementDisplayed(newChatButton);
        return isDisplayed;
    }

    /**
     * Method to click on Search button.
     */
    public void clickOnSearchButton(){
        findClickableElement(searchGroup,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to Set Group name.
     * @param groupName
     */
    public void setGroupName(String groupName){
        clearAndType(findVisibleElement(searchGroup),groupName);
    }

    /**
     * Method to click on Searched group name.
     */
    public void clickOnSearchedGroupName(){
        sleep(2);
        findClickableElement(searchedGroupName,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method to wait until Visible of new chat button.
     */
    public void waitUntilVisibleOfNewChatButton(){
        findVisibleElement(newChatButton);
    }

}
