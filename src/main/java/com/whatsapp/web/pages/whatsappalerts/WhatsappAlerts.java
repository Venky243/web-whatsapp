package com.whatsapp.web.pages.whatsappalerts;

import com.whatsapp.web.enums.WhatsappAlertsEnum;
import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by SHIVA on 1/24/2017.
 */
public class WhatsappAlerts extends BasePage {
    /**
     * Constructor.
     * @param webDriver
     */
    public WhatsappAlerts(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    /**
     * By Element for ChatPage unmuted.
     */
//    By chatUnmuted = By.xpath("//span[text()='Chat unmuted']");

    /**
     * By element for block contact Undo button.
     */
    By blockOrUnblockContactUndoButton = By.cssSelector("button.action");

    /**
     * By element for Undo.
     */
    By undo = By.cssSelector("div.toast .action");

    /**
     * By element for chat deleted alert.
     */
//    By chatDeletedAlert = By.xpath("//span[text()='Chat deleted']");

    /**
     * By element for stared Message alert.
     */
//    By messageStarredAlert = By.xpath("");

    /**
     * By element for un starred Message alert.
     */
//    By messageUnstarredAlert = By.xpath("//span[text()='Message unstarred']");

    /**
     * By element for Message deleted.
     */
//    By messageDeletedAlert = By.xpath("//span[text()='Message deleted']");

    /**
     * By element for Chat cleared.
     */
//    By chatClearedAlert = By.xpath("//span[text()='Chat cleared']");

    /**
     * Method to check whether ChatPage unmuted alert displayed.
     * @return boolean.
     */
    public boolean isChatUnmutedAlertDisplayed(WhatsappAlertsEnum alert) {
        String xpathString = "//span[text()='"+alert.getAlertType()+"']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to check whether Block contact alert displayed.
     * @param contactName
     * @return boolean.
     */
    public boolean isBlockContactAlertDisplayed(String contactName) {
        String xpathString = "//span[text()='" + contactName + " blocked']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to Check whether unblock contact alert displayed.
     * @param contactName
     * @return boolean.
     */
    public boolean isUnblockedContactAlertDisplayed(String contactName) {
        String xpathString = "//span[text()='" + contactName + " unblocked']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to click on Block contact Undo button.
     */
    public void clickOnBlockOrUnblockContactUndoButton() {
        String cssString = "button.action";
        findClickableElement(By.cssSelector(cssString)).click();
    }

    /**
     * Method to click on Unblock contact Undo button.
     */
    public void clickOnUnBlockContactUndoButton() {
        findClickableElement(undo).click();
    }

    /**
     * Method to wait until Invisible of Block or Unblock contact undo button alert.
     */
    public void waitUntilInvisibleOfBlockOrUnblockContactUndoButtonAlert() {
        waitUntilInvisibleOfBanner(blockOrUnblockContactUndoButton);
    }

    /**
     * Method to wait until Visible of alert.
     */
    public void waitUntilvisibleOfAlert() {
        String xpathString = "//button[text()='Undo']//preceding::span";
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathString)));

    }

    /**
     * Method to check whether chat deleted Alert displayed.
     * @return boolean.
     */
    public boolean isChatDeletedAlertDisplayed(WhatsappAlertsEnum alert) {
        String xpathString = "//span[text()='"+alert.getAlertType()+"']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to verify message Stared alert displayed.
     * @return boolean.
     */
    public boolean isMessageStaredAlertDisplayed(WhatsappAlertsEnum alert) {
        String xpathString = "//span[text()='"+alert.getAlertType()+"']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to click on Message Stared or Unstared Undo Button.
     */
    public void clickOnMessageStaredOrUnstaredUndoButton() {
        findClickableElement(undo).click();
    }

    /**
     * Method to verify whether unstarred message alert displayed.
     * @return boolean.
     */
    public boolean isUnstarredMessageAlertDisplayed(WhatsappAlertsEnum alert) {
        String xpathString = "//span[text()='"+alert.getAlertType()+"']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to verify whether Message deleted alert displayed.
     * @return boolean.
     */
    public boolean isMessageDeletedAlertDisplayed(WhatsappAlertsEnum alert) {
        String xpathString = "//span[text()='"+alert.getAlertType()+"']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to verify whether Chat cleared alert displayed.
     * @return boolean.
     */
    public boolean isChatClearedAlertDisplayed(WhatsappAlertsEnum alert) {
        String xpathString = "//span[text()='"+alert.getAlertType()+"']";
        return isElementDisplayed(By.xpath(xpathString));
    }

    /**
     * Method to wait until invisible of Alert.
     * @param value
     */
    public void waitUntilInvisibleOfAlert(String value) {
        waitUntilInvisibleOfBanner(By.xpath("//span[text()='"+value+"']"));
    }
}
