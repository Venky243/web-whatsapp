package com.whatsapp.web.pages.whatsappalerts;

import com.whatsapp.web.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * This class contains all Whatsapp alerts page features.
 * Created by Venu on 1/29/2017.
 */
public class WhatsappAlertsPage extends BasePage {

    /**
     * By element for Profile photo removed alert.
     */
    By profilePhotoRemovedAlert = By.xpath("//span[text()='Profile photo removed']");
    /**
     * By element for profile photo set alert.
     */
    By profilePhotoSetAlert = By.xpath("//span[text()='Profile photo set']");
    /**
     * By element for status changed alert.
     */
    By statusChangedAlert = By.xpath("//span[text()='Status changed']");
    /**
     * By element for chat wallpaper set alert.
     */
    By chatWallpaperSetAlert = By.xpath("//span[text()='Chat wallpaper set']");
    /**
     * By element for Group created alert.
     */
    By groupCreatedAlert = By.xpath("//span[text()='Created group']");
    /**
     * By element for Group deleted alert.
     */
    By groupDeletedAlert = By.xpath("//span[text()='Group deleted']");
    /**
     * By element for Group exited alert.
     */
    By groupExitedAlert = By.xpath("//span[text()='Exited group']");
    /**
     * By element for group icon set alert.
     */
    By groupIconSetAlert = By.xpath("//span[text()='Group icon set']");
    /**
     * By element for Setting group icon alert.
     */
    By settingGroupIconAlert = By.xpath("//span[text()='Setting group icon']");
    /**
     * By element for Renaming group alert.
     */
    By renamingGroup = By.xpath("//span[text()='Renaming group']");
    /**
     * By element for clearing chat alert.
     */
    By clearingChatAlert = By.xpath("//span[text()='Clearing chat']");
    /**
     * By element for Clear chat alert.
     */
    By chatClearedAlert = By.xpath("//span[text()='Chat cleared']");
    /**
     * By element for Undo button alert.
     */
    By undoButtonAlert = By.cssSelector(".action");

    /**
     * Constructor to Initialize Whatsapp alerts page.
     * @param driver
     */
    public WhatsappAlertsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Method to Check Whether Status changed alert displayed or not.
     * @return boolean
     */
    public boolean isStatusChangedAlertDisplayed() {
        return isElementDisplayed(statusChangedAlert);
    }

    /**
     * Method to check whether Group created alert displayed or not.
     * @return boolean
     */
    public boolean isGroupCreatedAlertDisplayed() {
        return isElementDisplayed(groupCreatedAlert);
    }

    /**
     * Method to wait until invisible of Group created alert.
     */
    public void waitUntilInvisibleOfGroupCreatedAlert(){
        waitUntilInvisibleOfBanner(groupCreatedAlert);
    }

    /**
     * Method to check whether Group deleted alert displayed or not.
     * @return boolean
     */
    public boolean isGroupDeletedAlertDisplayed() {
        return isElementDisplayed(groupDeletedAlert);
    }

    /**
     * Method to wait until invisible of group deleted alert.
     */
    public void waitUntilInvisibleOfGroupDeletedAlert(){
        waitUntilInvisibleOfBanner(groupDeletedAlert);
    }

    /**
     * Method to check whether Exited group alert displayed or not.
     * @return boolean
     */
    public boolean isExitedGroupAlertDisplayed() {
        return isElementDisplayed(groupExitedAlert);
    }

    /**
     * Method to check whether Chat wallpaper set alert displayed or not.
     * @return boolean
     */
    public boolean isChatWallpaperSetAlertDisplayed() {
        return isElementDisplayed(chatWallpaperSetAlert);
    }

    /**
     * Method to check whether Profile photo set alert displayed or not.
     * @return boolean
     */
    public boolean isProfilePhotoSetAlertDisplayed() {
        return isElementDisplayed(profilePhotoSetAlert);
    }

    /**
     * Method to check whether Profile photo removed alert displayed or not.
     * @return boolean
     */
    public boolean isProfilePhotoRemovedAlertDisplayed() {
        return isElementDisplayed(profilePhotoRemovedAlert);
    }

    /**
     * Method to wait until invisible of photo removed alert.
     */
    public void waitUntilInvisibleOfPhotoRemovedAlert() {
        waitUntilInvisibleOfBanner(profilePhotoRemovedAlert);
    }

    /**
     * Method to wait until invisible of profile photo set alert.
     */
    public void waitUntilInvisibleOfProfilePhotoSetAlert() {
        waitUntilInvisibleOfBanner(profilePhotoSetAlert);
    }

    /**
     * Method to wait until invisible of status changed alert.
     */
    public void waitUntilInvisibleOfStatusChangedAlert() {
        waitUntilInvisibleOfBanner(statusChangedAlert);
    }

    /**
     * Method to wait until invisible of chat wallpaper alert.
     */
    public void waitUntilInvisibleOfChatWallpaperSetAlert(){
        waitUntilInvisibleOfBanner(chatWallpaperSetAlert);
    }

    /**
     * Method to check whether Contact added alert displayed or not.
     * @param contactName
     * @return boolean
     */
    public boolean isContactAddedAlertDisplayed(String contactName){
        String xpath = "//span[text()='"+contactName+" added']";
      return   isElementDisplayed(By.xpath(xpath));
    }

    /**
     * Method to check whether Contact removed alert displayed or not.
     * @param contactName
     * @return boolean
     */
    public boolean isContactRemovedAlertDisplayed(String contactName){
        String xpath = "//span[text()='"+contactName+" removed']";
        return isElementDisplayed(By.xpath(xpath));
    }

    /**
     * Method to wait until invisible of Contact removed alert.
     * @param contactName
     */
    public void waitUntilInvisibleOfContactRemovedAlert(String contactName){
        String xpath = "//span[text()='"+contactName+" removed']";
        waitUntilInvisibleOfBanner(By.xpath(xpath));
    }

    /**
     * Method to wait until invisible of contact added alert displayed.
     * @param contactName
     */
    public void waitUntilInvisibleOfContactAddedAlertDisplayed(String contactName){
        String xpath = "//span[text()='"+contactName+" added']";
        waitUntilInvisibleOfBanner(By.xpath(xpath));
    }

    /**
     * Method to check whether Group icon set alert displayed or not.
     * @return boolean.
     */
    public boolean isGroupIconSetAlertDisplayed(){
       return  isElementDisplayed(groupIconSetAlert,MAX_WEBELMENT_TIMEOUT);
    }

    /**
     *Method to wait until invisible of Group icon set alert displayed.
     */
    public void waitUntilInvisibleOfGroupIconSetAlertDisplayed(){
        waitUntilInvisibleOfBanner(groupIconSetAlert);
    }


    /**
     * Method to wait untill invisible of Setting group ico alert.
     */
    public void waitUntilInvisibleOfSettingGroupIconAlert(){
        waitUntilInvisibleOfBanner(settingGroupIconAlert);
    }

    /**
     * Method to check whether  on Group renamed alert displayed or not.
     * @param groupName
     */
    public void isGroupRenamedAlertDisplayed(String groupName){
        String xpath = "//span[text()='Group renamed to "+groupName+"']";
        isElementDisplayed(By.xpath(xpath));
    }

    /**
     * Method to click on Undo button.
     */
    public void clickOnUndoButtonAlert(){
        findClickableElement(undoButtonAlert,MAX_WEBELMENT_TIMEOUT).click();
    }

    /**
     * Method ot Wait until invisible of Undo button alert.
     */
    public void waitUntilInvisibleOfUndoButtonAlert(){
        waitUntilInvisibleOfBanner(undoButtonAlert);
    }

    /**
     * Method to wait until invisible of Renaming group alert.
     */
    public void waitUntilInvisibleOfRenamingGroupAlert(){
        waitUntilInvisibleOfBanner(renamingGroup);
    }

    public void waitUntilInvisibleOfClearingChatAlert(){
        waitUntilInvisibleOfBanner(clearingChatAlert);
    }

    /**
     * Method to check whether Chat cleared alert displayed or not.
     * @return boolean
     */
    public boolean isChatClearedAlertDisplayed(){
       return isElementDisplayed(chatClearedAlert);
    }

    /**
     * Method to wait until invisible of Chat cleared alert.
     */
    public void waitUntilInvisibleOfChatClearedAlert(){
        waitUntilInvisibleOfBanner(chatClearedAlert);
    }

    /**
     * Method to wait until invisible of Exited group alert.
     */
    public void waitUntilInvisibleOfExitedGroupAlert(){
        waitUntilInvisibleOfBanner(groupExitedAlert);
    }
}
