package com.whatsapp.web.util;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * This class contains all Property util features.
 */
public class PropertyUtil {
    /**
     * Instance variable for properties.
     */
    Properties properties = new Properties();

    /**
     * Constructor to load the all property files.
     */
    public PropertyUtil() {
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("default.properties"));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("whatsapp.properties"));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("whatsappweb.properties"));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to get the property value.
     * @param property
     * @return
     */
    public String getProperty(final String property) {
        return properties.getProperty(property);
    }

}
