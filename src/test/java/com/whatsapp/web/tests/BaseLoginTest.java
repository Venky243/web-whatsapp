package com.whatsapp.web.tests;

import com.whatsapp.web.pages.home.HomePage;
import com.whatsapp.web.pages.scan.ScanPage;
import com.whatsapp.web.util.TestLogger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

/**
 * This class contains all the BaseLoginTest Features.
 * Created by Venu on 1/24/2017.
 */
public class BaseLoginTest extends BaseTest {

    /**
     * Test Logger.
     */
    private static final TestLogger LOG = TestLogger.getLogger(BaseLoginTest.class);

    /**
     * Instance variable for Scanpage.
     */
    ScanPage scanPage;
    /**
     * Instance Variable for Homepage.
     */
    HomePage homePage;

    /**
     * Method to Scan nad Navigate to Homepage.
     */
    @BeforeSuite
    public void setUp() {
        LOG.logInfo("Before Login Test - setUp()");
        this.webDriver = getWebDriver();
        this.webDriver.get(getHomePageURL());
        this.webDriver.manage().window().maximize();
    }

    @BeforeTest
    public void beforeTest(){
        LOG.logInfo("Before Login Test - scanAndNavigateToHomePage()");
        scanPage = new ScanPage(webDriver);
        homePage = new HomePage(webDriver);
        boolean isScanLogoDisplayed = scanPage.isScanLogoDisplayed();
        LOG.logInfo("Verify Scan Logo Displayed: "+isScanLogoDisplayed);
        Assert.assertTrue(isScanLogoDisplayed,"Scan Logo is not displayed.");
        boolean isHomePageDisplayed = homePage.isHomePageTextVisible();
        LOG.logInfo("Verify Home page Displayed: "+isHomePageDisplayed);
        Assert.assertTrue(isHomePageDisplayed,"Home Page is not Visible.");
    }

    @AfterTest(alwaysRun = true)
    public void tearDown(){
        this.webDriver.quit();
    }
}
