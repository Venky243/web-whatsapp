package com.whatsapp.web.tests;

import com.whatsapp.web.util.TestLogger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.whatsapp.web.util.PropertyUtil;
import org.testng.annotations.*;

/**
 *This class contains all Base test features
 */
public class BaseTest {
    /**
     * Test Logger.
     */
    private static final TestLogger LOG = TestLogger.getLogger(BaseTest.class);
    /**
     * Instance Variable for Property util
     */
    public PropertyUtil propertyUtil;

    /**
     * Instance variable for WebDriver.
     */
    protected static WebDriver webDriver;

    /**
     * Instance Variable for Default web element timeout.
     */
    protected final static int DEFAULT_WEBELMENT_TIMEOUT = 30;

    /**
     * Constructor.
     */
    public BaseTest() {
        propertyUtil = new PropertyUtil();
    }

    /**
     * Method to get the home page URL.
     *
     * @return String.
     */
    public String getHomePageURL() {
        final String url = this.propertyUtil.getProperty("url");
        return url;
    }

    /**
     * Method to get the WebDriver object.
     *
     * @return driver
     */
    public WebDriver getWebDriver() {
        final String driver = this.propertyUtil.getProperty("driver");
        this.webDriver = getWebDriver(driver);
        return this.webDriver;
    }

    /**
     * Method to initialize webdriver.
     * @param driver
     * @return webdriver
     */
    public WebDriver getWebDriver(final String driver) {
        if (driver.equals("firefox")) {
            System.setProperty("webdriver.firefox.marionette","src\\main\\resources\\binaries\\geckodriver.exe");
            this.webDriver = new FirefoxDriver();
        } else if (driver.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver",
                    "src\\main\\resources\\binaries\\chromedriver.exe");
            // ChromeOptions for allowing camera pop-up.
            ChromeOptions options= new ChromeOptions();
            options.addArguments("--use-fake-ui-for-media-stream");
            this.webDriver = new ChromeDriver(options);
        } else if (driver.equals("ie")) {
            System.setProperty("webdriver.ie.driver",
                    "src/main/resources/binaries/IEDriverServer.exe");
            this.webDriver = new InternetExplorerDriver();
        } else {
            this.webDriver = new HtmlUnitDriver();
        }
        return this.webDriver;
    }
}
