package com.whatsapp.web.tests.attachment;

import com.whatsapp.web.pages.camera.CameraPage;
import com.whatsapp.web.pages.chat.ChatPage;
import com.whatsapp.web.pages.confirmpages.ClearOrDeleteMessageConfirmationPage;
import com.whatsapp.web.pages.confirmpages.DeleteChatConfirmPage;
import com.whatsapp.web.pages.confirmpages.RemoveUserConfirmPage;
import com.whatsapp.web.pages.confirmpages.SendContactConfirmPage;
import com.whatsapp.web.enums.ClearAndDeleteMessageAlert;
import com.whatsapp.web.pages.contact.ContactPage;
import com.whatsapp.web.pages.contactinfopage.ContactInfoPage;
import com.whatsapp.web.pages.contactlist.ContactListPage;
import com.whatsapp.web.pages.document.DocumentPage;
import com.whatsapp.web.pages.forwardmessage.ForwardMessagePage;
import com.whatsapp.web.pages.groups.GroupsPage;
import com.whatsapp.web.pages.mycontactinfo.MyContactInfoPage;
import com.whatsapp.web.pages.newchat.NewChatPage;
import com.whatsapp.web.pages.preview.AutoITScripts;
import com.whatsapp.web.pages.preview.PreviewMediaPage;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.pages.whatsappalerts.WhatsappAlerts;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.tests.contactmenutest.ContactMenuTest;
import com.whatsapp.web.util.TestLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by SHIVA on 2/2/2017.
 */
public class AttachmentTest extends BaseLoginTest {
    /**
     * Instance variable for Test logger.
     */
    private static final TestLogger logger = TestLogger.getLogger(AttachmentTest.class);
    /**
     * Instance variable for Contact list.
     */
    ContactListPage contactListPage;
    /**
     * Instance variable for User control panel page.
     */
    UserControlPanelPage userControlPanelPage;
    /**
     * Instance variable for New chat.
     */
    NewChatPage newChatPage;
    /**
     * Instance variable for My contact info.
     */
    MyContactInfoPage myContactInfoPage;
    /**
     * Instance variable for Alerts.
     */
    WhatsappAlerts whatsappAlerts;
    /**
     * Instance variable for Contact info page.
     */
    ContactInfoPage contactInfoPage;
    /**
     * Instance variable for ChatPage.
     */
    ChatPage chatPage;
    /**
     * Instance variable for Forward message page.
     */
    ForwardMessagePage forwardMessagePage;
    /**
     * Instance variable for Preview page.
     */
    PreviewMediaPage previewMediaPage;
    /**
     * Instance variable for Camera page.
     */
    CameraPage cameraPage;
    /**
     * Instance variable for Document page.
     */
    DocumentPage documentPage;
    /**
     * Instance variable for Contact page.
     */
    ContactPage contactPage;
    /**
     * Instance variable for Group page.
     */
    GroupsPage groupsPage;
    /**
     * Instance variable for Send contact confirm page.
     */
    SendContactConfirmPage sendContactConfirmPage;
    /**
     * Instance variable for Clear messages confirm page.
     */
    ClearOrDeleteMessageConfirmationPage clearOrDeleteMessageConfirmationPage;
    /**
     * Instance variable for remove user confirm page.
     */
    RemoveUserConfirmPage removeUserConfirmPage;
    /**
     * Instance variable for Autoit scripts page.
     */
    AutoITScripts autoITScripts;
    /**
     * Instance variable for Delete chat confirm page.
     */
    DeleteChatConfirmPage deleteChatConfirmPage;

    @BeforeClass(alwaysRun = true)
    public void setupBrowser() {
        contactListPage = new ContactListPage(webDriver);
        userControlPanelPage = new UserControlPanelPage(webDriver);
        newChatPage = new NewChatPage(webDriver);
        myContactInfoPage = new MyContactInfoPage(webDriver);
        whatsappAlerts = new WhatsappAlerts(webDriver);
        contactInfoPage = new ContactInfoPage(webDriver);
        chatPage = new ChatPage(webDriver);
        forwardMessagePage = new ForwardMessagePage(webDriver);
        previewMediaPage = new PreviewMediaPage(webDriver);
        cameraPage = new CameraPage(webDriver);
        documentPage = new DocumentPage(webDriver);
        contactPage = new ContactPage(webDriver);
        groupsPage = new GroupsPage(webDriver);
        sendContactConfirmPage = new SendContactConfirmPage(webDriver);
        clearOrDeleteMessageConfirmationPage = new ClearOrDeleteMessageConfirmationPage(webDriver);
        removeUserConfirmPage = new RemoveUserConfirmPage(webDriver);
        autoITScripts = new AutoITScripts(webDriver);
        deleteChatConfirmPage = new DeleteChatConfirmPage(webDriver);

        logger.logTestStep("Click on new ChatPage button.");
        userControlPanelPage.clickOnNewChatButton();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Set search Contact." + user);
        newChatPage.setSearchContact(user);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(user);
    }

    /**
     * AttachmentTest-@T1:Photos attachments in attachments.
     */
    @Test(description = "Photos attachment in attachments.")
    public void verifyPhotosAttachment() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Photos and Videos button.");
        contactInfoPage.clickOnPhotosAndVideosButton();
        logger.logTestStep("Select Photo.");
        autoITScripts.selectPhoto();

        logger.logTestStep("Store Chat input text.");
        String captionText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(captionText);
        logger.logTestStep("Wait until caption text displayed.");
        previewMediaPage.waitUntilCaptionTextDisplayed(captionText);

        logger.logTestStep("Click on Add media.");
        previewMediaPage.clickOnAddMedia();
        logger.logTestStep("Select Second Photo");
        autoITScripts.selectSecondPhoto();
        logger.logTestStep("Wait until photo displayed.");
        previewMediaPage.waitUntilPhotoOrVideoDisplayed();
        logger.logTestStep("Store Chat input text.");
        String secondCaptionText = RandomStringUtils.randomAlphanumeric(7);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(secondCaptionText);
        logger.logTestStep("Wait until caption text displayed.");
        previewMediaPage.waitUntilCaptionTextDisplayed(secondCaptionText);
        logger.logTestStep("Click on Send photo or Video button.");
        previewMediaPage.clickOnSendButton();

        logger.logTestStep("Check whether Caption text displayed.");
        boolean isCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(captionText);
        logger.logTestVerificationStep("Verify whether Caption text displayed." + isCaptionTextDisplayed);
        Assert.assertTrue(isCaptionTextDisplayed, "Caption text not Displayed.");
        logger.logTestStep("Check whether second photo Caption text displayed.");
        boolean isSecondPhotoCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(secondCaptionText);
        logger.logTestVerificationStep("Verify whether second photo Caption text displayed." + isSecondPhotoCaptionTextDisplayed);
        Assert.assertTrue(isSecondPhotoCaptionTextDisplayed,"Second photo caption text not Displayed.");
    }

    /**
     * AttachmentTest-@T2:Photos attachment in attachments.
     */
    @Test(description = "Videos attachment in attachments.")
    public void verifyVideosAttachment() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Photos and Videos button.");
        contactInfoPage.clickOnPhotosAndVideosButton();
        logger.logTestStep("Select Photo.");
        autoITScripts.selectVideo();

        logger.logTestStep("Store Chat input text.");
        String captionText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(captionText);
        logger.logTestStep("Wait until caption text displayed.");
        previewMediaPage.waitUntilCaptionTextDisplayed(captionText);

        logger.logTestStep("Click on Add media.");
        previewMediaPage.clickOnAddMedia();
        logger.logTestStep("Select Second Photo");
        autoITScripts.selectVideo();
        logger.logTestStep("Wait until photo displayed.");
        previewMediaPage.waitUntilPhotoOrVideoDisplayed();
        logger.logTestStep("Store Chat input text.");
        String secondCaptionText = RandomStringUtils.randomAlphanumeric(7);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(secondCaptionText);
        logger.logTestStep("Wait until caption text displayed.");
        previewMediaPage.waitUntilCaptionTextDisplayed(secondCaptionText);

        logger.logTestStep("Click on Send photo or Video button.");
        previewMediaPage.clickOnSendButton();

        logger.logTestStep("Check whether Caption text displayed.");
        boolean isCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(captionText);
        logger.logTestVerificationStep("Verify whether Caption text displayed." + isCaptionTextDisplayed);
        Assert.assertTrue(isCaptionTextDisplayed,"Caption text not Displayed.");

        logger.logTestStep("Check whether second photo Caption text displayed.");
        boolean isSecondPhotoCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(secondCaptionText);
        logger.logTestVerificationStep("Verify whether second photo Caption text displayed." + isSecondPhotoCaptionTextDisplayed);
        Assert.assertTrue(isSecondPhotoCaptionTextDisplayed, "Second photo caption text not Displayed.");
    }

    /**
     * AttachmentTest-@T3:Send button in camera.
     */
    @Test(description = "Send button in camera.")
    public void verifyCameraPhotoSendInAttachments() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Camera button.");
        contactInfoPage.clickOnCameraButton();
        logger.logTestStep("Click on take Photo button.");
        previewMediaPage.clickOnTakePhotoButton();
        logger.logTestStep("Wait until visible of captured snapshot.");
        cameraPage.waitUntilVisibleOfCapturedSnapshot();
        logger.logTestStep("Store Chat input text.");
        String captionText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(captionText);
        logger.logTestStep("Click on send captured snapshot button.");
        previewMediaPage.clickOnSendButton();

        logger.logTestStep("Check whether Caption text displayed.");
        boolean isCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(captionText);
        logger.logTestVerificationStep("Verify whether Caption text displayed." + isCaptionTextDisplayed);
        Assert.assertTrue(isCaptionTextDisplayed, "Caption text not Displayed.");
    }

    /**
     * AttachmentTest-@T4:Cancel take photo in Camera.
     */
    @Test(description = "Cancel take photo in Camera.")
    public void verifyCancelTakePhoto() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Camera button.");
        contactInfoPage.clickOnCameraButton();
        logger.logTestStep("Click on take Photo button.");
        previewMediaPage.clickOnTakePhotoButton();

        logger.logTestStep("check whether take photo label displayed.");
        boolean isTakePhotoLabelDisplayed = cameraPage.isTakePhotoLabelDisplayed();
        logger.logTestVerificationStep("Verify whether take photo label displayed." + isTakePhotoLabelDisplayed);
        Assert.assertTrue(isTakePhotoLabelDisplayed, "Take photo label not displayed.");

        logger.logTestStep("Click on Cancel take photo.");
        cameraPage.clickOnCancelTakePhoto();
        logger.logTestStep("Wait until invisible of Take photo label.");
        cameraPage.waitUntilInvisibleOfTakePhotoLabel();
        logger.logTestStep("check whether take photo label displayed.");
        boolean isTakePhotoLabelDisplayedOrNot = cameraPage.isTakePhotoLabelDisplayed();
        logger.logTestVerificationStep("Verify whether take photo label displayed." + isTakePhotoLabelDisplayedOrNot);
        Assert.assertFalse(isTakePhotoLabelDisplayedOrNot, "Take photo label displayed.");
    }

    /**
     * AttachmentTest-@T5:Retake photo in Camera.
     */
    @Test(description = "Retake photo in Camera")
    public void verifyRetakePhotoInCamera() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Camera button.");
        contactInfoPage.clickOnCameraButton();
        logger.logTestStep("Check whether video viewer displayed.");
        boolean isVideoViewerDisplayed = cameraPage.isVideoViewerDisplayed();
        logger.logTestVerificationStep("Verify whether video viewer displayed." + isVideoViewerDisplayed);
        Assert.assertTrue(isVideoViewerDisplayed);

        logger.logTestStep("Click on take Photo button.");
        previewMediaPage.clickOnTakePhotoButton();
        logger.logTestStep("Check whether captured snapshot displayed.");
        boolean isCapturedSnapshotDisplayed = cameraPage.isCapturedSnapshotDisplayed();
        logger.logTestVerificationStep("Verify whether captured snapshot displayed." + isCapturedSnapshotDisplayed);
        Assert.assertTrue(isCapturedSnapshotDisplayed, "Captured snapshot not displayed.");
        logger.logTestStep("Click on Retake photo button.");
        cameraPage.clickOnRetakePhotoButton();
        logger.logTestStep("Check whether video viewer displayed.");
        boolean isVideoViewerDisplayedOrNot = cameraPage.isVideoViewerDisplayed();
        logger.logTestVerificationStep("Verify whether video viewer displayed." + isVideoViewerDisplayed);
        Assert.assertTrue(isVideoViewerDisplayed, "Video viewer not displayed.");
        logger.logTestStep("Click on Cancel take photo.");
        cameraPage.clickOnCancelTakePhoto();
    }

    /**
     * AttachmentTest-@T6:Document send button in Attached documents.
     */
    @Test(description = "Document send button in Attached documents.")
    public void verifySendButtonInAttachedDocuments() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Document button.");
        contactInfoPage.clickOnDocumentButton();
        logger.logTestStep("Set document.");
        autoITScripts.setDocument();
        logger.logTestStep("Click on send document button.");
        documentPage.clickOnSendDocumentButton();
        logger.logTestStep("Check whether Text document displayed.");
        boolean isTextDocumentDisplayed = chatPage.isTextDocumentDisplayed();
        logger.logTestVerificationStep("Verify whether Text document displayed." + isTextDocumentDisplayed);
        Assert.assertTrue(isTextDocumentDisplayed, "Text document not displayed.");
    }

    /**
     * AttachmentTest-@T7:Cancel preview in Attached documents.
     */
    @Test(description = "Cancel preview in Attached documents.")
    public void verifyCancelPreviewDocument() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Document button.");
        contactInfoPage.clickOnDocumentButton();
        logger.logTestStep("Set document.");
        autoITScripts.setDocument();
        logger.logTestStep("Check whether Preview title displayed.");
        boolean isPreviewTitleDisplayed = documentPage.isPreviewTitleDisplayed();
        logger.logTestVerificationStep("Verify whether Preview title displayed." + isPreviewTitleDisplayed);
        Assert.assertTrue(isPreviewTitleDisplayed, "Preview title is not displayed.");
        logger.logTestStep("Click on cancel preview document.");
        documentPage.clickOnCancelPreviewDocument();
        logger.logTestStep("Wait until Invisible of cancel preview document.");
        documentPage.waitUntilInvisibleOfCancelPreviewDocument();
        logger.logTestStep("Check whether Preview title displayed.");
        boolean isPreviewTitleDisplayedOrNot = documentPage.isPreviewTitleDisplayed();
        logger.logTestVerificationStep("Verify whether Preview title displayed." + isPreviewTitleDisplayedOrNot);
        Assert.assertFalse(isPreviewTitleDisplayedOrNot, "Preview title displayed.");
    }

    /**
     * AttachmentTest-@T8:Send contact in Attached documents.
     */
    @Test(description = "Send contact in Attached documents.")
    public void verifySendContactInAttachments() {
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Contact button.");
        contactInfoPage.clickOnContactButton();
        logger.logTestStep("Store chat forward user.");
        String chatForwardUser = propertyUtil.getProperty("chatForwardUser");
        logger.logTestStep("Set Search contact" + chatForwardUser);
        forwardMessagePage.setSearchContact(chatForwardUser);
        logger.logTestStep("Select send contact.");
        contactPage.selectSendContact(chatForwardUser);

        logger.logTestStep("Click on send contact confirm button.");
        sendContactConfirmPage.clickOnSendContactConfirmButton();
        logger.logTestStep("Check whether Sended contact displayed.");
        boolean isSendedContactDisplayed = chatPage.isSendedContactDisplayed(chatForwardUser);
        logger.logTestVerificationStep("Verify whether Sended contact displayed." + isSendedContactDisplayed);
        Assert.assertTrue(isSendedContactDisplayed, "Sended contact not displayed.");

        logger.logTestStep("Store chat forward user.");
        String chatUser = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Set search Contact." + chatUser);
        forwardMessagePage.setSearchContact(chatUser);
        logger.logTestStep("Select send contact.");
        contactPage.sleep(2);
        contactPage.selectSendContact(chatUser);
    }

    /**
     * AttachmentTest-@T9:Send message to Sended contact in Attached documents.
     */
    @Test(description = "Send message to Sended contact in Attached documents.")
    public void verifyMessageToSendedContact() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Clear message link.");
        contactInfoPage.clickOnClearMessageLink();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Check whether Clear message alert body displayed or not.");
        boolean isClearMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert body displayed or not."+isClearMessageAlertBodyDisplayed);
        Assert.assertTrue(isClearMessageAlertBodyDisplayed,"Clear message alert body not displayed.");

        logger.logTestStep("Check whether Clear message alert clear button displayed or not.");
        boolean isClearMessageAlertClearButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertClearButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert clear button displayed or not.");
        Assert.assertTrue(isClearMessageAlertClearButtonDisplayed,"Clear message alert clear button not displayed.");

        logger.logTestStep("Check whether Clear message alert cancel button displayed or not.");
        boolean isClearMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert cancel button displayed or not."+isClearMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isClearMessageAlertCancelButtonDisplayed,"Clear message alert cancel button not displayed.");
        logger.logTestStep("Click on Clear messages confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnClearMessagesConfirm(ClearAndDeleteMessageAlert.CLEAR_CHAT);

        logger.logTestStep("Store chat cleared.");
        String chatClearedAlert = "Chat cleared";
        whatsappAlerts.waitUntilInvisibleOfAlert(chatClearedAlert);
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Contact button.");
        contactInfoPage.clickOnContactButton();
        logger.logTestStep("Store chat forward user.");
        String chatForwardUser = propertyUtil.getProperty("chatForwardUser");
        logger.logTestStep("Set Search contact" + chatForwardUser);
        forwardMessagePage.setSearchContact(chatForwardUser);
        logger.logTestStep("Select send contact.");
        contactPage.selectSendContact(chatForwardUser);
        logger.logTestStep("Click on send contact confirm button.");
        sendContactConfirmPage.clickOnSendContactConfirmButton();

        logger.logTestStep("Click on Message button.");
        chatPage.clickOnMessageButton();
        logger.logTestStep("Store Chat input text.");
        String inputText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input."+inputText);
        chatPage.setChatInput(inputText);
        logger.logTestStep("Click on Send message button.");
        chatPage.clickOnSendMessageButton();

        logger.logTestStep("Check whether Chat message text Displayed.");
        boolean isChatMessageTextDisplayed = chatPage.isChatMessageTextDisplayed(inputText);
        logger.logTestVerificationStep("Verify whether Chat message text Displayed." + isChatMessageTextDisplayed);
        Assert.assertTrue(isChatMessageTextDisplayed,"Chat message text not displayed.");

        logger.logTestStep("Store chat forward user.");
        String chatUser = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Set search Contact." + chatUser);
        forwardMessagePage.setSearchContact(chatUser);
        logger.logTestStep("Select send contact.");
        contactPage.selectSendContact(chatUser);
    }

    /**
     * AttachmentTest-@T10:Add Sended contact to a group in Attached documents.
     */
    @Test(description = "Add Sended contact to a group in Attached documents.")
    public void verifyAddSendedContactToAGroup() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Clear message link.");
        contactInfoPage.clickOnClearMessageLink();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Check whether Clear message alert body displayed or not.");
        boolean isClearMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert body displayed or not."+isClearMessageAlertBodyDisplayed);
        Assert.assertTrue(isClearMessageAlertBodyDisplayed,"Clear message alert body not displayed.");

        logger.logTestStep("Check whether Clear message alert clear button displayed or not.");
        boolean isClearMessageAlertClearButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertClearButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert clear button displayed or not.");
        Assert.assertTrue(isClearMessageAlertClearButtonDisplayed,"Clear message alert clear button not displayed.");

        logger.logTestStep("Check whether Clear message alert cancel button displayed or not.");
        boolean isClearMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert cancel button displayed or not."+isClearMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isClearMessageAlertCancelButtonDisplayed,"Clear message alert cancel button not displayed.");
        logger.logTestStep("Click on Clear messages confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnClearMessagesConfirm(ClearAndDeleteMessageAlert.CLEAR_CHAT);

        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Contact button.");
        contactInfoPage.clickOnContactButton();

        logger.logTestStep("Store chat forward user.");
        String chatForwardUser = propertyUtil.getProperty("chatForwardUser");
        logger.logTestStep("Set Search contact" + chatForwardUser);
        forwardMessagePage.setSearchContact(chatForwardUser);
        logger.logTestStep("Select send contact.");
        contactPage.selectSendContact(chatForwardUser);
        logger.logTestStep("Click on send contact confirm button.");
        sendContactConfirmPage.clickOnSendContactConfirmButton();
        logger.logTestStep("Click on Add to a group button.");
        chatPage.clickOnAddToAGroupButton();

        logger.logTestStep("Store chat forward user.");
        String group = propertyUtil.getProperty("group");
        logger.logTestStep("Set Search contact" + group);
        forwardMessagePage.setSearchContact(group);
        logger.logTestStep("Wait until visible of User.");
        contactPage.waitUntilVisibleUser();
        logger.logTestStep("Select send contact.");
        contactPage.selectSendContact(group);
        logger.logTestStep("Click on send contact confirm button.");
        sendContactConfirmPage.clickOnSendContactConfirmButton();

        logger.logTestStep("Check whether Group page displayed.");
        boolean isGroupPageDisplayed = chatPage.isGroupPageDisplayed(group);
        logger.logTestVerificationStep("Verify whether Group page displayed." + isGroupPageDisplayed);
        Assert.assertTrue(isGroupPageDisplayed,"Group page is not displayed.");
        logger.logTestStep("Check whether User added confirm message displayed.");
        boolean isUserAddedConfirmMessage = chatPage.isUserAddedConfirmMessage(chatForwardUser);
        logger.logTestVerificationStep("Verify whether User added confirm message displayed." + isUserAddedConfirmMessage);
        Assert.assertTrue(isUserAddedConfirmMessage,"User added confirm message not displayed.");

        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Group info link.");
        groupsPage.clickOnGroupInfoLink();
        logger.logTestStep("Click on Scroll down.");
        groupsPage.scrollDown();
        logger.logTestStep("Move to user.");
        groupsPage.moveToUser(chatForwardUser);
        logger.logTestStep("Click on Select user.");
        groupsPage.clickOnUserOptionsButton();
        logger.logTestStep("Click on remove button.");
        groupsPage.clickOnRemoveButton();

        logger.logTestStep("Check whether Remove user alert head displayed or not.");
        boolean isRemoveUserAlertHeadDisplayed = removeUserConfirmPage.isRemoveUserAlertHeadDisplayed(chatForwardUser,group);
        logger.logTestVerificationStep("Verify whether Remove user alert head displayed or not."+isRemoveUserAlertHeadDisplayed);
        Assert.assertTrue(isRemoveUserAlertHeadDisplayed);

        logger.logTestStep("Check whether Remover user alert cancel button displayed or not.");
        boolean isRemoveUserAlertCancelButton = removeUserConfirmPage.isRemoveUserAlertCancelButton();
        logger.logTestVerificationStep("Verify whether Remover user alert cancel button displayed or not."+isRemoveUserAlertCancelButton);
        Assert.assertTrue(isRemoveUserAlertCancelButton,"Remove user alert cancel button not displayed.");

        logger.logTestStep("Check whether Remove user alert remove button displayed or not.");
        boolean isRemoveUserAlertRemoveButton = removeUserConfirmPage.isRemoveUserAlertRemoveButton();
        logger.logTestVerificationStep("Verify whether Remove user alert remove button displayed or not."+isRemoveUserAlertRemoveButton);
        Assert.assertTrue(isRemoveUserAlertRemoveButton);

        logger.logTestStep("Click on Remove user confirm button.");
        removeUserConfirmPage.clickOnRemoveUserConfirmButton();
        logger.logTestStep("Click on Close info button.");
        groupsPage.clickOnCloseInfoButton();

        logger.logTestStep("Check whether User removed confirm message displayed.");
        boolean isUserRemovedConfirmMessage = chatPage.isUserRemovedConfirmMessage(chatForwardUser);
        logger.logTestVerificationStep("Verify whether User removed confirm message displayed." + isUserRemovedConfirmMessage);
        Assert.assertTrue(isUserRemovedConfirmMessage,"User removed confirm message not displayed.");


        logger.logTestStep("Click on new ChatPage button.");
        userControlPanelPage.clickOnNewChatButton();
        logger.logTestStep("Set search Contact." + user);
        newChatPage.setSearchContact(user);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(user);
    }

    /**
     * AttachmentTest-@T11:Reply a message.
     */
    @Test(description = "Reply a message.")
    public void verifyReplayMessage() {
        logger.logTestStep("Store Chat input text.");
        String chatInputText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input");
        chatPage.setChatInput(chatInputText);
        logger.logTestStep("Click on Send message button");
        chatPage.clickOnSendMessageButton();
        logger.logTestStep("Move to user.");
        chatPage.moveToReplyMessage(chatInputText);
        logger.logTestStep("Click on Select user.");
        chatPage.clickOnOptionsButton();

        chatPage.clickOnReplyLink();
        logger.logTestStep("Store Chat input text.");
        String chatReplyText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input");
        chatPage.setChatInput(chatReplyText);
        logger.logTestStep("Click on Send message button");
        chatPage.clickOnSendMessageButton();

        logger.logTestStep("Check whether Replay text displayed.");
        boolean isReplayTextDisplayed = chatPage.isReplayTextDisplayed(chatReplyText);
        logger.logTestVerificationStep("Verify whether Replay text displayed." + isReplayTextDisplayed);
        Assert.assertTrue(isReplayTextDisplayed,"Replay text not displayed.");
    }

    @AfterClass(alwaysRun = true)
    public void checkNewChatutton(){
        boolean isNewChatButtonDisplayed = userControlPanelPage.isNewChatButtonDisplayed();
        Assert.assertTrue(isNewChatButtonDisplayed,"New Chat button not displayed.");
    }
}
