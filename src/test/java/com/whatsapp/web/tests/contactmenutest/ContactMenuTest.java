package com.whatsapp.web.tests.contactmenutest;

import com.whatsapp.web.enums.WhatsappAlertsEnum;
import com.whatsapp.web.pages.chat.ChatPage;
import com.whatsapp.web.pages.confirmpages.*;
import com.whatsapp.web.enums.ClearAndDeleteMessageAlert;
import com.whatsapp.web.pages.contactinfopage.ContactInfoPage;
import com.whatsapp.web.pages.contactlist.ContactListPage;
import com.whatsapp.web.enums.MuteAlertEnum;
import com.whatsapp.web.pages.mycontactinfo.MyContactInfoPage;
import com.whatsapp.web.pages.newchat.NewChatPage;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.pages.whatsappalerts.WhatsappAlerts;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.tests.BaseTest;
import com.whatsapp.web.util.TestLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by SHIVA on 1/23/2017.
 */
public class ContactMenuTest extends BaseLoginTest {
    /**
     * Instance variable for Test logger.
     */
    private static final TestLogger logger = TestLogger.getLogger(ContactMenuTest.class);
    /**
     * Instance variable for Contact list.
     */
    ContactListPage contactListPage;
    /**
     * Instance variable for User control panel page.
     */
    UserControlPanelPage userControlPanelPage;
    /**
     * Instance variable for New chat.
     */
    NewChatPage newChatPage;
    /**
     * Instance variable for My contact info.
     */
    MyContactInfoPage myContactInfoPage;
    /**
     * Instance variable for Alerts.
     */
    WhatsappAlerts whatsappAlerts;
    /**
     * Instance variable for Contact info page.
     */
    ContactInfoPage contactInfoPage;
    /**
     * Instance variable for ChatPage.
     */
    ChatPage chatPage;
    /**
     * Instance variable for Block contact confirmation alert page.
     */
    BlockContactConfirmationAlertPage blockContactConfirmationAlertPage;
    /**
     * Instance variable for Mute confirm page.
     */
    MuteConfirmPage muteConfirmPage;
    /**
     * Instance variable for Delete chat confirm page.
     */
    DeleteChatConfirmPage deleteChatConfirmPage;
    /**
     * Instance variable for Unblock confirm page.
     */
    UnblockContactConfirmPage unblockContactConfirmPage;


    @BeforeClass(alwaysRun = true)
    public void setupBrowser() {
        contactListPage = new ContactListPage(webDriver);
        userControlPanelPage = new UserControlPanelPage(webDriver);
        newChatPage = new NewChatPage(webDriver);
        myContactInfoPage = new MyContactInfoPage(webDriver);
        whatsappAlerts = new WhatsappAlerts(webDriver);
        contactInfoPage = new ContactInfoPage(webDriver);
        chatPage = new ChatPage(webDriver);
        blockContactConfirmationAlertPage = new BlockContactConfirmationAlertPage(webDriver);
        muteConfirmPage = new MuteConfirmPage(webDriver);
        deleteChatConfirmPage = new DeleteChatConfirmPage(webDriver);
        unblockContactConfirmPage = new UnblockContactConfirmPage(webDriver);

        logger.logTestStep("Click on new ChatPage button.");
        userControlPanelPage.clickOnNewChatButton();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Set search Contact." + user);
        newChatPage.setSearchContact(user);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(user);
    }

    /**
     * ContactMenuTest-@T1:Mute button in contact info page.
     */
    @Test(description = "Mute button in contact info page.")
    public void verifyMuteButtonInContactInfo() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Contact Info.");
        contactInfoPage.clickOnContactInfo();
        logger.logTestStep("click on Mute Check box.");
        myContactInfoPage.clickOnMuteCheckBox();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Check whether Mute alert head displayed or not.");
        boolean isMuteAlertHeadDisplayed= muteConfirmPage.isMuteAlertHeadDisplayed(user);
        logger.logTestVerificationStep("Verify whether Mute alert head displayed or not.");
        Assert.assertTrue(isMuteAlertHeadDisplayed,"Mute alert head not displayed.");

        logger.logTestStep("Check whether Mute alert mute button displayed or not");
        boolean isMuteAlertMuteButtonDisplayed = muteConfirmPage.isMuteAlertMuteButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Mute alert mute button displayed or not"+isMuteAlertMuteButtonDisplayed);
        Assert.assertTrue(isMuteAlertMuteButtonDisplayed,"Mute alert mute button not displayed.");

        logger.logTestStep("Check whether Mute alert cancel button displayed or not.");
        boolean isMuteAlertCancelButtonDisplayed = muteConfirmPage.isMuteAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Mute alert cancel button displayed or not."+isMuteAlertCancelButtonDisplayed);
        Assert.assertTrue(isMuteAlertCancelButtonDisplayed,"Mute alert cancel button not displayed.");

        logger.logTestStep("Set Mute time.");
        muteConfirmPage.setMuteTime(MuteAlertEnum.ONE_WEEK);

        logger.logTestStep("Check whether Muted label displayed.");
        boolean isMutedLabelDisplayed = myContactInfoPage.isMutedLabelDisplayed();
        logger.logTestVerificationStep("Verify whether Muted label displayed." + isMutedLabelDisplayed);
        Assert.assertTrue(isMutedLabelDisplayed,"Muted label still not displayed.");
        logger.logTestStep("Click on Mute label.");
        muteConfirmPage.clickOnMuteButton();

        logger.logTestStep("Check whether Muted icon displayed.");
        boolean isMutedIconDisplayed = contactListPage.isMutedIconDisplayed();
        logger.logTestVerificationStep("Verify whether Muted icon displayed.");
        Assert.assertTrue(isMutedIconDisplayed,"Method Icon still not displayed.");
        logger.logTestStep("Move to Muted icon.");
        contactListPage.moveToMutedIcon();
        logger.logTestStep("Click on contact context button.");
        contactListPage.clickOnContactContextButton();
        logger.logTestStep("Click on Cancel Muted.");
        contactListPage.clickOnCancelMuted();

        logger.logTestStep("Check whether ChatPage unmuted alert displayed.");
        boolean isChatUnmutedDisplayed = whatsappAlerts.isChatUnmutedAlertDisplayed(WhatsappAlertsEnum.CHAT_UNMUTED);
        logger.logTestVerificationStep("Verify whether ChatPage unmuted alert displayed." + isChatUnmutedDisplayed);
        Assert.assertTrue(isChatUnmutedDisplayed,"ChatPage unmuted alert is not displayed");
    }

    /**
     * ContactMenuTest-@T2 - Block Contact in Contact info.
     */
    @Test(description = "Block Contact in Contact info.")
    public void verifyBlockContactInContactInfo() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Contact Info.");
        contactInfoPage.clickOnContactInfo();
        logger.logTestStep("Click on Block contact.");
        myContactInfoPage.clickOnBlockContact();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");

        logger.logTestStep("Check whether Block alert body displayed or not.");
        boolean isBlockAlertBodyDisplayed = blockContactConfirmationAlertPage.isBlockAlertBodyDisplayed(user);
        logger.logTestVerificationStep("Verify whether Block alert body displayed or not.");
        Assert.assertTrue(isBlockAlertBodyDisplayed,"Block alert body not displayed.");

        logger.logTestStep("Check whether block alert Block confirm button displayed or not.");
        boolean isBlockAlertBlockConfirmButtonDisplayed = blockContactConfirmationAlertPage.isBlockAlertBlockConfirmButtonDisplayed();
        logger.logTestVerificationStep("Verify whether block alert Block confirm button displayed or not."+isBlockAlertBlockConfirmButtonDisplayed);
        Assert.assertTrue(isBlockAlertBlockConfirmButtonDisplayed,"Block alert block confirm button not displayed.");

        logger.logTestStep("Check whether block alert Cancel button displayed or not.");
        boolean isBlockAlertCancelButtonDisplayed = blockContactConfirmationAlertPage.isBlockAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether block alert Cancel button displayed or not."+isBlockAlertCancelButtonDisplayed);
        Assert.assertTrue(isBlockAlertCancelButtonDisplayed,"Block alert cancel button not displayed.");

        logger.logTestStep("Click on Block confirm.");
        blockContactConfirmationAlertPage.clickOnBlockConfirm();
        logger.logTestStep("Check whether Block chat message displayed.");
        boolean isBlockChatDisplayed = chatPage.isBlockChatMessageDisplayed();
        logger.logTestVerificationStep("Verify whether Block chat message displayed." + isBlockChatDisplayed);
        Assert.assertTrue(isBlockChatDisplayed,"Block chat message not displayed.");
        logger.logTestStep("Check whether Block contact alert displayed.");
        boolean isBlockContactAlertDisplayed = whatsappAlerts.isBlockContactAlertDisplayed(user);
        logger.logTestVerificationStep("Verify whether Block contact alert displayed." + isBlockContactAlertDisplayed);
        Assert.assertTrue(isBlockContactAlertDisplayed,"Block contact alert not displayed.");
        logger.logTestStep("Wait until invisible of block or unblock contact undo button alert.");
        whatsappAlerts.waitUntilInvisibleOfBlockOrUnblockContactUndoButtonAlert();
        logger.logTestStep("Click on User control panel menu button.");
        userControlPanelPage.clickOnUserControlPanelMenuButton();
        logger.logTestStep("Click on Settings.");
        userControlPanelPage.clickOnSettingsLink();
        logger.logTestStep("Click on Blocked contacts.");
        userControlPanelPage.clickOnBlockedContacts();

        logger.logTestStep("Check whether blocked contact displayed.");
        boolean isBlockedContactDisplayed = userControlPanelPage.isBlockedContactDisplayed(user);
        logger.logTestVerificationStep("Verify whether blocked contact displayed." + isBlockedContactDisplayed);
        Assert.assertTrue(isBlockedContactDisplayed,"Blocked contact not Displayed.");

        logger.logTestStep("Check whether unblock contact button displayed.");
        boolean isUnblockContactDisplayed = myContactInfoPage.isUnblockContactButtonDisplayed();
        logger.logTestVerificationStep("Verify whether unblock contact button displayed." + isUnblockContactDisplayed);
        Assert.assertTrue(isUnblockContactDisplayed,"unblock contact button not displayed.");
        logger.logTestStep("Click on Unblock contact.");
        myContactInfoPage.clickOnUnblockContactButton();

        logger.logTestStep("Check whether Unblock alert head displayed or not.");
        boolean isUnblockAlertHeadDisplayed= unblockContactConfirmPage.isUnblockAlertHeadDisplayed(user);
        logger.logTestVerificationStep("Verify whether Unblock alert head displayed or not."+isUnblockAlertHeadDisplayed);
        Assert.assertTrue(isUnblockAlertHeadDisplayed,"Unblock alert head not displayed.");

        logger.logTestStep("Check whether Unblock alert unblock button displayed or not");
        boolean isUnblockAlertUnblockButtonDisplayed = unblockContactConfirmPage.isUnblockAlertUnblockButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Unblock alert unblock button displayed or not"+isUnblockAlertUnblockButtonDisplayed);
        Assert.assertTrue(isUnblockAlertUnblockButtonDisplayed,"Unblock alert unblock button not displayed.");

        logger.logTestStep("Check whether Unblock alert cancel button displayed or not.");
        boolean isUnblockAlertCancelButtonDisplayed = unblockContactConfirmPage.isUnblockAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Unblock alert cancel button displayed or not.");
        Assert.assertTrue(isUnblockAlertCancelButtonDisplayed);

        logger.logTestStep("Click on Unblock contact confirm.");
        unblockContactConfirmPage.clickOnUnblockContactConfirm();

        logger.logTestStep("Check whether unblocked contact alert displayed.");
        boolean isUnblockedContactAlertDisplayed = whatsappAlerts.isUnblockedContactAlertDisplayed(user);
        logger.logTestVerificationStep("Verify whether unblocked contact alert displayed." + isUnblockedContactAlertDisplayed);
        Assert.assertTrue(isUnblockedContactAlertDisplayed,"Unblocked contact alert not displayed.");
        logger.logTestStep("Click on Move to back page on new chat.");
        newChatPage.clickOnMoveToBackPageOnNewChat();
        logger.logTestStep("Click on Move to back page on new chat.");
        newChatPage.clickOnMoveToBackPageOnNewChat();
        logger.logTestStep("Click on close contact info page.");
        contactInfoPage.clickOnCloseContactInfo();
    }

    /**
     * ContactMenuTest-@T3 - Undo Block Contact in Contact info.
     */
    @Test(description = "Undo Block Contact in Contact info.")
    public void verifyUndoBlockContact() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Contact Info.");
        contactInfoPage.clickOnContactInfo();
        logger.logTestStep("Click on Block contact.");
        myContactInfoPage.clickOnBlockContact();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");

        logger.logTestStep("Check whether Block alert body displayed or not.");
        boolean isBlockAlertBodyDisplayed = blockContactConfirmationAlertPage.isBlockAlertBodyDisplayed(user);
        logger.logTestVerificationStep("Verify whether Block alert body displayed or not.");
        Assert.assertTrue(isBlockAlertBodyDisplayed,"Block alert body not displayed.");

        logger.logTestStep("Check whether block alert Block confirm button displayed or not.");
        boolean isBlockAlertBlockConfirmButtonDisplayed = blockContactConfirmationAlertPage.isBlockAlertBlockConfirmButtonDisplayed();
        logger.logTestVerificationStep("Verify whether block alert Block confirm button displayed or not."+isBlockAlertBlockConfirmButtonDisplayed);
        Assert.assertTrue(isBlockAlertBlockConfirmButtonDisplayed,"Block alert block confirm button not displayed.");

        logger.logTestStep("Check whether block alert Cancel button displayed or not.");
        boolean isBlockAlertCancelButtonDisplayed = blockContactConfirmationAlertPage.isBlockAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether block alert Cancel button displayed or not."+isBlockAlertCancelButtonDisplayed);
        Assert.assertTrue(isBlockAlertCancelButtonDisplayed,"Block alert cancel button not displayed.");

        logger.logTestStep("Click on Block confirm.");
        blockContactConfirmationAlertPage.clickOnBlockConfirm();
        logger.logTestStep("Check whether Block chat message displayed.");
        boolean isBlockChatDisplayed = chatPage.isBlockChatMessageDisplayed();
        logger.logTestVerificationStep("Verify whether Block chat message displayed." + isBlockChatDisplayed);
        Assert.assertTrue(isBlockChatDisplayed,"Block chat message not displayed.");
        logger.logTestStep("Check whether Block contact alert displayed.");
        boolean isBlockContactAlertDisplayed = whatsappAlerts.isBlockContactAlertDisplayed(user);
        logger.logTestVerificationStep("Verify whether Block contact alert displayed." + isBlockContactAlertDisplayed);
        Assert.assertTrue(isBlockContactAlertDisplayed,"Block contact alert not displayed.");

        logger.logTestStep("Click on Block contact Undo button.");
        whatsappAlerts.clickOnBlockOrUnblockContactUndoButton();
        logger.logTestStep("Check whether unblock contact button displayed.");
        boolean isUnblockContactDisplayed = myContactInfoPage.isUnblockContactButtonDisplayed();
        logger.logTestVerificationStep("Verify whether unblock contact button displayed." + isUnblockContactDisplayed);
        Assert.assertTrue(isUnblockContactDisplayed,"unblock contact button not displayed.");

        logger.logTestStep("Click on Unblock contact Undo button.");
        whatsappAlerts.clickOnUnBlockContactUndoButton();
        logger.logTestStep("Click on Unblock contact.");
        myContactInfoPage.clickOnUnblockContactButton();
        logger.logTestStep("Click on Unblock contact confirm.");
        unblockContactConfirmPage.clickOnUnblockContactConfirm();
        logger.logTestStep("Click on close contact info page.");
        contactInfoPage.clickOnCloseContactInfo();

        logger.logTestStep("Store chat cleared.");
        String chatUnmutedAlert = "Chat unmuted";
        logger.logTestStep("Wait until Invisible of Alert.");
        whatsappAlerts.waitUntilInvisibleOfAlert(chatUnmutedAlert);
        logger.logTestStep("Store Chat input text.");
    }

    /**
     * ContactMenuTest-@T4 - Delete chat in Contact info.
     */
    @Test(description = "Delete chat in Contact info.")
    public void verifyDeleteChat() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Contact Info.");
        contactInfoPage.clickOnContactInfo();
        logger.logTestStep("Click on Block contact.");
        logger.logTestStep("Click on Delete chat button.");
        myContactInfoPage.clickOnDeleteChatButton();
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Click on new ChatPage button.");
        logger.logTestStep("Check whether delete chat alert Body displayed or not.");
        boolean isDeleteChatAlertBodyDisplayed = deleteChatConfirmPage.isDeleteChatAlertBodyDisplayed(user);
        logger.logTestVerificationStep("Verify whether delete chat alert Body displayed or not."+isDeleteChatAlertBodyDisplayed);
        Assert.assertTrue(isDeleteChatAlertBodyDisplayed,"Delete chat alert Body not displayed.");

        logger.logTestStep("Check whether Delete chat alert delete button displayed or not.");
        boolean isDeleteChatAlertDeleteButtonDisplayed = deleteChatConfirmPage.isDeleteChatAlertDeleteButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete chat alert delete button displayed or not."+isDeleteChatAlertDeleteButtonDisplayed);
        Assert.assertTrue(isDeleteChatAlertDeleteButtonDisplayed,"Delete chat alert delete button not displayed.");

        logger.logTestStep("Check whether Delete chat alert cancel button displayed or not.");
        boolean isDeleteChatAlertCancelButtonDisplayed = deleteChatConfirmPage.isDeleteChatAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete chat alert cancel button displayed or not."+isDeleteChatAlertCancelButtonDisplayed);
        Assert.assertTrue(isDeleteChatAlertCancelButtonDisplayed,"Delete chat alert cancel button not displayed.");

        logger.logTestStep("Click on Delete chat.");
        deleteChatConfirmPage.clickOnDeleteChatConfirm(ClearAndDeleteMessageAlert.DELETE_CHAT);

        logger.logTestStep("Check whether chat Deleted alert displayed.");
        boolean isChatDeletedAlertDisplayed = whatsappAlerts.isChatDeletedAlertDisplayed(WhatsappAlertsEnum.CHAT_DELETED_ALERT);
        logger.logTestVerificationStep("Verify whether chat Deleted alert displayed." + isChatDeletedAlertDisplayed);
        Assert.assertTrue(isChatDeletedAlertDisplayed,"Chat deleted alert  not displayed.");
        logger.logTestStep("Store User.");
        userControlPanelPage.clickOnNewChatButton();
        logger.logTestStep("Set search Contact." + user);
        newChatPage.setSearchContact(user);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(user);

        logger.logTestStep("Store Chat input text.");
        String chatInputText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input");
        chatPage.setChatInput(chatInputText);
        logger.logTestStep("Click on Send message button");
        chatPage.clickOnSendMessageButton();
    }
}
