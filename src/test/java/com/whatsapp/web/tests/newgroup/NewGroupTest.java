package com.whatsapp.web.tests.newgroup;

import com.whatsapp.web.enums.GroupMenuOptionsEnum;
import com.whatsapp.web.enums.UserProfileEnum;
import com.whatsapp.web.pages.addgroupparticipants.AddGroupParticipantsPage;
import com.whatsapp.web.pages.autoit.AutoItScriptsPage;
import com.whatsapp.web.pages.chat.ChatPage;
import com.whatsapp.web.pages.confirmationspage.ClearGroupChatPage;
import com.whatsapp.web.pages.confirmationspage.DeleteGroupPage;
import com.whatsapp.web.pages.confirmationspage.ExitGroupPage;
import com.whatsapp.web.pages.confirmationspage.RemoveConfirmationPage;
import com.whatsapp.web.pages.groupinfopage.GroupInfoPage;
import com.whatsapp.web.pages.groupinfopage.MyGroupInfoAddParticipantPage;
import com.whatsapp.web.pages.groupinfopage.MyGroupInfoPage;
import com.whatsapp.web.pages.groupinfopage.SearchMessagesPage;
import com.whatsapp.web.pages.newgroup.NewGroupPage;
import com.whatsapp.web.pages.profileandstatus.ProfileAndStatusPage;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.pages.whatsappalerts.WhatsappAlertsPage;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.util.PropertyUtil;
import com.whatsapp.web.util.TestLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains all New group test features.
 * Created by Venu on 1/30/2017.
 */
public class NewGroupTest extends BaseLoginTest {
    /**
     * Instance variable for New group page.
     */
    NewGroupPage newGroupPage;
    /**
     * Instance variable for User control panel page.
     */
    UserControlPanelPage userControlPanelPage;
    /**
     * Instance variable for Add group participants.
     */
    AddGroupParticipantsPage addGroupParticipantsPage;
    /**
     * Instance variable for Group info page.
     */
    GroupInfoPage groupInfoPage;
    /**
     * Instance variable for Whatsapp alerts page.
     */
    WhatsappAlertsPage whatsappAlertsPage;
    /**
     * Instance variable for My group info page.
     */
    MyGroupInfoPage myGroupInfoPage;
    /**
     * Instance variable for My group info add participant page.
     */
    MyGroupInfoAddParticipantPage myGroupInfoAddParticipantPage;
    /**
     * Instance variable for Property util.
     */
    PropertyUtil propertyUtil;
    /**
     * Instance variable for Test logger.
     */
   private static final TestLogger logger = TestLogger.getLogger(NewGroupTest.class);
    /**
     * Instance variable for Remove confirmation page.
     */
    RemoveConfirmationPage removeConfirmationPage;
    /**
     * Instance variable for Profile and status page.
     */
    ProfileAndStatusPage profileAndStatusPage;
    /**
     * Instance variable for Chat page.
     */
    ChatPage chatPage;
    /**
     * Instance variable for Clear group chat page.
     */
    ClearGroupChatPage clearGroupChatPage;
    /**
     * Instance variable for Search message page.
     */
    SearchMessagesPage searchMessagesPage;
    /**
     * Instance variable for Exit group page.
     */
    ExitGroupPage exitGroupPage;
    /**
     * Instance variable for Delete group page.
     */
    DeleteGroupPage deleteGroupPage;
    /**
     * Initialize count value.
     */
    int count = 1;
    /**
     * Instance variable for groupName.
     */
    String groupName;

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        logger.logTestStep("Scan code and Navigate to home page");
        newGroupPage = new NewGroupPage(webDriver);
        userControlPanelPage = new UserControlPanelPage(webDriver);
        addGroupParticipantsPage = new AddGroupParticipantsPage(webDriver);
        whatsappAlertsPage = new WhatsappAlertsPage(webDriver);
        propertyUtil = new PropertyUtil();
        myGroupInfoPage = new MyGroupInfoPage(webDriver);
        myGroupInfoAddParticipantPage = new MyGroupInfoAddParticipantPage(webDriver);
        groupInfoPage = new GroupInfoPage(webDriver);
        removeConfirmationPage = new RemoveConfirmationPage(webDriver);
        profileAndStatusPage = new ProfileAndStatusPage(webDriver);
        chatPage = new ChatPage(webDriver);
        clearGroupChatPage = new ClearGroupChatPage(webDriver);
        searchMessagesPage = new SearchMessagesPage(webDriver);
        exitGroupPage = new ExitGroupPage(webDriver);
        deleteGroupPage = new DeleteGroupPage(webDriver);

        logger.logTestStep("Create new group and navigate to Group info page.");
        createNewGroup();
    }

    /**
     * Method to Create new group.
     */
    private void createNewGroup(){
        logger.logTestStep("Click on user control panel menu button");
        userControlPanelPage.clickOnUserControlPanelMenuButton();
        logger.logTestStep("Click on new group link");
        userControlPanelPage.clickOnNewGroupLink();
        logger.logTestStep("Create group name using random string utils");
        groupName = "Group - " + RandomStringUtils.randomAlphanumeric(8);
        logger.logTestStep("Click on group subject");
        newGroupPage.clickOnGroupSubject(groupName);
        logger.logTestStep("Click on arrow button");
        newGroupPage.clickOnArrowButton();
        logger.logTestStep("Set contact name");
        addGroupParticipantsPage.setContactName(propertyUtil.getProperty("contactname2"));
        logger.logTestStep("Count increase");
        count++;
        logger.logTestStep("Click on searched contact name");
        addGroupParticipantsPage.clickOnSearchedContactName(propertyUtil.getProperty("contactname2"));
        logger.logTestStep("Click on check mark button");
        addGroupParticipantsPage.clickOnCheckMarkButton();
        logger.logTestStep("Check whether group created alert displayed or not");
        boolean isGroupCreatedAlertDisplayed = whatsappAlertsPage.isGroupCreatedAlertDisplayed();
        logger.logTestVerificationStep("Verify Group created alert displayed or not :"+isGroupCreatedAlertDisplayed);
        Assert.assertTrue(isGroupCreatedAlertDisplayed, "Group created alert is not displayed");
        logger.logTestStep("Wait until invisible of group created alert");
        whatsappAlertsPage.waitUntilInvisibleOfGroupCreatedAlert();
        logger.logTestStep("Check whether created group name displayed or not");
        boolean isCreatedGroupNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify created group name displayed or not :"+isCreatedGroupNameDisplayed);
        Assert.assertTrue(isCreatedGroupNameDisplayed, "Created Group is not Displayed.");
    }

    /**
     * Method to After class Exit and Delete the created group.
     */
    @AfterClass
    public void tearDown() {
        logger.logTestStep("Click on Group info menu button");
        groupInfoPage.clickOnGroupInfoMenuButton();
        logger.logTestStep("Click on Exit group link");
        groupInfoPage.groupInfoMenuOptions(GroupMenuOptionsEnum.EXIT_GROUP);
        logger.logTestStep("Check whether Exited group name displayed or not");
        boolean isExitedGroupNameDisplayed = exitGroupPage.isExitedGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify Exited group name displayed or not :"+isExitedGroupNameDisplayed);
        Assert.assertTrue(isExitedGroupNameDisplayed, "Exited group name is not displayed");

        logger.logTestStep("Click on Exit button");
        exitGroupPage.clickOnExitButton();
        logger.logTestStep("Check whether Exited group alert displayed or not");
        boolean isExitedGroupAlertDisplayed = whatsappAlertsPage.isExitedGroupAlertDisplayed();
        logger.logTestVerificationStep("Verify Exited group alert displayed or not :"+isExitedGroupAlertDisplayed);
        Assert.assertTrue(isExitedGroupAlertDisplayed, "Exited group alert is not displayed");

        logger.logTestStep("Wait until invisible of Exited group alert");
        whatsappAlertsPage.waitUntilInvisibleOfExitedGroupAlert();
        logger.logTestStep("Check whether You left text displayed or not");
        boolean isYouLeftTextDisplayed = chatPage.isYouLeftTextDisplayed();
        logger.logTestVerificationStep("Verify You left text is displayed or not :"+isYouLeftTextDisplayed);
        Assert.assertTrue(isYouLeftTextDisplayed, "You left text is not displayed");

        logger.logTestStep("Click on Group info menu button");
        groupInfoPage.clickOnGroupInfoMenuButton();
        logger.logTestStep("Click on Delete button");
        groupInfoPage.groupInfoMenuOptions(GroupMenuOptionsEnum.DELETE_GROUP);
        logger.logTestStep("Check whether Deleted group name Displayed or not");
        boolean isDeletedGroupNameDisplayed = deleteGroupPage.isDeletedGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify Deleted group name displayed or not :"+isDeletedGroupNameDisplayed);
        Assert.assertTrue(isDeletedGroupNameDisplayed, "Deleted group name is not displayed");

        logger.logTestStep("Click on Deleted button");
        deleteGroupPage.clickOnDeleteButton();
        logger.logTestStep("Check whether Group deleted alert displayed or not");
        boolean isGroupDeletedAlertDisplayed = whatsappAlertsPage.isGroupDeletedAlertDisplayed();
        logger.logTestVerificationStep("Verify Group deleted alert displayed or not");
        Assert.assertTrue(isGroupDeletedAlertDisplayed, "Group deleted alert is not displayed");

        logger.logTestStep("Wait until invisible of group deleted alert");
        whatsappAlertsPage.waitUntilInvisibleOfGroupDeletedAlert();
        logger.logTestStep("Logout from whatsApp");
    }

    /**
     * Test case to verify Add participant to group.
     */
    @Test(description = "Test case to verify Add participant to group")
    public void verifyAddParticipantToGroup() {
        logger.logTestStep("Click on group info menu button");
        groupInfoPage.clickOnGroupInfoMenuButton();
        logger.logTestStep("Click on group info link");
        groupInfoPage.groupInfoMenuOptions(GroupMenuOptionsEnum.GROUP_INFO);
        logger.logTestStep("Scroll down");
        myGroupInfoPage.scrollDown();
        logger.logTestStep("Get participants count");
        String actualText = myGroupInfoPage.getParticipantsCount();
        logger.logTestVerificationStep("Verify participant count :"+actualText);
        Assert.assertTrue(actualText.contains(Integer.toString(count)));

        logger.logTestStep("Click on add participant button");
        myGroupInfoPage.clickOnAddParticipantButton();
        logger.logTestStep("Get contact name from property class ");
        String contactName = propertyUtil.getProperty("santhu");
        logger.logTestStep("Search contact name");
        myGroupInfoAddParticipantPage.searchContactName(contactName);
        logger.logTestStep("Click on searched contact");
        myGroupInfoAddParticipantPage.clickOnSearchedContact(contactName);
        logger.logTestStep("Click on pop_up add participant button");
        myGroupInfoAddParticipantPage.clickOnPopupAddParticipantButton();
        logger.logTestStep("Check whether Contact added alert displayed or not");
        boolean isContactAddedAlertDisplayed = whatsappAlertsPage.isContactAddedAlertDisplayed(contactName);
        logger.logTestVerificationStep("Verify contact added alert displayed or not :"+isContactAddedAlertDisplayed);
        Assert.assertTrue(isContactAddedAlertDisplayed, "Contact added alert is not displayed");
        logger.logTestStep("Count increase");
        count--;

        logger.logTestStep("Wait until invisible of Contact added alert displayed");
        whatsappAlertsPage.waitUntilInvisibleOfContactAddedAlertDisplayed(contactName);
        logger.logTestStep("Get participants count");
        String expectedText = myGroupInfoPage.getParticipantsCount();
        logger.logTestVerificationStep("verify participants count :"+expectedText);
        Assert.assertNotEquals(actualText, expectedText);

        logger.logTestStep("Move to selected contact :"+contactName);
        myGroupInfoPage.moveToSelectedContact(contactName);
        logger.logTestStep("Click on contact options button");
        myGroupInfoPage.clickOnContactOptionsButton();
        logger.logTestStep("Click on remove link");
        myGroupInfoPage.clickOnRemoveLink();
        logger.logTestStep("Click on remove button");
        removeConfirmationPage.clickOnRemoveButton();
        logger.logTestStep("Check whether contact removed alert displayed");
        boolean isContactRemovedAlertDisplayed = whatsappAlertsPage.isContactRemovedAlertDisplayed(contactName);
        logger.logTestVerificationStep("Verify contact removed alert displayed or not "+isContactRemovedAlertDisplayed);
        Assert.assertTrue(isContactRemovedAlertDisplayed, "Contact removed alert is not displayed");
        logger.logTestStep("Wait until invisible of contact removed alert");
        whatsappAlertsPage.waitUntilInvisibleOfContactRemovedAlert(contactName);
    }

    /**
     * Test case to verify Upload group profile photo.
     */
    @Test(description = "Test case to verify upload group profile photo")
    public void verifyUploadGroupProfilePhoto() {
        logger.logTestStep("Click on group info menu button");
        groupInfoPage.clickOnGroupInfoMenuButton();
        logger.logTestStep("Click on group info button");
        groupInfoPage.groupInfoMenuOptions(GroupMenuOptionsEnum.GROUP_INFO);
        logger.logTestStep("Click on add group icon");
        myGroupInfoPage.clickOnAddGroupIcon();
        logger.logTestStep("Click on upload photo link");
        profileAndStatusPage.updateUserProfile(UserProfileEnum.UPLOAD_PHOTO);
        logger.logTestStep("Select group profile picture");
        AutoItScriptsPage.selectGroupProfilePicture();
        logger.logTestStep("Click on done button");
        myGroupInfoPage.clickOnDoneButton();
        logger.logTestStep("Wait until invisible of setting group icon alert");
        whatsappAlertsPage.waitUntilInvisibleOfSettingGroupIconAlert();

        logger.logTestStep("Check whether group icon set alert displayed or not");
        boolean isGroupIconSetAlertDisplayed = whatsappAlertsPage.isGroupIconSetAlertDisplayed();
        logger.logTestVerificationStep("Verify group icon set alert displayed or not");
        Assert.assertTrue(isGroupIconSetAlertDisplayed, "Group icon set alert is not displayed");
        logger.logTestStep("Wait until invisible of group icon set alert displayed");
        whatsappAlertsPage.waitUntilInvisibleOfGroupIconSetAlertDisplayed();
    }

    /**
     * Test case to verify Unfinished messages.
     */
    @Test(description = "Test case to verify unfinished messages")
    public void verifyUnfinishedMessages() {
        logger.logTestStep("Store actual text");
        String actualText = RandomStringUtils.randomAlphanumeric(8);
        logger.logTestStep("Click on search button");
        userControlPanelPage.clickOnSearchButton();
        logger.logTestStep("Set group name");
        userControlPanelPage.setGroupName(propertyUtil.getProperty("contactname3"));
        logger.logTestStep("Click on searched group name");
        userControlPanelPage.clickOnSearchedGroupName();
        logger.logTestStep("Check whether Searched contact name displayed or not");
        boolean isSearchedContactNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(propertyUtil.getProperty("contactname3"));
        logger.logTestVerificationStep("Verify Searched contact name displayed or not");
        Assert.assertTrue(isSearchedContactNameDisplayed, "Searched contact name is not displayed");

        logger.logTestStep("Wait until invisible of created group name displayed or not");
        newGroupPage.waitUntilInvisibleOfCreatedGroupNameDisplayed("contactname3");
        logger.logTestStep("Set chat input");
        chatPage.setChatInput(actualText);
        logger.logTestStep("Click on Search button");
        userControlPanelPage.clickOnSearchButton();
        logger.logTestStep("Set group name");
        userControlPanelPage.setGroupName(propertyUtil.getProperty("groupname"));
        logger.logTestStep("Click on searched group name");
        userControlPanelPage.clickOnSearchedGroupName();
        logger.logTestStep("Check whether searched group name displayed or not");
        boolean isSearchedGroupNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(propertyUtil.getProperty("groupname"));
        logger.logTestVerificationStep("Verify searched group name displayed");
        Assert.assertTrue(isSearchedGroupNameDisplayed, "Searched group name is not displayed");

        logger.logTestStep("Set chat input");
        chatPage.setChatInput(actualText);
        logger.logTestStep("Get unfinished message text");
        String text = chatPage.getUnfinishedMessageText();
        logger.logTestStep("Store expt");
        String expectedText = text.substring(0, text.length() - 1);
        logger.logTestVerificationStep("Verify Actual and Expected text equal or not");
        Assert.assertEquals(actualText, expectedText, "Actual text not matched with Expected text");

        logger.logTestStep("Click on search button");
        userControlPanelPage.clickOnSearchButton();
        logger.logTestStep("Set group name");
        userControlPanelPage.setGroupName(groupName);
        logger.logTestStep("Click on searched group name :"+groupName);
        userControlPanelPage.clickOnSearchedGroupName();
        logger.logTestStep("Check whether Searched created group name displayed or not");
        boolean isSearchedCreatedGroupNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify searched created group name displayed or not :"+isSearchedCreatedGroupNameDisplayed);
        Assert.assertTrue(isSearchedCreatedGroupNameDisplayed, "Searched group name is not displayed");

    }

    /**
     * Test case to verify Change group name.
     */
    @Test(description = "Test case to verify Change group name")
    public void verifyChangeGroupName() {
        logger.logTestStep("Click on group info menu button");
        groupInfoPage.clickOnGroupInfoMenuButton();
        logger.logTestStep("Click on group info link");
        groupInfoPage.groupInfoMenuOptions(GroupMenuOptionsEnum.GROUP_INFO);
        logger.logTestStep("click on pencil button");
        myGroupInfoPage.clickOnPencilButton();
        logger.logTestStep("Set rename group name from property page");
        myGroupInfoPage.setGroupName(propertyUtil.getProperty("rename"));
        logger.logTestStep("Click on save button");
        myGroupInfoPage.clickOnSaveButton();
        logger.logTestStep("Wait until invisible of renaming group alert");
        whatsappAlertsPage.waitUntilInvisibleOfRenamingGroupAlert();
        logger.logTestStep("Click on Undo button alert");
        whatsappAlertsPage.clickOnUndoButtonAlert();
        logger.logTestStep("Check whether Group renamed alert displayed or not");
        whatsappAlertsPage.isGroupRenamedAlertDisplayed(groupName);
        logger.logTestStep("Wait until invisible of renaming group alert");
        whatsappAlertsPage.waitUntilInvisibleOfRenamingGroupAlert();

        logger.logTestStep("Check whether Created group name displayed or not");
        boolean isCreatedGroupNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify searched group name displayed or not :" + isCreatedGroupNameDisplayed);
        Assert.assertTrue(isCreatedGroupNameDisplayed, "Initial Group name is not displayed");

        logger.logTestStep("Click on group info menu button");
        groupInfoPage.clickOnGroupInfoMenuButton();
        logger.logTestStep("Click on clear messages link");
        groupInfoPage.groupInfoMenuOptions(GroupMenuOptionsEnum.CLEAR_MESSAGES);
        logger.logTestStep("Check whether Clear chat group name displayed or not");
        boolean isClearChatGroupNameDisplayed = clearGroupChatPage.isClearChatGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify clear chat group name displayed or not :" + isClearChatGroupNameDisplayed);
        Assert.assertTrue(isClearChatGroupNameDisplayed, "Clear chat group name is not displayed");

        /*logger.logTestStep("Check whether Cancel button displayed or not ");
        boolean isCancelButtonDisplayed = clearGroupChatPage.isCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify cancel button displayed or not :" + isCancelButtonDisplayed);
        Assert.assertTrue(isCancelButtonDisplayed, "Cancel button not displayed");*/

        logger.logTestStep("Click on clear button");
        clearGroupChatPage.clickOnClearButton();
        logger.logTestStep("Wait until invisible of Clearing chat alert");
        whatsappAlertsPage.waitUntilInvisibleOfClearingChatAlert();
        logger.logTestStep("Check whether chat cleared alert displayed or not");
        boolean isChatClearedAlertDisplayed = whatsappAlertsPage.isChatClearedAlertDisplayed();
        logger.logTestVerificationStep("Verify chat cleared alert displayed :" + isChatClearedAlertDisplayed);
        Assert.assertTrue(isChatClearedAlertDisplayed, "Chat cleared alert is not displayed");

        logger.logTestStep("Wait until invisible of Chat cleared alert");
        whatsappAlertsPage.waitUntilInvisibleOfChatClearedAlert();
    }

    /**
     * Test case to verify Search message from Chat page
     */
    @Test(description = "Test case to verify Search message from chat page")
    public void verifySearchMessageFromChatPage() {
        logger.logTestStep("Click on Search button");
        userControlPanelPage.clickOnSearchButton();
        logger.logTestStep("Set group name from property file");
        userControlPanelPage.setGroupName(propertyUtil.getProperty("contactname2"));
        logger.logTestStep("Click on searched contact name");
        userControlPanelPage.clickOnSearchedGroupName();
        logger.logTestStep("Check whether searched contact name displayed or not");
        boolean isSearchedContactNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(propertyUtil.getProperty("contactname2"));
        logger.logTestVerificationStep("Verify searched contact name displayed or not");
        Assert.assertTrue(isSearchedContactNameDisplayed, "Searched group name is not displayed");

        logger.logTestStep("Store actual text");
        String actualText = RandomStringUtils.randomAlphanumeric(12);
        logger.logTestStep("Set chat input");
        chatPage.setChatInput(actualText);
        logger.logTestStep("Click on send message button");
        chatPage.clickOnSendMessageButton();
        logger.logTestStep("Click on search button");
        groupInfoPage.clickOnSearchButton();
        logger.logTestStep("Set search message");
        searchMessagesPage.setSearchMessage(actualText);
        logger.logTestStep("Check whether searched message displayed or not");
        boolean isSearchedMessageDisplayed = searchMessagesPage.isSearchedMessageDisplayed();
        logger.logTestVerificationStep("Verify Searched message displayed or not :"+isSearchedMessageDisplayed);
        Assert.assertTrue(isSearchedMessageDisplayed, "Searched message not found");

        logger.logTestStep("Click on close button");
        searchMessagesPage.clickOnCloseButton();
        logger.logTestStep("Click on search button");
        userControlPanelPage.clickOnSearchButton();
        logger.logTestStep("Set group name");
        userControlPanelPage.setGroupName(groupName);
        logger.logTestStep("Click on searched group name :"+groupName);
        userControlPanelPage.clickOnSearchedGroupName();
        logger.logTestStep("Check whether searched group name displayed or not");
        boolean isSearchedGroupNameDisplayed = newGroupPage.isCreatedGroupNameDisplayed(groupName);
        logger.logTestVerificationStep("Verify searched group name displayed");
        Assert.assertTrue(isSearchedGroupNameDisplayed, "Searched group name is not displayed");
    }
}