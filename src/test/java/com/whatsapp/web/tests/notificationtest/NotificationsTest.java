package com.whatsapp.web.tests.notificationtest;

import com.whatsapp.web.pages.Settings.SettingsPage;
import com.whatsapp.web.pages.Settings.notifications.NotificationsPage;
import com.whatsapp.web.enums.NotificationsTurnoffAlertsEnum;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.util.TestLogger;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 * This class contains all Notifications test features.
 * Created by Venu on 1/25/2017.
 */
public class NotificationsTest extends BaseLoginTest {
    /**
     * Instance variable for Notification page.
     */
    NotificationsPage notificationsPage;
    /**
     * Instance variable for Settings page.
     */
    SettingsPage settingsPage;
    /**
     * Instance variable for Test logger.
     */
   private static final TestLogger logger = TestLogger.getLogger(NotificationsTest.class);
    /**
     * Instance variable for User Control Panel page.
     */
    UserControlPanelPage userControlPanelPage;

    /**
     * Before method scan and navigate to home page.
     */
    @BeforeClass(alwaysRun = true)
    public void setup() {
        notificationsPage = new NotificationsPage(webDriver);
        settingsPage = new SettingsPage(webDriver);
        userControlPanelPage = new UserControlPanelPage(webDriver);
    }

    /**
     * Before method navigate to notifications page.
     */
    @BeforeMethod(alwaysRun = true)
    public void beforeMethodForNavigateToNotificationsPage() {
        logger.logTestStep("Click on User control panel menu button");
        userControlPanelPage.clickOnUserControlPanelMenuButton();
        logger.logTestStep("Click on settings link");
        userControlPanelPage.clickOnSettingsLink();
        logger.logTestStep("Click on notifications link");
        settingsPage.clickOnNotificationsLink();
    }

    /**
     * After method navigate to home page.
     */
    @AfterMethod(alwaysRun = true)
    public void afterMethodForNavigateToHomePage() {
        logger.logTestStep("Click on back arrow button");
        notificationsPage.clickOnBackArrowButton();
        logger.logTestStep("Navigate to Homepage");
        settingsPage.navigateToHomePage();
    }

    /**
     * Test case to verify deselect sounds checkbox
     */
    @Test(description = "Test case to verify deselect sounds checkbox. ")
    public void verifySoundsNotifications() {
        logger.logTestStep("Reset sounds checkbox");
        notificationsPage.resetSoundsCheckbox();
        logger.logTestStep("Is Sounds checkbox deselected.");
        boolean isSoundsUncheckedCheckboxDisplayed = notificationsPage.isSoundsCheckboxDisplayed();
        logger.logTestVerificationStep("Verify sounds checkbox selected or not :" + isSoundsUncheckedCheckboxDisplayed);
        Assert.assertTrue(isSoundsUncheckedCheckboxDisplayed, "Sounds checkbox is selected.");
        logger.logTestStep("Set sounds checkbox");
        notificationsPage.setSoundsCheckbox();
        logger.logTestStep("Is Sounds checkbox selected");
        isSoundsUncheckedCheckboxDisplayed = notificationsPage.isSoundsCheckboxDisplayed();
        logger.logTestVerificationStep("Verify sounds checkbox selected or not :" + isSoundsUncheckedCheckboxDisplayed);
        Assert.assertTrue(isSoundsUncheckedCheckboxDisplayed, "Sounds checkbox is deselected.");
    }

    /**
     * Test case to verify deselect desktop alerts checkbox
     */
    @Test(description = "Test case to verify deselect desktop alerts checkbox.")
    public void verifyDesktopAlertsNotifications() {
        logger.logTestStep("Reset desktop alerts");
        notificationsPage.resetDesktopAlertsCheckbox();
        logger.logTestStep("Is desktop alerts checkbox selected");
        boolean isDesktopAlertsCheckboxSelected = notificationsPage.isDesktopAlertsCheckboxDisplayed();
        logger.logTestVerificationStep("Verify Desktop alerts checkbox deselected or not." + isDesktopAlertsCheckboxSelected);
        Assert.assertTrue(isDesktopAlertsCheckboxSelected, "Desktop alerts check box is selected.");
        logger.logTestStep("Reset desktop alerts");
        notificationsPage.setDesktopAlertsCheckbox();
        logger.logTestVerificationStep("Verify Desktop alerts checkbox selected or not" + isDesktopAlertsCheckboxSelected);
        Assert.assertTrue(isDesktopAlertsCheckboxSelected, "Desktop alerts checkbox is deselected.");
    }

    /**
     * Test case to verify deselect show previews
     */
    @Test(description = "Test case to verify deselect show previews checkbox")
    public void verifyShowPreviewsNotifications() {
        logger.logTestStep("Reset show previews");
        notificationsPage.resetShowPreviewsCheckbox();
        logger.logTestStep("Is show previews checkbox selected");
        boolean isShowPreviewsCheckboxSelected = notificationsPage.isShowPreviewsRadioButtonDisplayed();
        logger.logTestVerificationStep("Verify show previews checkbox deselected or not." + isShowPreviewsCheckboxSelected);
        Assert.assertTrue(isShowPreviewsCheckboxSelected, "Show previews checkbox is Selected.");
        logger.logTestStep("Reset show previews");
        notificationsPage.setShowPreviewsCheckbox();
        logger.logTestVerificationStep("Verify show previews checkbox selected or not." + isShowPreviewsCheckboxSelected);
        Assert.assertTrue(isShowPreviewsCheckboxSelected, "Show previews checkbox is deselected.");
    }

    /**
     * Test cae to verify turnoff alerts for one hour
     */
    @Test(description = "Test to verify TurnOffAlertsForOneHour. ")
    public void verifyTurnOffAlertsForOneHour() {
        logger.logTestStep("SetTurnOffAlertsForOneHour ");
        notificationsPage.setTurnOffAlerts(NotificationsTurnoffAlertsEnum.ONEHOUR);
        logger.logTestStep("Get Actual and Expected Text");
        String actualText = NotificationsTurnoffAlertsEnum.ONEHOUR.getNotificationTurnOffAlertTime();
        String expectedText = notificationsPage.getFirstSelectedValueForTurnOffAlertDropdown();
        logger.logTestVerificationStep("Verify actualText and ExpectedText.");
        Assert.assertEquals(actualText, expectedText, "ActualText is not Matched with ExpectedText.");

        logger.logTestStep("ResetTurnOffAlerts");
        notificationsPage.setTurnOffAlerts(NotificationsTurnoffAlertsEnum.RESETTURNOFFALERTS);
        logger.logTestStep("Get actual and expected Text.");
        String actText = NotificationsTurnoffAlertsEnum.RESETTURNOFFALERTS.getNotificationTurnOffAlertTime();
        String exptText = notificationsPage.getFirstSelectedValueForTurnOffAlertDropdown();
        logger.logTestVerificationStep("Verify ActualText is matched with ExpectedText.");
        Assert.assertEquals(actText, exptText, "ActualText is not Matched with ExpectedText.");
    }

    /**
     * Test case to verify turnoff alerts for one day
     */
    @Test(description = "Test case to verify TurnOffAlertsForOneDay")
    public void verifyTurnOffAlertsForOneDay() {
        logger.logTestStep("SetTurnOffAlertsForOneDay. ");
        notificationsPage.setTurnOffAlerts(NotificationsTurnoffAlertsEnum.ONEDAY);
        logger.logTestStep("Get Actual and Expected Text");
        String actualText = NotificationsTurnoffAlertsEnum.ONEDAY.getNotificationTurnOffAlertTime();
        String expectedText = notificationsPage.getFirstSelectedValueForTurnOffAlertDropdown();
        logger.logTestVerificationStep("Verify actualText and ExpectedText.");
        Assert.assertEquals(actualText, expectedText, "ActualText is not Matched with ExpectedText.");

        logger.logTestStep("ResetTurnOffAlerts");
        notificationsPage.setTurnOffAlerts(NotificationsTurnoffAlertsEnum.RESETTURNOFFALERTS);
        logger.logTestStep("Get Actual and Expected Text");
        String actText = NotificationsTurnoffAlertsEnum.RESETTURNOFFALERTS.getNotificationTurnOffAlertTime();
        String exptText = notificationsPage.getFirstSelectedValueForTurnOffAlertDropdown();
        logger.logTestVerificationStep("Verify actualText and ExpectedText.");
        Assert.assertEquals(actText, exptText, "ActualText is not Matched with ExpectedText.");
    }

    /**
     * Test case to verify turnoff alerts for one week
     */
    @Test(description = "Test case to verify TurnOffAlertsForOneWeek.")
    public void verifyTurnOffAlertsForOneWeek() {
        logger.logTestStep("SetTurnOffAlertsForOneWeek ");
        notificationsPage.setTurnOffAlerts(NotificationsTurnoffAlertsEnum.ONEWEEK);
        logger.logTestStep("Get Actual and Expected Text");
        String actualText = NotificationsTurnoffAlertsEnum.ONEWEEK.getNotificationTurnOffAlertTime();
        String expectedText = notificationsPage.getFirstSelectedValueForTurnOffAlertDropdown();
        logger.logTestVerificationStep("Verify actualText and ExpectedText.");
        Assert.assertEquals(actualText, expectedText, "ActualText is not matched with ExpectedText.");

        logger.logTestStep("ResetTurnOffAlerts");
        notificationsPage.setTurnOffAlerts(NotificationsTurnoffAlertsEnum.RESETTURNOFFALERTS);
        logger.logTestStep("Get Actual and Expected Text");
        String actText = NotificationsTurnoffAlertsEnum.RESETTURNOFFALERTS.getNotificationTurnOffAlertTime();
        String exptText = notificationsPage.getFirstSelectedValueForTurnOffAlertDropdown();
        logger.logTestVerificationStep("Verify actualText and ExpectedText.");
        Assert.assertEquals(actText, exptText, "ActualText is not matched with ExpectedText.");
    }
}
