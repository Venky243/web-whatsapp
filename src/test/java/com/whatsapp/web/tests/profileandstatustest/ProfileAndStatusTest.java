package com.whatsapp.web.tests.profileandstatustest;

import com.whatsapp.web.pages.Settings.notifications.NotificationsPage;
import com.whatsapp.web.enums.UserProfileEnum;
import com.whatsapp.web.pages.autoit.AutoItScriptsPage;
import com.whatsapp.web.pages.profileandstatus.ProfileAndStatusPage;
import com.whatsapp.web.pages.profileandstatus.takephoto.TakePhotoPage;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.pages.whatsappalerts.WhatsappAlertsPage;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.util.PropertyUtil;
import com.whatsapp.web.util.TestLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This Class Contains all ProfileAndStatusTest features.
 * Created by Venu on 1/23/2017.
 */
public class ProfileAndStatusTest extends BaseLoginTest {
    /**
     * Instance variable for Property util.
     */
    PropertyUtil propertyUtil;
    /**
     * Instance variable for profile and status page.
     */
    ProfileAndStatusPage profileAndStatusPage;
    /**
     * Instance variable for User control panel page.
     */
    UserControlPanelPage userControlPanelPage;
    /**
     * Instance variable for Whatsapp alerts page.
     */
    WhatsappAlertsPage whatsappAlertsPage;
    /**
     * Instance variable for Take photo page.
     */
    TakePhotoPage takePhotoPage;
    /**
     * Instance variable for Notifications page
     */
    NotificationsPage notificationsPage;
    /**
     * Instance variable for TestLogger.
     */
   private static final TestLogger logger = TestLogger.getLogger(ProfileAndStatusTest.class);

    /**
     * Before class scan whatsapp logo and navigate to homepage.
     */
    @BeforeClass(alwaysRun = true)
    public void setup() {
        profileAndStatusPage = new ProfileAndStatusPage(webDriver);
        userControlPanelPage = new UserControlPanelPage(webDriver);
        propertyUtil = new PropertyUtil();
        whatsappAlertsPage = new WhatsappAlertsPage(webDriver);
        takePhotoPage = new TakePhotoPage(webDriver);
        notificationsPage = new NotificationsPage(webDriver);
    }

    /**
     * Test case to verify set Status and User name displayed.
     */
    @Test(description = "Test case to verify Status and Username displayed or not.")
    public void verifyProfileAndStatus() {
        logger.logTestStep("Click on user profile picture");
        userControlPanelPage.clickOnUserProfilePicture();
        logger.logTestStep("Get Username from Property file.");
        profileAndStatusPage.editYourName(propertyUtil.getProperty("yourname"));
        logger.logTestStep("Click on save profile name");
        profileAndStatusPage.clickOnSaveProfileName();
        logger.logTestVerificationStep("Get actual and expected text.");
        String actualText = propertyUtil.getProperty("yourname");
        String expectedText = profileAndStatusPage.getUserNameText();
        Assert.assertNotEquals(actualText, expectedText, "Actual text is not matched with expected.");

        String text = RandomStringUtils.randomAlphanumeric(8);
        logger.logTestStep("Get status from property file");
        profileAndStatusPage.editYourStatus(text);
        logger.logTestStep("Click on save status");
        profileAndStatusPage.clickOnSaveStatus();

        logger.logTestStep("Check whether status changed alert present or not");
        boolean isStatusChangedAlertDisplayed = whatsappAlertsPage.isStatusChangedAlertDisplayed();
        logger.logTestVerificationStep("Verify alert is present or not : " + isStatusChangedAlertDisplayed);
        Assert.assertTrue(isStatusChangedAlertDisplayed, "Alert is not presented.");

        logger.logTestStep("Wait until invisible of status changed alert");
        whatsappAlertsPage.waitUntilInvisibleOfStatusChangedAlert();
        logger.logTestStep("Click on back arrow button and navigate to home page");
        notificationsPage.clickOnBackArrowButton();

        logger.logTestStep("Check whether new chat button displayed or not");
        boolean isNewChatButtonDisplayed = userControlPanelPage.isNewChatButtonDisplayed();
        logger.logTestVerificationStep("Verify new chat button displayed or not: " + isNewChatButtonDisplayed);
        Assert.assertTrue(isNewChatButtonDisplayed, "New chat button displayed is not displayed.");
    }

    /**
     * Test case to verify upload profile photo.
     */
    @Test(description = "Test case to verify Upload profile picture")
    public void verifyUploadProfilePhoto() {
        logger.logTestStep("Click on user profile picture");
        userControlPanelPage.clickOnUserProfilePicture();
        logger.logTestStep("Click on change profile photo button");
        profileAndStatusPage.clickOnChangeProfilePhotoButton();
        logger.logTestStep("Click on upload photo link");
        profileAndStatusPage.updateUserProfile(UserProfileEnum.UPLOAD_PHOTO);
        logger.logTestStep("Select profile picture");
        AutoItScriptsPage.selectProfilePicture();
        logger.logTestStep("Click on done button");
        profileAndStatusPage.clickOnDoneButton();

        logger.logTestStep("Check whether profile photo set alert displayed or not");
        boolean isProfilePhotoSetAlertDisplayed = whatsappAlertsPage.isProfilePhotoSetAlertDisplayed();
        logger.logTestVerificationStep("Verify profile photo set alert is displayed or not :" + isProfilePhotoSetAlertDisplayed);
        Assert.assertTrue(isProfilePhotoSetAlertDisplayed, "Profile photo set alert is not displayed.");

        logger.logTestStep("Wait until invisible of profile photo set alert");
        whatsappAlertsPage.waitUntilInvisibleOfProfilePhotoSetAlert();
        logger.logTestStep("Click on back arrow button and navigate to home page");
        notificationsPage.clickOnBackArrowButton();


        logger.logTestStep("Check whether new chat button displayed or not");
        boolean isNewChatButtonDisplayed = userControlPanelPage.isNewChatButtonDisplayed();
        logger.logTestVerificationStep("Verify new chat button displayed or not: " + isNewChatButtonDisplayed);
        Assert.assertTrue(isNewChatButtonDisplayed, "New chat button displayed is not displayed.");
    }

    /**
     * Test case to verify view profile photo displayed or not.
     */
    @Test(description = "Test case to verify View profile photo")
    public void verifyViewProfilePhoto() {
        logger.logTestStep("Click on user profile picture");
        userControlPanelPage.clickOnUserProfilePicture();
        logger.logTestStep("Click on change profile photo button");
        profileAndStatusPage.clickOnChangeProfilePhotoButton();
        logger.logTestStep("Click on view photo link");
        profileAndStatusPage.updateUserProfile(UserProfileEnum.VIEW_PHOTO);

        logger.logTestStep("Check whether profile viewer image displayed or not");
        boolean isProfileViewerImageDisplayed = profileAndStatusPage.isProfileViewerImageDisplayed();
        logger.logTestVerificationStep("Verify profile viewer image displayed or not.");
        Assert.assertTrue(isProfileViewerImageDisplayed, "Profile viewer image image is not displayed");

        logger.logTestStep("Click on profile viewer image close button");
        profileAndStatusPage.clickOnProfileViewerImageCloseButton();
        logger.logTestStep("Wait until invisible of viewer image.");
        profileAndStatusPage.waitUntilInvisibleOfProfileViewerImage();
        logger.logTestStep("Click on back arrow button and navigate to home page");
        notificationsPage.clickOnBackArrowButton();

        logger.logTestStep("Check whether new chat button displayed or not");
        boolean isNewChatButtonDisplayed = userControlPanelPage.isNewChatButtonDisplayed();
        logger.logTestVerificationStep("Verify new chat button displayed or not: " + isNewChatButtonDisplayed);
        Assert.assertTrue(isNewChatButtonDisplayed, "New chat button displayed is not displayed.");
    }

    /**
     * Test case to verify remove profile photo.
     */
    @Test(description = "Test case to verify Remove profile photo")
    public void verifyRemoveProfilePhoto() {
        logger.logTestStep("Click on user profile picture");
        userControlPanelPage.clickOnUserProfilePicture();
        logger.logTestStep("Click on change profile photo button");
        profileAndStatusPage.clickOnChangeProfilePhotoButton();
        logger.logTestStep("Click on remove photo link");
        profileAndStatusPage.updateUserProfile(UserProfileEnum.REMOVE_PHOTO);
        logger.logTestStep("Click on remove profile photo button");
        profileAndStatusPage.clickOnRemoveProfilePhotoButton();

        logger.logTestStep("Check whether profile photo removed alert displayed or not");
        boolean isProfilePhotoRemovedAlertDisplayed = whatsappAlertsPage.isProfilePhotoRemovedAlertDisplayed();
        logger.logTestVerificationStep("Verify profile photo removed alert displayed or not :" + isProfilePhotoRemovedAlertDisplayed);
        Assert.assertTrue(isProfilePhotoRemovedAlertDisplayed, "Profile photo removed alert is not displayed");

        logger.logTestStep("Wait until invisible of photo removed alert");
        whatsappAlertsPage.waitUntilInvisibleOfPhotoRemovedAlert();
        logger.logTestStep("Click on back arrow button and navigate to home page");
        notificationsPage.clickOnBackArrowButton();
        logger.logTestStep("This method reset the profile photo");
        resetProfilePhoto();

        logger.logTestStep("Check whether new chat button displayed or not");
        boolean isNewChatButtonDisplayed = userControlPanelPage.isNewChatButtonDisplayed();
        logger.logTestVerificationStep("Verify new chat button displayed or not: " + isNewChatButtonDisplayed);
        Assert.assertTrue(isNewChatButtonDisplayed, "New chat button displayed is not displayed.");
    }

    /**
     * Test case to verify take photo.
     */
    @Test(description = "Test case to verify Take photo")
    public void verifyTakePhoto() {
        logger.logTestStep("Click on user profile picture");
        userControlPanelPage.clickOnUserProfilePicture();
        logger.logTestStep("Click on change profile photo button");
        profileAndStatusPage.clickOnChangeProfilePhotoButton();
        logger.logTestStep("Click on take photo link");
        profileAndStatusPage.updateUserProfile(UserProfileEnum.TAKE_PHOTO);

        logger.logTestStep("Check whether take photo text displayed or not");
        boolean isTakePhotoTextDisplayed = takePhotoPage.isTakePhotoTextDisplayed();
        logger.logTestVerificationStep("Verify take photo text displayed or not :" + isTakePhotoTextDisplayed);
        Assert.assertTrue(isTakePhotoTextDisplayed, "Take photo text not displayed");

        logger.logTestStep("Check whether camera icon displayed or not ");
        boolean isCameraIconDisplayed = takePhotoPage.isCameraIconDisplayed();
        logger.logTestVerificationStep("Verify camera icon displayed or not :" + isCameraIconDisplayed);
        Assert.assertTrue(isCameraIconDisplayed, "Camera icon not displayed");

        logger.logTestStep("Click on camera close button");
        takePhotoPage.clickOnCameraCloseButton();
        logger.logTestStep("Click on back arrow button and navigate to home page");
        notificationsPage.clickOnBackArrowButton();

        logger.logTestStep("Check whether new chat button displayed or not");
        boolean isNewChatButtonDisplayed = userControlPanelPage.isNewChatButtonDisplayed();
        logger.logTestVerificationStep("Verify new chat button displayed or not: " + isNewChatButtonDisplayed);
        Assert.assertTrue(isNewChatButtonDisplayed, "New chat button displayed is not displayed.");
    }

    /**
     * Method to reset profile photo.
     */
    private void resetProfilePhoto() {
        logger.logTestStep("Click on reset user profile picture");
        userControlPanelPage.clickOnResetUserProfilePicture();
        logger.logTestStep("Click on reset add profile photo");
        profileAndStatusPage.clickOnResetAddProfilePhoto();
        logger.logTestStep("Click on upload photo link");
        profileAndStatusPage.updateUserProfile(UserProfileEnum.UPLOAD_PHOTO);
        logger.logTestStep("Select profile picture");
        AutoItScriptsPage.selectProfilePicture();
        logger.logTestStep("Click on done button");
        profileAndStatusPage.clickOnDoneButton();
        logger.logTestStep("Check whether profile photo set alert displayed or not");
        boolean isProfilePhotoSetAlertDisplayed = whatsappAlertsPage.isProfilePhotoSetAlertDisplayed();
        logger.logTestVerificationStep("Verify profile photo set alert is displayed or not :" + isProfilePhotoSetAlertDisplayed);
        Assert.assertTrue(isProfilePhotoSetAlertDisplayed, "Profile photo set alert is not displayed.");
        logger.logTestStep("Wait until invisible of profile photo set alert");
        whatsappAlertsPage.waitUntilInvisibleOfProfilePhotoSetAlert();
        logger.logTestStep("Click on back arrow button and navigate to home page");
        notificationsPage.clickOnBackArrowButton();
    }

}
