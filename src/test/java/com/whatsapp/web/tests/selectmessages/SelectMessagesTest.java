package com.whatsapp.web.tests.selectmessages;

import com.whatsapp.web.enums.WhatsappAlertsEnum;
import com.whatsapp.web.pages.chat.ChatPage;
import com.whatsapp.web.pages.confirmpages.BlockContactConfirmationAlertPage;
import com.whatsapp.web.pages.confirmpages.ClearOrDeleteMessageConfirmationPage;
import com.whatsapp.web.pages.confirmpages.ForwardMessageConfirmPage;
import com.whatsapp.web.pages.contact.ContactPage;
import com.whatsapp.web.enums.ClearAndDeleteMessageAlert;
import com.whatsapp.web.pages.contactinfopage.ContactInfoPage;
import com.whatsapp.web.pages.contactlist.ContactListPage;
import com.whatsapp.web.pages.forwardmessage.ForwardMessagePage;
import com.whatsapp.web.pages.mycontactinfo.MyContactInfoPage;
import com.whatsapp.web.pages.newchat.NewChatPage;
import com.whatsapp.web.pages.preview.AutoITScripts;
import com.whatsapp.web.pages.preview.PreviewMediaPage;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.pages.whatsappalerts.WhatsappAlerts;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.tests.contactmenutest.ContactMenuTest;
import com.whatsapp.web.util.TestLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by SHIVA on 1/28/2017.
 */
public class SelectMessagesTest extends BaseLoginTest {
    /**
     * Instance variable for Test logger.
     */
    private static final TestLogger logger = TestLogger.getLogger(SelectMessagesTest.class);
    /**
     * Instance variable for Contact list.
     */
    ContactListPage contactListPage;
    /**
     * Instance variable for User control panel page.
     */
    UserControlPanelPage userControlPanelPage;
    /**
     * Instance variable for New chat.
     */
    NewChatPage newChatPage;
    /**
     * Instance variable for My contact info.
     */
    MyContactInfoPage myContactInfoPage;
    /**
     * Instance variable for Alerts.
     */
    WhatsappAlerts whatsappAlerts;
    /**
     * Instance variable for Contact info page.
     */
    ContactInfoPage contactInfoPage;
    /**
     * Instance variable for ChatPage.
     */
    ChatPage chatPage;
    /**
     * Instance variable for Block contact confirmation alert page.
     */
    BlockContactConfirmationAlertPage blockContactConfirmationAlertPage;
    /**
     * Instance variable for Forward message page.
     */
    ForwardMessagePage forwardMessagePage;
    /**
     * Instance variable for Preview page.
     */
    PreviewMediaPage previewMediaPage;
    /**
     * Instance variable for Clear messages confirm page.
     */
    ClearOrDeleteMessageConfirmationPage clearOrDeleteMessageConfirmationPage;
    /**
     * Instance variable for Forward messages confirm page.
     */
    ForwardMessageConfirmPage forwardMessageConfirmPage;
    /**
     * Instance variable for contact page.
     */
    ContactPage contactPage;
    /**
     * Instance variable for Auto it scripts page.
     */
    AutoITScripts autoITScripts;

    @BeforeClass(alwaysRun = true)
    public void setupBrowser() {
        contactListPage = new ContactListPage(webDriver);
        userControlPanelPage = new UserControlPanelPage(webDriver);
        newChatPage = new NewChatPage(webDriver);
        myContactInfoPage = new MyContactInfoPage(webDriver);
        whatsappAlerts = new WhatsappAlerts(webDriver);
        contactInfoPage = new ContactInfoPage(webDriver);
        chatPage = new ChatPage(webDriver);
        blockContactConfirmationAlertPage = new BlockContactConfirmationAlertPage(webDriver);
        forwardMessagePage = new ForwardMessagePage(webDriver);
        previewMediaPage = new PreviewMediaPage(webDriver);
        forwardMessageConfirmPage = new ForwardMessageConfirmPage(webDriver);
        clearOrDeleteMessageConfirmationPage = new ClearOrDeleteMessageConfirmationPage(webDriver);
        contactPage = new ContactPage(webDriver);
        autoITScripts = new AutoITScripts(webDriver);

        logger.logTestStep("Click on new ChatPage button.");
        userControlPanelPage.clickOnNewChatButton();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Set search Contact." + user);
        newChatPage.setSearchContact(user);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(user);
    }

    /**
     * SelectMessagesTest-@T1:Deselect selected messages.
     */
    @Test(description = "Deselect selected messages.")
    public void verifyDeselectSelectedMessages() {
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();

        logger.logTestStep("Check whether checkbox to Select messages is displayed.");
        boolean isCheckboxToSelectMessagesIsDisplayed = chatPage.isCheckboxToSelectMessagesIsDisplayed();
        logger.logTestVerificationStep("Verify whether checkbox to Select messages is displayed." + isCheckboxToSelectMessagesIsDisplayed);
        Assert.assertTrue(isCheckboxToSelectMessagesIsDisplayed,"checkbox to Select messages is not displayed.");
        logger.logTestStep("Set checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();
        logger.logTestStep("Click on Deselect checkbox to select messages.");
        chatPage.clickOnDeselectCheckboxToSelectedMessages();
        logger.logTestStep("Wait until Invisible of checkbox to select messages.");
        chatPage.waitUntilInvisibleOfCheckboxToSelectMessages();

        logger.logTestStep("Check whether checkbox to Select messages is displayed.");
        boolean isCheckboxToSelectMessagesIsDisplayedOrNot = chatPage.isCheckboxToSelectMessagesIsDisplayed();
        logger.logTestVerificationStep("Verify whether checkbox to Select messages is displayed." + isCheckboxToSelectMessagesIsDisplayedOrNot);
        Assert.assertFalse(isCheckboxToSelectMessagesIsDisplayedOrNot,"checkbox to Select messages is displayed.");
    }

    @BeforeMethod
    public void clearAndSendMessage() {
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Clear message link.");
        contactInfoPage.clickOnClearMessageLink();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");

        logger.logTestStep("Check whether Clear message alert body displayed or not.");
        boolean isClearMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert body displayed or not."+isClearMessageAlertBodyDisplayed);
        Assert.assertTrue(isClearMessageAlertBodyDisplayed,"Clear message alert body not displayed.");

        logger.logTestStep("Check whether Clear message alert clear button displayed or not.");
        boolean isClearMessageAlertClearButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertClearButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert clear button displayed or not.");
        Assert.assertTrue(isClearMessageAlertClearButtonDisplayed,"Clear message alert clear button not displayed.");

        logger.logTestStep("Check whether Clear message alert cancel button displayed or not.");
        boolean isClearMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert cancel button displayed or not."+isClearMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isClearMessageAlertCancelButtonDisplayed,"Clear message alert cancel button not displayed.");

        logger.logTestStep("Click on Clear messages confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnClearMessagesConfirm(ClearAndDeleteMessageAlert.CLEAR_CHAT);
        logger.logTestStep("Store chat cleared.");
        String chatClearedAlert = "Chat cleared";
        logger.logTestStep("Wait until Invisible of Alert.");
        whatsappAlerts.waitUntilInvisibleOfAlert(chatClearedAlert);
        logger.logTestStep("Store Chat input text.");
        String chatInputText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input");
        chatPage.setChatInput(chatInputText);
        logger.logTestStep("Click on Send message button");
        chatPage.clickOnSendMessageButton();
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
    }

    /**
     * SelectMessagesTest-@T2:Star messages in selected messages.
     */
    @Test(description = "Star messages in selected messages.")
    public void verifyStarSelectedMessages() {
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();

        logger.logTestStep("Check whether Star messages button displayed.");
        boolean isStarMessagesButtonEnabled = chatPage.isStarMessagesButtonEnabled();
        logger.logTestVerificationStep("Verify whether Star messages button displayed." + isStarMessagesButtonEnabled);
        Assert.assertTrue(isStarMessagesButtonEnabled," Star message button displayed.");
        logger.logTestStep("Set checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();
        logger.logTestStep("Click on Star message button.");
        chatPage.clickOnStarMessagesButton();

        logger.logTestStep("Check whether message Stared alert displayed.");
        boolean isMessageStaredAlertDisplayed = whatsappAlerts.isMessageStaredAlertDisplayed(WhatsappAlertsEnum.MESSAGE_STARRED_ALERT);
        logger.logTestVerificationStep("Verify whether message Stared alert displayed." + isMessageStaredAlertDisplayed);
        Assert.assertTrue(isMessageStaredAlertDisplayed,"Message stared alert not displayed.");

        logger.logTestStep("Click on Message stared or unstared undo button.");
        whatsappAlerts.clickOnMessageStaredOrUnstaredUndoButton();

        logger.logTestStep("Check whether Unstarred message alert displayed.");
        boolean isUnstarredMessageAlertDisplayed = whatsappAlerts.isUnstarredMessageAlertDisplayed(WhatsappAlertsEnum.MESSAGE_UNSTARRED_ALERT);
        logger.logTestVerificationStep("Verify whether Unstarred message alert displayed." + isUnstarredMessageAlertDisplayed);
        Assert.assertTrue(isUnstarredMessageAlertDisplayed,"Unstarred message alert not displayed.");
    }

    /**
     * SelectMessagesTest-@T3:Delete message in selected messages.
     */
    @Test(description = "Delete message in selected messages.")
    public void verifyDeleteMessage() {
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();
        logger.logTestStep("Click on Delete message button.");
        chatPage.clickOnDeleteMessageButton();

        logger.logTestStep("Check whether Delete message alert body displayed or not.");
        boolean isDeleteMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert body displayed or not."+isDeleteMessageAlertBodyDisplayed);
        Assert.assertTrue(isDeleteMessageAlertBodyDisplayed,"Delete message alert body not displayed.");

        logger.logTestStep("Check whether Delete message alert delete button displayed or not.");
        boolean isDeleteMessageAlertDeleteButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertDeleteButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert delete button displayed or not."+isDeleteMessageAlertDeleteButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertDeleteButtonDisplayed,"Delete message alert delete button not displayed.");

        logger.logTestStep("Check whether Delete message alert cancel button displayed or not.");
        boolean isDeleteMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert cancel button displayed or not."+isDeleteMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertCancelButtonDisplayed,"Delete message alert cancel button not displayed.");

        logger.logTestStep("Click on Delete message confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnDeleteMessageConfirm(ClearAndDeleteMessageAlert.DELETE_CHAT);
        logger.logTestStep("Store to Message deleted alert.");
        String messageDeletedAlert = "Message deleted";
        logger.logTestStep("Wait until Invisible of alert.");
        whatsappAlerts.waitUntilInvisibleOfAlert(messageDeletedAlert);
        logger.logTestStep("Check whether Message deleted alert displayed.");
        boolean isMessageDeletedAlertDisplayed = whatsappAlerts.isMessageDeletedAlertDisplayed(WhatsappAlertsEnum.MESSAGE_DELETED_ALERT);
        logger.logTestVerificationStep("Verify whether Message deleted alert displayed." + isMessageDeletedAlertDisplayed);
        Assert.assertTrue(isMessageDeletedAlertDisplayed,"Message deleted alert not displayed.");
    }

    /**
     * SelectMessagesTest-@T4:Forward message in selected messages.
     */
    @Test(description = "Forward message in selected messages.")
    public void verifyForwardMessage() {
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();

        logger.logTestStep("Store Selected message text with time.");
        String selectedMessageTextwithTime = chatPage.getSelectedMessage();
        logger.logTestStep("Store selected message text without time.");
        String selectedMessageTextWithoutTime = chatPage.selectedMessageTextWithoutTime(selectedMessageTextwithTime);
        logger.logTestStep("Click on Forward message button.");
        chatPage.clickOnForwardMessageButton();
        logger.logTestStep("Store User.");
        String chatForwardUser = propertyUtil.getProperty("chatForwardUser");

        logger.logTestStep("Set search Contact." + chatForwardUser);
        forwardMessagePage.setSearchContact(chatForwardUser);

        forwardMessagePage.setContactCheckbox();
        logger.logTestStep("Click on Forward message button.");
        chatPage.clickOnForwardMessageButton();


        forwardMessagePage.waitUntilVisibleOfSearchContact();
        logger.logTestStep("Set search Contact." + chatForwardUser);
        forwardMessagePage.setSearchContact(chatForwardUser);
        logger.logTestStep("Click on Searched contact.");
        forwardMessagePage.clickOnSearchedContact(chatForwardUser);

        logger.logTestStep("Check whether Chat message text Displayed.");
        boolean isChatMessageTextDisplayed = chatPage.isChatMessageTextDisplayed(selectedMessageTextWithoutTime);
        logger.logTestVerificationStep("Verify whether Chat message text Displayed." + isChatMessageTextDisplayed);
        Assert.assertTrue(isChatMessageTextDisplayed,"Chat message text not displayed.");
    }

    /**
     * SelectMessagesTest-@T5:Clear messages in selected messages.
     */
    @Test(description = "Clear messages in selected messages.")
    public void verifyClearMessages() {
        logger.logTestStep("Click on Clear message link.");
        contactInfoPage.clickOnClearMessageLink();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Check whether Clear message alert body displayed or not.");
        boolean isClearMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert body displayed or not."+isClearMessageAlertBodyDisplayed);
        Assert.assertTrue(isClearMessageAlertBodyDisplayed,"Clear message alert body not displayed.");

        logger.logTestStep("Check whether Clear message alert clear button displayed or not.");
        boolean isClearMessageAlertClearButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertClearButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert clear button displayed or not.");
        Assert.assertTrue(isClearMessageAlertClearButtonDisplayed,"Clear message alert clear button not displayed.");

        logger.logTestStep("Check whether Clear message alert cancel button displayed or not.");
        boolean isClearMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isClearMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Clear message alert cancel button displayed or not."+isClearMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isClearMessageAlertCancelButtonDisplayed,"Clear message alert cancel button not displayed.");
        logger.logTestStep("Click on Clear messages confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnClearMessagesConfirm(ClearAndDeleteMessageAlert.CLEAR_CHAT);

        logger.logTestStep("Check whether Chat cleared alert displayed.");
        boolean isChatClearedAlertDisplayed = whatsappAlerts.isChatClearedAlertDisplayed(WhatsappAlertsEnum.CHAT_CLEARED_ALERT);
        logger.logTestVerificationStep("Verify whether Chat cleared alert displayed." + isChatClearedAlertDisplayed);
        Assert.assertTrue(isChatClearedAlertDisplayed,"Chat cleared alert not displayed.");
    }

    /**
     * SelectMessagesTest-@T6:Selected messages count in selected messages.
     */
    @Test(description = "Selected messages count in selected messages.")
    public void verifySelectedMessagesCount() {
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set all checkbox to select messages and Store.");
        int selectedMessagesCount = chatPage.setAllCheckboxesToSelectMessages();

        logger.logTestStep("Check whether Selected messages count displayed.");
        boolean isSelectedMessagesCountDisplayed = chatPage.isSelectedMessagesCountDisplayed(selectedMessagesCount);
        logger.logTestVerificationStep("Verify whether Selected messages count displayed." + isSelectedMessagesCountDisplayed);
        Assert.assertTrue(isSelectedMessagesCountDisplayed,"Selected messages count not displayed.");

        logger.logTestStep("Click on Deselect checkbox to selected messages.");
        chatPage.clickOnDeselectCheckboxToSelectedMessages();
    }

    /**
     * SelectMessagesTest-@T7:Download button in selected messages.
     */
    @Test(description = "Download button in selected messages.")
    public void verifyDownloadButtonToTextMessage() {
        logger.logTestStep("Click on Select messages link.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set all checkboxes to select messages.");
        chatPage.setAllCheckboxesToSelectMessages();
        logger.logTestStep("Click on Delete message button.");
        chatPage.clickOnDeleteMessageButton();

        logger.logTestStep("Check whether Delete message alert body displayed or not.");
        boolean isDeleteMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessagesAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert body displayed or not."+isDeleteMessageAlertBodyDisplayed);
        Assert.assertTrue(isDeleteMessageAlertBodyDisplayed,"Delete message alert body not displayed.");

        logger.logTestStep("Check whether Delete message alert delete button displayed or not.");
        boolean isDeleteMessageAlertDeleteButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertDeleteButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert delete button displayed or not."+isDeleteMessageAlertDeleteButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertDeleteButtonDisplayed,"Delete message alert delete button not displayed.");

        logger.logTestStep("Check whether Delete message alert cancel button displayed or not.");
        boolean isDeleteMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert cancel button displayed or not."+isDeleteMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertCancelButtonDisplayed,"Delete message alert cancel button not displayed.");

        logger.logTestStep("Click on Delete message confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnDeleteMessageConfirm(ClearAndDeleteMessageAlert.DELETE_CHAT);
        logger.logTestStep("Store to Message deleted alert.");
        String messageDeletedAlert = "Message deleted";
        logger.logTestStep("Wait until Invisible of alert.");
        whatsappAlerts.waitUntilInvisibleOfAlert(messageDeletedAlert);

        logger.logTestStep("Store Chat input text.");
        String chatInputText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input");
        chatPage.setChatInput(chatInputText);
        logger.logTestStep("Click on Send message button");
        chatPage.clickOnSendMessageButton();
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();

        logger.logTestStep("Check whether Download button disabled.");
        boolean isDownloadButtonDisabled = chatPage.isDownloadButtonDisabled();
        logger.logTestVerificationStep("Verify whether Download button disabled." + isDownloadButtonDisabled);
        Assert.assertTrue(isDownloadButtonDisabled,"Download button enabled.");
        logger.logTestStep("Click on Deselect checkbox to selected messages.");
        chatPage.clickOnDeselectCheckboxToSelectedMessages();
    }

    /**
     * SelectMessagesTest-@T8:Download button in selected image.
     */
    @Test(description = "Download button in selected image.")
    public void verifyDownloadButtonToSelectedImage() {
        logger.logTestStep("Click on Select messages link.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set all checkboxes to select messages.");
        chatPage.setAllCheckboxesToSelectMessages();
        logger.logTestStep("Click on Delete message button.");
        chatPage.clickOnDeleteMessageButton();
        logger.logTestStep("Check whether Delete message alert body displayed or not.");
        boolean isDeleteMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessagesAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert body displayed or not."+isDeleteMessageAlertBodyDisplayed);
        Assert.assertTrue(isDeleteMessageAlertBodyDisplayed,"Delete message alert body not displayed.");

        logger.logTestStep("Check whether Delete message alert delete button displayed or not.");
        boolean isDeleteMessageAlertDeleteButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertDeleteButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert delete button displayed or not."+isDeleteMessageAlertDeleteButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertDeleteButtonDisplayed,"Delete message alert delete button not displayed.");

        logger.logTestStep("Check whether Delete message alert cancel button displayed or not.");
        boolean isDeleteMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert cancel button displayed or not."+isDeleteMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertCancelButtonDisplayed,"Delete message alert cancel button not displayed.");

        logger.logTestStep("Click on Delete message confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnDeleteMessageConfirm(ClearAndDeleteMessageAlert.DELETE_CHAT);
        logger.logTestStep("Store to Message deleted alert.");
        String messageDeletedAlert = "Message deleted";
        logger.logTestStep("Wait until Invisible of alert.");
        whatsappAlerts.waitUntilInvisibleOfAlert(messageDeletedAlert);

        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Photos and Videos button.");
        contactInfoPage.clickOnPhotosAndVideosButton();
        logger.logTestStep("Select Photo.");
        autoITScripts.selectPhoto();

        logger.logTestStep("Store Chat input text.");
        String captionText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(captionText);
        logger.logTestStep("Click on Send photo or Video button.");
        previewMediaPage.clickOnSendButton();

        logger.logTestStep("Check whether Caption text displayed.");
        boolean isCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(captionText);
        logger.logTestVerificationStep("Verify whether Caption text displayed." + isCaptionTextDisplayed);
        Assert.assertTrue(isCaptionTextDisplayed,"Caption Text not displayed.");

        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set image checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();

        logger.logTestStep("Check whether Download button enabled.");
        boolean isDownloadButtonEnabled = chatPage.isDownloadButtonEnabled();
        logger.logTestVerificationStep("Verify whether Download button enabled." + isDownloadButtonEnabled);
        Assert.assertTrue(isDownloadButtonEnabled,"Download button not enabled.");
        logger.logTestStep("Click on Deselect checkbox to selected messages.");
        chatPage.clickOnDeselectCheckboxToSelectedMessages();
    }

    /**
     * SelectMessagesTest-@T9:Download button in selected video.
     */
    @Test(description = "Download button in selected video.")
    public void verifyDownloadButtonToSelectedVideo() {
        logger.logTestStep("Click on Select messages link.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set all checkboxes to select messages.");
        chatPage.setAllCheckboxesToSelectMessages();
        logger.logTestStep("Click on Delete message button.");
        chatPage.clickOnDeleteMessageButton();

        logger.logTestStep("Check whether Delete message alert body displayed or not.");
        boolean isDeleteMessageAlertBodyDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessagesAlertBodyDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert body displayed or not."+isDeleteMessageAlertBodyDisplayed);
        Assert.assertTrue(isDeleteMessageAlertBodyDisplayed,"Delete message alert body not displayed.");

        logger.logTestStep("Check whether Delete message alert delete button displayed or not.");
        boolean isDeleteMessageAlertDeleteButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertDeleteButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert delete button displayed or not."+isDeleteMessageAlertDeleteButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertDeleteButtonDisplayed,"Delete message alert delete button not displayed.");

        logger.logTestStep("Check whether Delete message alert cancel button displayed or not.");
        boolean isDeleteMessageAlertCancelButtonDisplayed = clearOrDeleteMessageConfirmationPage.isDeleteMessageAlertCancelButtonDisplayed();
        logger.logTestVerificationStep("Verify whether Delete message alert cancel button displayed or not."+isDeleteMessageAlertCancelButtonDisplayed);
        Assert.assertTrue(isDeleteMessageAlertCancelButtonDisplayed,"Delete message alert cancel button not displayed.");

        logger.logTestStep("Click on Delete message confirm button.");
        clearOrDeleteMessageConfirmationPage.clickOnDeleteMessageConfirm(ClearAndDeleteMessageAlert.DELETE_CHAT);
        logger.logTestStep("Store to Message deleted alert.");
        String messageDeletedAlert = "Message deleted";
        logger.logTestStep("Wait until Invisible of alert.");
        whatsappAlerts.waitUntilInvisibleOfAlert(messageDeletedAlert);
        logger.logTestStep("Click on Attachment button.");
        contactInfoPage.clickOnAttachmentButton();
        logger.logTestStep("Click on Photos and Videos button.");
        contactInfoPage.clickOnPhotosAndVideosButton();
        logger.logTestStep("Select Photo.");
        autoITScripts.selectVideo();

        logger.logTestStep("Store Chat input text.");
        String captionText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Photo or Video caption.");
        previewMediaPage.setCaption(captionText);
        logger.logTestStep("Click on Send photo or Video button.");
        previewMediaPage.clickOnSendButton();

        logger.logTestStep("Check whether Caption text displayed.");
        boolean isCaptionTextDisplayed = chatPage.isCaptionTextDisplayed(captionText);
        logger.logTestVerificationStep("Verify whether Caption text displayed." + isCaptionTextDisplayed);
        Assert.assertTrue(isCaptionTextDisplayed,"Caption Text not displayed.");

        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        logger.logTestStep("Click on Select messages button.");
        contactInfoPage.clickOnSelectMessagesLink();
        logger.logTestStep("Set image checkbox to select messages.");
        chatPage.setCheckboxToSelectMessages();

        logger.logTestStep("Check whether Download button enabled.");
        boolean isDownloadButtonEnabled = chatPage.isDownloadButtonEnabled();
        logger.logTestVerificationStep("Verify whether Download button enabled." + isDownloadButtonEnabled);
        Assert.assertTrue(isDownloadButtonEnabled,"Download button not enabled.");
        logger.logTestStep("Click on Deselect checkbox to selected messages.");
        chatPage.clickOnDeselectCheckboxToSelectedMessages();
    }

    /**
     * SelectMessagesTest-@T10:Unfinished message in Chat.
     */
    @Test(description = "Unfinished message in Chat.")
    public void verifyChatUnfinishedMessage(){
        logger.logTestStep("Click on Contact user menu.");
        contactInfoPage.clickOnContactUserMenu();
        chatPage.waitUntilVisibleOfChatInput();
        logger.logTestStep("Store Chat input text.");
        String chatInputText = RandomStringUtils.randomAlphanumeric(9);
        logger.logTestStep("Set Chat input"+chatInputText);
        chatPage.setChatInput(chatInputText);

        logger.logTestStep("Store chat forward user.");
        String chatForwardUser = propertyUtil.getProperty("chatForwardUser");
        logger.logTestStep("Set Search contact" + chatForwardUser);
        forwardMessagePage.setSearchContact(chatForwardUser);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(chatForwardUser);

        logger.logTestStep("Click on new ChatPage button.");
        userControlPanelPage.clickOnNewChatButton();
        logger.logTestStep("Store User.");
        String user = propertyUtil.getProperty("chatUser");
        logger.logTestStep("Set search Contact." + user);
        newChatPage.setSearchContact(user);
        logger.logTestStep("Click on Searched contact.");
        newChatPage.clickOnSearchedContact(user);

        String actualText = chatPage.getChatInputText();
        logger.logTestStep(""+actualText+" "+chatInputText);
        Assert.assertTrue(actualText.contains(chatInputText));
    }
}