package com.whatsapp.web.tests.setchatwallpaper;

import com.whatsapp.web.pages.Settings.SettingsPage;
import com.whatsapp.web.pages.Settings.notifications.NotificationsPage;
import com.whatsapp.web.pages.setchatwallpaper.SetChatWallpaperPage;
import com.whatsapp.web.pages.usercontrolpanelpage.UserControlPanelPage;
import com.whatsapp.web.pages.whatsappalerts.WhatsappAlertsPage;
import com.whatsapp.web.tests.BaseLoginTest;
import com.whatsapp.web.util.TestLogger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains all Set chat wallpaper test features.
 * Created by Venu on 1/29/2017.
 */
public class SetChatWallPaperTest extends BaseLoginTest {
    /**
     * Instance variable for Set chat wallpaper page.
     */
    SetChatWallpaperPage setChatWallPaperPage;
    /**
     * Instance variable for Settings page.
     */
    SettingsPage settingsPage;
    /**
     * Instance variable for User control panel page.
     */
    UserControlPanelPage userControlPanelPage;
    /**
     * Instance variable for Whatsapp page.
     */
    WhatsappAlertsPage whatsappAlertsPage;
    /**
     * Instance variable for Notifications page.
     */
    NotificationsPage notificationsPage;
    /**
     * Instance variable for Test logger.
     */
   private static final TestLogger logger = TestLogger.getLogger(SetChatWallPaperTest.class);

    /**
     * Before class scan whatsapp logo and navigate to home page.
     */
    @BeforeClass(alwaysRun = true)
    public void setUp() {
        userControlPanelPage = new UserControlPanelPage(webDriver);
        settingsPage = new SettingsPage(webDriver);
        setChatWallPaperPage = new SetChatWallpaperPage(webDriver);
        whatsappAlertsPage = new WhatsappAlertsPage(webDriver);
        notificationsPage = new NotificationsPage(webDriver);
    }

    /**
     * Test case to verify set wallpaper in list
     */
    @Test(description = "Test case to verify set wallpaper in list")
    public void setWallpaperInList() {
        logger.logTestStep("Click on user control panel menu button.");
        userControlPanelPage.clickOnUserControlPanelMenuButton();
        logger.logTestStep("Click on settings link.");
        userControlPanelPage.clickOnSettingsLink();
        logger.logTestStep("Click on chat wallpaper.");
        settingsPage.clickOnChatWallpaper();
        logger.logTestStep("Get the colour value from Property file");
        setChatWallPaperPage.setColourInList(propertyUtil.getProperty("colour3"));
        logger.logTestStep("Check whether chat wallpaper set alert displayed or not.");
        boolean isChatWallpaperSetAlertDisplayed = whatsappAlertsPage.isChatWallpaperSetAlertDisplayed();
        logger.logTestVerificationStep("Verify chat wallpaper set alert displayed or not");
        Assert.assertTrue(isChatWallpaperSetAlertDisplayed, "Chat wallpaper set alert is not displayed");
        logger.logTestStep("Wait until invisible of chat wallpaper set alert");
        whatsappAlertsPage.waitUntilInvisibleOfChatWallpaperSetAlert();

        logger.logTestVerificationStep("Check whether selected colour displayed or not.");
        boolean isSelectedColourDisplayed = setChatWallPaperPage.isSelectedColourDisplayed();
        logger.logTestVerificationStep("Verify selected colour displayed or not " + isSelectedColourDisplayed);
        Assert.assertTrue(isSelectedColourDisplayed, "Selected colour is not displayed");


        logger.logTestStep("Click on default colour.");
        setChatWallPaperPage.clickOnDefaultColour();
        logger.logTestVerificationStep("Verify selected colour displayed or not.");
        Assert.assertTrue(isSelectedColourDisplayed, "Selected colour is not displayed");

        logger.logTestVerificationStep("Check whether chat wallpaper set alert displayed or not.");
        Assert.assertTrue(isChatWallpaperSetAlertDisplayed, "Chat wallpaper set alert is not displayed");
        logger.logTestStep("Wait until invisible of wallpaper set alert");
        whatsappAlertsPage.waitUntilInvisibleOfChatWallpaperSetAlert();
        logger.logTestStep("Click on back arrow button");
        notificationsPage.clickOnBackArrowButton();
        logger.logTestStep("Click on back arrow button");
        notificationsPage.clickOnBackArrowButton();
    }
}
